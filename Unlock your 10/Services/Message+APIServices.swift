//
//  Message+APIServices.swift
//  Unlock your 10
//
//  Created by brst on 10/25/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import Foundation


enum MessageAPIServices: APIService {

    case createMessage(messageDetails: NSDictionary)
    case getHome(messageDetails: NSDictionary)
    case getChatThreads(threadDetails:NSDictionary)
    case getChatHistory(chatDetails:NSDictionary)
    case deleteThread(threadDetail:NSDictionary)
    
    
    var path: String {
        var path = ""
        switch self {
        case .createMessage:
            path = BASE_USER_URL.appending("createmessage")
        case .getHome:
            path = BASE_API_URL.appending("homecontact")
        case .getChatThreads:
            path = BASE_USER_URL.appending("get_chat_thread")
            
        case .getChatHistory:
            path = BASE_USER_URL.appending("get_chat_history")
            
        case .deleteThread:
            path = BASE_USER_URL.appending("delete_thread")

        }
        return path
    }
    
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .createMessage(messageDetails):
            resource = Resource(method: .post, parameters: messageDetails as? [String : Any], headers: nil)
            
        case let .getHome(homeDetails):
            resource = Resource(method: .post, parameters: homeDetails as? [String : Any], headers: nil)
            
        case let .getChatThreads(threadDetails):
            resource = Resource(method: .post, parameters: threadDetails as? [String : Any], headers: nil)
            
        case let .getChatHistory(chatDetails):
            resource = Resource(method: .post, parameters: chatDetails as? [String : Any], headers: nil)
            
        case let .deleteThread(threadDetails):
            resource = Resource(method: .post, parameters: threadDetails as? [String : Any], headers: nil)
        }
        
        
        return resource
    }
 
}



extension APIManager {
    
    class func createMessage(messageDetails:NSDictionary, applicationDict:[String:Data]?,mineType:String? , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        MessageAPIServices.createMessage(messageDetails:messageDetails).uploadMultiple(imageDict: nil, applicationDict: applicationDict,mineType: mineType, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func getHome(homeDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        MessageAPIServices.getHome(messageDetails: homeDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
   
    
    class func getChatThreads(threadDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        MessageAPIServices.getChatThreads(threadDetails: threadDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    
    class func getChatHistory(chatDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        MessageAPIServices.getChatHistory(chatDetails: chatDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func deleteThread(threadDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        MessageAPIServices.deleteThread(threadDetail: threadDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
}

