//
//  Keys+APIServices.swift
//  Unlock your 10
//
//  Created by brst on 11/24/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import Foundation
//
//  Users+APIServices.swift
//  HealthSplashPatient
//
//  Created by 123 on 13/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation


enum KeysAPIServices: APIService {
    
    case getKeys(keysDetails: NSDictionary)
    case purchaseKeys(keysDetails : NSDictionary)
    case purchasePackage(packageDetails : NSDictionary)
    case withdrawalRequest(requestDetails : NSDictionary)
    case referUser(userDetails :NSDictionary)
    case requestTraining (trainingDetails:NSDictionary)
    
    var path: String {
        var path = ""
        switch self {
            
        case .getKeys:
            path = BASE_API_URL.appending("get_keys")
        case .purchaseKeys:
            path = BASE_API_URL.appending("purchase_key")
        case .purchasePackage:
            path = BASE_API_URL.appending("purchase_package")
        case .withdrawalRequest:
            path = BASE_API_URL.appending("withdrawal_request")
        case .referUser:
            path = BASE_API_URL.appending("referUser")
        case .requestTraining:
            path = BASE_API_URL.appending("request_training_session")
       
        }
        return path
    }
    
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .getKeys(keysDetails):
            resource = Resource(method: .post, parameters: keysDetails as? [String : Any], headers: nil)
 
        case let .purchaseKeys(keysDetails):
            resource = Resource(method: .post, parameters: keysDetails as? [String : Any], headers: nil)
            
        case let .purchasePackage(packageDetails):
            resource = Resource(method: .post, parameters: packageDetails as? [String : Any], headers: nil)
            
        case let .withdrawalRequest(requestDetails):
            resource = Resource(method: .post, parameters: requestDetails as? [String : Any], headers: nil)
            
        case let .referUser(userDetails):
            resource = Resource(method: .post, parameters: userDetails as? [String : Any], headers: nil)
            
        case let .requestTraining(trainingDetails):
            resource = Resource(method: .post, parameters: trainingDetails as? [String : Any], headers: nil)
            
        }
        return resource
    }
}


extension APIManager {
    class func getKeys(keysDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        KeysAPIServices.getKeys(keysDetails: keysDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func purchaseKeys(keysDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        KeysAPIServices.purchaseKeys(keysDetails: keysDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func purchasePackage(packageDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        KeysAPIServices.purchasePackage(packageDetails: packageDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    class func withdrawalRequest(requestDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        KeysAPIServices.withdrawalRequest(requestDetails: requestDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func referUser(userDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        KeysAPIServices.referUser(userDetails: userDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    class func requestTraining(trainingDetails:NSDictionary , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        KeysAPIServices.requestTraining(trainingDetails: trainingDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    
}

