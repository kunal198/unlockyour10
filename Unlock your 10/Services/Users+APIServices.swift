//
//  Users+APIServices.swift
//  HealthSplashPatient
//
//  Created by 123 on 13/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation

enum UserAPIServices: APIService {
    
    case SignUpByEmail(signupDetails: NSDictionary)
    case LoginByEmail(loginDetails: NSDictionary)
    case ForgotPassword(forgotPasswordDetails: NSDictionary)
    case FetchInsuranceList()
    case mentorRegister(mentorDetails: NSDictionary)
    case searchMentors(searchDetails: NSDictionary)
    case updateProfile(profileDetails:NSDictionary)
    case getMentorProfile(profileDetails:NSDictionary)
    case postComplaints(complaintDetails:NSDictionary)
    case getComplaints(complaintDetails:NSDictionary)
    case postComments(complaintDetails:NSDictionary)
    case addTrainingSession(sessionDetails:NSDictionary)
    case getTrainingSession(sessionDetails:NSDictionary)
    case deleteTrainingSession(sessionDetails: NSDictionary)
    case editTrainingSession(sessionDetails: NSDictionary)
    case addDateRequest(dateRequestDetails: NSDictionary)
    case checkName(checkName: NSDictionary)
    case getUserProfile(userProfileDetails: NSDictionary)
    case bookingRequest(bookingDetails: NSDictionary)
    case getRequestSession(sessionDetails: NSDictionary)
    case chnageTrainingSession(sessionDetails: NSDictionary)
    case getDetailsOfTrainig(sessionDetails: NSDictionary)
    case joinTraining(sessionDetails: NSDictionary)
    case leaveTraining(sessionDetails: NSDictionary)
    var path: String {
        var path = ""
        switch self {
        case .SignUpByEmail:
            path = BASE_USER_URL.appending("signup")
        case .LoginByEmail:
            path = BASE_USER_URL.appending("login")
        case .ForgotPassword:
            path = BASE_USER_URL.appending("forgetpassword")
        case .FetchInsuranceList:
            path = BASE_USER_URL.appending("insurance")
        case .mentorRegister:
            path = BASE_USER_URL.appending("mentorsignup")
        case .searchMentors:
            path = BASE_API_URL.appending("search")
        case .updateProfile:
            path = BASE_USER_URL.appending("updateprofile")
        case .getMentorProfile:
            path = BASE_USER_URL.appending("get_mentor_profile")
        case .postComplaints:
            path = BASE_USER_URL.appending("post_complaints")
        case .getComplaints:
            path = BASE_USER_URL.appending("get_complaints")
        case .postComments:
            path = BASE_USER_URL.appending("post_comments")
        case .addTrainingSession:
            path = BASE_USER_URL.appending("add_training_session")
        case .getTrainingSession:
            path = BASE_USER_URL.appending("get_training_session")
        case .deleteTrainingSession:
             path = BASE_USER_URL.appending("delete_training_session")
        case .editTrainingSession:
            path = BASE_API_URL.appending("edit_training_session")
        case .addDateRequest:
            path = BASE_API_URL.appending("add_date_request")
        case .checkName:
            path = BASE_API_URL.appending("check_name")
        case .getUserProfile:
            path = BASE_USER_URL.appending("get_user_profile")
        case .bookingRequest:
            path = BASE_API_URL.appending("request_training_session")
        case .getRequestSession:
            path = BASE_API_URL.appending("get_request_trainings")
        case .chnageTrainingSession:
            path = BASE_API_URL.appending("change_traning_status")
        case .getDetailsOfTrainig:
            path = BASE_API_URL.appending("training_detail")
        case .joinTraining:
            path = BASE_API_URL.appending("join_training")
        case .leaveTraining:
            path = BASE_API_URL.appending("leave_training")
        }
        return path
    }
    
    
    var resource: Resource {
        var resource: Resource!
        switch self {
            
        case let .SignUpByEmail(signupDetails):
            resource = Resource(method: .post, parameters: signupDetails as? [String : Any], headers: nil)
            
        case let .LoginByEmail(loginDetails):
            resource = Resource(method: .post, parameters: loginDetails as? [String : Any] , headers: nil)
      

        case let .ForgotPassword(forgotPasswordDetails):
            resource = Resource(method: .post, parameters: forgotPasswordDetails as? [String : Any] , headers: nil)
            
        case .FetchInsuranceList():
            resource = Resource(method: .get, parameters: nil  , headers: nil)
            
        case let .mentorRegister(mentorDetails):
            resource = Resource(method: .post, parameters: mentorDetails as? [String : Any] , headers: nil)
            
        case let .searchMentors(searchDetails):
            resource = Resource(method: .post, parameters: searchDetails as? [String : Any] , headers: nil)
            
        case let .updateProfile(profileDetails):
            resource = Resource(method: .post, parameters: profileDetails as? [String : Any] , headers: nil)
            
        case let .getMentorProfile(profileDetails):
            resource = Resource(method: .post, parameters: profileDetails as? [String : Any] , headers: nil)
        
        case let .getComplaints(complaintDetails):
            resource = Resource(method: .post, parameters: complaintDetails as? [String : Any] , headers: nil)
            
        case let .postComplaints(complaintDetails):
            resource = Resource(method: .post, parameters: complaintDetails as? [String : Any] , headers: nil)
            
        case let .postComments(commentDetails):
            resource = Resource(method: .post, parameters: commentDetails as? [String: Any], headers: nil)
            
        case let .addTrainingSession(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
            
        case let .getTrainingSession(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
            
        case let .deleteTrainingSession(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
            
        case let .editTrainingSession(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
        case let .addDateRequest(dateReqDetails):
            resource = Resource(method: .post, parameters: dateReqDetails as? [String: Any], headers: nil)
        case let .checkName(checkName):
            resource = Resource(method: .post, parameters: checkName as? [String: Any], headers: nil)
        case let .getUserProfile(profileDetails):
            resource = Resource(method: .post, parameters: profileDetails as? [String: Any], headers: nil)
        case let .bookingRequest(bookingDetails):
            resource = Resource(method: .post, parameters: bookingDetails as? [String: Any], headers: nil)
        case let .getRequestSession(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
        case let .chnageTrainingSession(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
        case let .getDetailsOfTrainig(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
        case let .joinTraining(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
        case let .leaveTraining(sessionDetails):
            resource = Resource(method: .post, parameters: sessionDetails as? [String: Any], headers: nil)
        }
        return resource
    }
}


extension APIManager {
    class func signupByEmail(signupDetails: NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.SignUpByEmail(signupDetails: signupDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func loginWithEmail(loginDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.LoginByEmail(loginDetails:loginDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
  
    class func forgotPassword(forgotPasswordDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.ForgotPassword(forgotPasswordDetails:forgotPasswordDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func fetchInsuranceList(successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.FetchInsuranceList().request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func mentorRegister(mentorDetails:NSDictionary,imageDict:[String:Data]?, applicationDict:[String:Data]?,mineType:String? , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.mentorRegister(mentorDetails:mentorDetails).uploadMultiple(imageDict: imageDict, applicationDict: applicationDict,mineType: mineType, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func searchMentors(searchDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.searchMentors(searchDetails: searchDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }

    class func updateProfile(profileDetails:NSDictionary,imageDict:[String:Data]?, applicationDict:[String:Data]?,mineType:String? , successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.updateProfile(profileDetails: profileDetails).uploadMultiple(imageDict: imageDict, applicationDict: applicationDict,mineType: mineType, success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    class func getMentorProfile(profileDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        UserAPIServices.getMentorProfile(profileDetails: profileDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    //Post Complaints
    class func PostComplaints(complaintDetails:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.postComplaints(complaintDetails: complaintDetails).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    //Get Complaints
    class func getComplaints(getComplaints:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.getComplaints(complaintDetails: getComplaints).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    //Get Complaints
    class func postComments(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.postComments(complaintDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //Add Trainig Session
    class func addTrainingSession(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.addTrainingSession(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    //Get Trainig Session
    class func getTrainingSession(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.getTrainingSession(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    //Delete Trainig Session
    class func deleteTrainingSession(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.deleteTrainingSession(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //edit Trainig Session
    class func editTrainingSession(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.editTrainingSession(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    //Add Date Request
    class func addDateRequest(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.addDateRequest(dateRequestDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //Check Name
    class func checkName(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.checkName(checkName: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    //Get User Profile
    class func getUserProfile(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        UserAPIServices.getUserProfile(userProfileDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //Booking Request
    class func bookingRequest(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        print(dicData)
        UserAPIServices.bookingRequest(bookingDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    
    
    //Booking Request
    class func getRequestTrainingSession(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        print(dicData)
        UserAPIServices.getRequestSession(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    //ChnageTrainingStatus
    class func ChnageTrainingStatus(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        print(dicData)
        UserAPIServices.chnageTrainingSession(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    // Get Details Of Trainig
    class func getDetailsOfTrainig(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        print(dicData)
        UserAPIServices.getDetailsOfTrainig(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
}
extension APIManager
{
    // Join Training Session
    class func joinTraining(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        print(dicData)
        UserAPIServices.joinTraining(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
    // Leave Training Session
    class func leaveTrainingSession(dicData:NSDictionary, successCallback: @escaping JSONDictionaryResponseCallback, failureCallback: @escaping APIServiceFailureCallback) {
        
        print(dicData)
        UserAPIServices.leaveTraining(sessionDetails: dicData).request(success: { (response) in
            if let responseDict = response as? JSONDictionary {
                successCallback(responseDict)
            }
            else {
                successCallback([:])
            }
        }, failure: failureCallback)
    }
}
