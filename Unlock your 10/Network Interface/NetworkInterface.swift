//
//  NetworkInterface.swift
//  Prabhjot Singh
//
//  Created by Prabhjot Singh on 10/01/2017.
//  Copyright © 2017 . All rights reserved.
//

import Foundation
import Alamofire

//MARK: Call Backs
typealias JSONDictionary = [String:Any]
typealias JSONArray = [JSONDictionary]
typealias APIServiceSuccessCallback = ((Any?) -> ())
typealias APIServiceFailureCallback = ((NetworkErrorReason?, NSError?) -> ())
typealias JSONArrayResponseCallback = ((JSONArray?) -> ())
typealias JSONDictionaryResponseCallback = ((JSONDictionary?) -> ())
typealias responseCallBack = ((Bool, String?, NSError?) -> ())


//MARK: Constant
var kMessage = "message"

public enum NetworkErrorReason: Error {
    case FailureErrorCode(code: Int, message: String)
    case InternetNotReachable
    case UnAuthorizedAccess
    case Other
}

struct Resource {
    let method: HTTPMethod
    let parameters: [String : Any]?
    let headers: [String:String]?
}

protocol APIService {
    var path: String { get }
    var resource: Resource { get }
}

extension APIService {
    
    /**
     Method which needs to be called from the respective model class.
     - parameter successCallback:   successCallback with the JSON response.
     - parameter failureCallback:   failureCallback with ErrorReason, Error description and Error.
     */
    
    //MARK: Request Method to Send Request except in Multipart
    func request(isURLEncoded: Bool = false, success: @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        do {
            if Indicator.isEnabledIndicator {
                Indicator.sharedInstance.showIndicator()
            }
            //check for save keys
            if path == "purchase_key" || path == "purchase_package" {
                Indicator.isEnabledIndicator = true
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            debugPrint("********************************* API Request **************************************")
            debugPrint("Request URL:\(path)")
            debugPrint("Request resource: \(resource)")
            debugPrint("************************************************************************************")
            
            var encoding: JSONEncoding = .default
            if resource.method == .get || resource.method == .head || resource.method == .delete || isURLEncoded{
                encoding = .prettyPrinted
            }
            
            debugPrint("Request method: \(resource.method)")
            debugPrint("Request parameter:\(String(describing: resource.parameters))")
            debugPrint("Request encoding: \(encoding)")
            debugPrint("Request headers: \(String(describing: resource.headers))")
            
         
            
            Alamofire.request(path, method: resource.method, parameters: resource.parameters, encoding: encoding, headers: resource.headers).validate().responseJSON(completionHandler: { (response) in
                debugPrint("********************************* API Response *************************************")
                debugPrint("\(response.debugDescription)")
                debugPrint("************************************************************************************")
                if Indicator.isEnabledIndicator {
                    Indicator.sharedInstance.hideIndicator()
                }

                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success(let value):
                    success(value as AnyObject?)
                case .failure(let error):
                    self.handleError(response: response, error: error as NSError, callback: failure)
                }
            })
        }
    }
   
 
//MARK:
//MARK: Request Method to Upload Multipart
    func uploadMultiple(imageDict:[String: Data]?,applicationDict:[String: Data]?,mineType:String?, success:  @escaping APIServiceSuccessCallback, failure: @escaping APIServiceFailureCallback) {
        do {
            debugPrint("********************************* API Request **************************************")
            debugPrint("Request URL:\(path)")
            debugPrint("Request resource: \(resource)")
            debugPrint("image dictionary: \(String(describing: imageDict))")
            
            debugPrint("************************************************************************************")
            if Indicator.isEnabledIndicator {
                Indicator.sharedInstance.showIndicator()
            }
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let urlRequest = createRequestWithMultipleImages(urlString: path, parameters: resource.parameters, imageDict: imageDict,applicationDict: applicationDict,mineType: mineType)
            Alamofire.upload((urlRequest?.1)!, with: (urlRequest?.0)!).uploadProgress(closure: { (progress) in
                print(progress.localizedDescription)
            }).responseJSON(completionHandler: { (response) in
                debugPrint("********************************* API Response *************************************")
                debugPrint("\(response.debugDescription)")
                debugPrint("************************************************************************************")
                if Indicator.isEnabledIndicator {
                    Indicator.sharedInstance.hideIndicator()
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                switch response.result {
                case .success(let value):
                    success(value as AnyObject?)
                case .failure(let error):
                    self.handleError(response: response, error: error as NSError, callback: failure)
                }
            })
        }
    }
    
    func createRequestWithMultipleImages(urlString:String, parameters:[String : Any]?, imageDict: [String: Data]?,applicationDict: [String: Data]?,mineType:String?) -> (URLRequestConvertible, Data)? {
        
        // create url request to send
        var mutableURLRequest = URLRequest(url: NSURL(string: urlString)! as URL)
        mutableURLRequest.httpMethod = resource.method.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        var uploadData = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        if imageDict != nil {
            for (key, value) in imageDict! {
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).png\"\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append(value)
                uploadData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        if applicationDict != nil {
            for (key, value) in applicationDict! {
                
                
                uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
                if mineType == "pdf" {
                   uploadData.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).pdf\"\r\n".data(using: String.Encoding.utf8)!)
                }else{
                    uploadData.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(key).doc\"\r\n".data(using: String.Encoding.utf8)!)
                }

                uploadData.append("Content-Type: application/\(String(describing: mineType))\r\n\r\n".data(using: String.Encoding.utf8)!)
                uploadData.append(value)
                uploadData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        uploadData.append("--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        do {
            let result = try Alamofire.URLEncoding.default.encode(mutableURLRequest, with: nil)
            return (result, uploadData)
        }
        catch _ {
        }
        
        return nil
    }
    
//MARK: Data Handling
// Convert from NSData to json object
    private func JSONFromData(data: NSData) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil
    }
    
    // Convert from JSON to nsdata
    private func nsdataFromJSON(json: AnyObject) -> NSData?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?
        } catch let myJSONError {
            debugPrint(myJSONError)
        }
        return nil;
    }
    
    //MARK: Error Handling
    private func handleError(response: DataResponse<Any>?, error: NSError, callback:APIServiceFailureCallback) {
        print(response?.response?.statusCode ?? "")
        if let errorCode = response?.response?.statusCode {
            guard let responseJSON = self.JSONFromData(data: (response?.data)! as NSData) else {
                callback(NetworkErrorReason.FailureErrorCode(code: errorCode, message:""), error)
                debugPrint("Couldn't read the data")
                return
            }
            let message = (responseJSON as? NSDictionary)?["err"] as? String ?? "Something went wrong. Please try again."
            callback(NetworkErrorReason.FailureErrorCode(code: errorCode, message: message), error)
        }
        else {
            let customError = NSError(domain: "Network Error", code: error.code, userInfo: error.userInfo)
            print((response?.result.error?.localizedDescription)! as String)
            if let errorCode = response?.result.error?.localizedDescription , errorCode == "The Internet connection appears to be offline." {
                callback(NetworkErrorReason.InternetNotReachable, customError)
            }
            else {
                callback(NetworkErrorReason.Other, customError)
            }
        }
    }
}

//MARK: API Manager Class
class APIManager {
    class func errorForNetworkErrorReason(errorReason: NetworkErrorReason) -> NSError {
        var error: NSError!
        
        switch errorReason {
        case .InternetNotReachable:
            error = NSError(domain: "No Internet", code: -1, userInfo: [kMessage : "The Internet connection appears to be offline."])
        case .UnAuthorizedAccess:
            error = NSError(domain: "Authorization Failed", code: 0, userInfo: [kMessage : "Please Re-login to the app."])
        case let .FailureErrorCode(code, message):
            switch code {
            case 500:
                error = NSError(domain: "Server Error", code: code, userInfo: [kMessage : message])
            default:
                error = NSError(domain: "Please try again!", code: code, userInfo: [kMessage : message])
            }
            
        case .Other:
            error = NSError(domain: "Please try again!", code: 0, userInfo: [kMessage : "Something went wrong!"])
        }
        return error
    }
}

//MARK: Indicator Class
public class Indicator {
    
    public static let sharedInstance = Indicator()
    var blurImg = UIImageView()
    var vwIndicator = SCSkypeActivityIndicatorView()
    static var isEnabledIndicator = true
    
    private init() {
        
        blurImg.frame = UIScreen.main.bounds
        blurImg.backgroundColor = UIColor.black
        blurImg.alpha = 0
        blurImg.isUserInteractionEnabled = true
        
        
        vwIndicator?.frame = CGRect.init(x: 0, y: 0, width: 70, height: 70)
        vwIndicator?.backgroundColor =  UIColor.clear
        vwIndicator?.center = blurImg.center
    }
    
    func showIndicator(){
        
        DispatchQueue.main.async( execute: {
            UIApplication.shared.keyWindow?.addSubview(self.blurImg)
            self.blurImg.fadeInForIndicator()
            UIApplication.shared.keyWindow?.addSubview(self.vwIndicator!)
            self.vwIndicator?.startAnimating()
            self.vwIndicator?.fadeIn()
        })
    }
    
    func hideIndicator(){
        
        DispatchQueue.main.async( execute: {
            self.blurImg.fadeOutForIndicator()
            self.vwIndicator?.fadeOut()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.blurImg.removeFromSuperview()
                self.vwIndicator?.removeFromSuperview()
                self.vwIndicator?.stopAnimating()
            })
        })
    }
}


