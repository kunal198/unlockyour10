//
//  HomeCollectionCell.swift
//  Unlock your 10
//
//  Created by brst on 10/25/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {
    
    @IBOutlet var shadowVw: UIView!
    @IBOutlet var imgProfile: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var imgLock: UIImageView!
    @IBOutlet var btnSendMessage: UIButton!
    
}
