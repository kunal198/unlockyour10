//
//  ReceiverCell.swift
//  Unlock your 10
//
//  Created by brst on 11/7/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ReceiverCell: UITableViewCell {

    //OUTLETS
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var lblTime: UILabel!
    
    @IBOutlet var lblLine: UILabel!
    @IBOutlet var lblMessage: UILabel!
    
    @IBOutlet var btnViewAttachment: UIButton!
    
    @IBOutlet var constraintHeightOfViewAttachtment: NSLayoutConstraint!
    
    @IBOutlet var shadowVw: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
