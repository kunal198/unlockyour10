//
//  ElevatorCell.swift
//  Unlock your 10
//
//  Created by Brst981 on 14/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ElevatorCell: UICollectionViewCell {
    

    @IBOutlet var imagViewElevator: UIImageView!

    @IBOutlet var lblNameDesp: UILabel!


    @IBOutlet var lblDistanceDesp: UILabel!

}
