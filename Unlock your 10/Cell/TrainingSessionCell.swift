//
//  TrainingSessionCell.swift
//  Unlock your 10
//
//  Created by brst on 11/28/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class TrainingSessionCell: UITableViewCell {
//OUTLETS
    @IBOutlet var lblTitle: UILabel!
    
    @IBOutlet var lblDateTime: UILabel!
    
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var createdTimeLbl: UILabel!
    @IBOutlet var btnRequest: UIButton!
    
    @IBOutlet var shadowVw: UIView!
    
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnBook: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var startSessionBtn: UIButton!
    @IBOutlet var joinBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
