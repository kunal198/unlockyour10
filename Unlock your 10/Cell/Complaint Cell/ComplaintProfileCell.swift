//
//  ComplaintProfileCell.swift
//  Unlock your 10
//
//  Created by Brst on 13/12/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ComplaintProfileCell: UITableViewCell {

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cellImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
