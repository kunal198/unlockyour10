//
//  HomeCell.swift
//  Unlock your 10
//
//  Created by brst on 10/23/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

//OUTLETS
    
    @IBOutlet var imgMandator: UIImageView!
    @IBOutlet var shadowView: UIView!
    
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var btnSendMessage: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
