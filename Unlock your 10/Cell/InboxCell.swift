//
//  InboxCell.swift
//  Unlock your 10
//
//  Created by brst on 11/8/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {
//OUTLETS
    
    @IBOutlet var shadowVw: UIView!
    
    @IBOutlet var imgProfile: UIImageView!
    
    @IBOutlet var imgLock: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var lblBadge: UILabel!
    
    @IBOutlet var lblMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
