//
//  UserDataModel.swift
//  Demo
//
//  Created by 123 on 13/07/17.
//  Copyright © 2017 TBI. All rights reserved.
//

import Foundation

struct InsuranceList {  //Insurance List 
    var id: String?
    var provider: String?
  
}

extension UserVM {
    func parseUserData(responseDict: JSONDictionary!, type: String) {
        var data: JSONDictionary?
        data = responseDict[APIKeys.kData] as? JSONDictionary
        if data != nil {
            DataManager.userId              = (data![APIKeys.kUserId] as! Int)
            DataManager.email               = data![APIKeys.kEmailId] as? String
            DataManager.token               = data![APIKeys.kToken] as? String
            DataManager.name                = data![APIKeys.kName] as? String
            
            if data![APIKeys.kUserRole] as? Int == 0{
                DataManager.role                = 2
            }else{
                DataManager.role                = data![APIKeys.kUserRole] as? Int
            }
            
            DataManager.elevatorPitch       = data![APIKeys.kElevatorPitch] as? String
            DataManager.actionPlan1         = data![APIKeys.kActionPlans1] as? String
            DataManager.actionPlan2         = data![APIKeys.kActionPlans2] as? String
            DataManager.actionPlan3         = data![APIKeys.kActionPlans3] as? String
            DataManager.userImage           = data![APIKeys.kProfilePic] as? String
            DataManager.careerField     = data![APIKeys.kCareer] as? String

           //test the user role
            if  DataManager.role  == 3 {
                
                if type == "updateProfile" {
                    DataManager.yearOfExp       = data!["experience"] as? String
                }else{
                    DataManager.yearOfExp       = data![APIKeys.kYearsOfExp] as? String
                }
                DataManager.description     = data![APIKeys.kDescription] as? String
                DataManager.resume          = data![APIKeys.kResume] as? String
            }else{
                
                DataManager.isPackagePurchase = data![APIKeys.kIsPackagePurchased] as? Int
                
                DataManager.freeTraining = data![APIKeys.kAccessTraining] as? Int
            }
        
        }
    }
}
