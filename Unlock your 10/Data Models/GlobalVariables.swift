//
//  GlobalVariables.swift
//  Unlock your 10
//
//  Created by Brst on 10/01/18.
//  Copyright © 2018 The Brihaspati Infotech. All rights reserved.
//

import Foundation
import UIKit
//Menter Video
var sessionTrainingID = Int()
var twillioID = String()
var sessionNameUser = String()
var TwilioAccessToken = String()
var VideoMenterID = String()
// MARK: - Singleton

class VideoSingleton: NSObject {
    var CheckVideoFinishStatus = Bool()
    
    class var sharedInstance: VideoSingleton {
        struct Static {
            static let instance: VideoSingleton = VideoSingleton()
        }
        return Static.instance
    }
}
func updateTwillioToken(completion:@escaping (_ result: Bool) -> Void) {
    
    var parametersDict = JSONDictionary()
    parametersDict[APIKeys.kUserId]         = DataManager.userId
    print(parametersDict)
    guard let serviceUrl = URL(string: "http://beta.brstdev.com/unlockyour/api/web/v1/user/get_twilio_token")
        else { return }
    var request = URLRequest(url: serviceUrl)
    request.httpMethod = "POST"
    request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    guard let httpBody = try? JSONSerialization.data(withJSONObject: parametersDict, options: []) else {
        return
    }
    request.httpBody = httpBody
    
    let session = URLSession.shared
    session.dataTask(with: request) { (data, response, error) in
        if let response = response {
            print(response)
        }
        if let data = data {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                
                TwilioAccessToken = (json as AnyObject).object(forKey: "data") as! String
                completion(true)
            }catch {
                completion(false)
                print(error)
            }
        }
        }.resume()
}
