//
//  AppDelegate.swift
//  Unlock your 10
//
//  Created by brst on 10/9/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//com.tbi.Unlock-your-10
//com.new.Unlock-your-10

import UIKit
import CoreData
import FBSDKLoginKit
import IQKeyboardManagerSwift
import UserNotifications
import GooglePlaces
import GoogleMaps

protocol ChatVCDelegate{
    func didTapOnNotification(data:NSDictionary)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,UIAlertViewDelegate {
    
    var window: UIWindow?
    var nvc: UINavigationController!
    var delegate:ChatVCDelegate! = nil
    var leftMenuProtocol:LeftMenuProtocol!
    var storyboard:UIStoryboard!
    var menuStoryboard:UIStoryboard!
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarView?.backgroundColor = .white
        IQKeyboardManager.sharedManager().enable = true
        
        GMSPlacesClient.provideAPIKey("AIzaSyC94XRsGzfQa2-T-b5LCKjdcq7GHXQT5Nw    ")
        GMSServices.provideAPIKey("AIzaSyC94XRsGzfQa2-T-b5LCKjdcq7GHXQT5Nw    ")
        UserDefaults.standard.object(forKey: "savedTwilioCheckVal")
        //check if user already login
        if DataManager.isUserLoggedIn == true {
            createMenu()
        }
        //set up the push notification
        intigrateNotificationSettings(application:application)
        
        let remoteNotif = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
        if remoteNotif != nil  {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.notificationMethod(userInfo:remoteNotif!)
            }
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    //MARK: Menu
    func createMenu()  {
        
        storyboard = UIStoryboard(storyboard: .Main)
        menuStoryboard = UIStoryboard(storyboard: .Menu)
        let mainViewController = storyboard.instantiateViewController(withIdentifier:kHomeVC) as! HomeVC
        let leftViewController = storyboard.instantiateViewController(withIdentifier: kLeftMenuVC) as! LeftMenuVC
        
        nvc = UINavigationController(rootViewController: mainViewController)
        nvc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        leftViewController.homeVC = nvc
        mainViewController.leftMenuself = leftViewController
        
        let slideMenuController =  ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController )
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = leftViewController
        
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        
        self.window?.layer.add(transition, forKey: nil)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }
    
    
    //MARK:
    //MARK: Notification setting.
    func intigrateNotificationSettings(application:UIApplication) {
        //code for accessing device token
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [UNAuthorizationOptions.sound ], completionHandler: { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    
                }
            })
        } else {
            let settings  = UIUserNotificationSettings(types: [UIUserNotificationType.alert , UIUserNotificationType.badge , UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
    }
    
    
    //MARK:
    //MARK: Open URL Method
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    //MARK:
    //MARK: Register Notification Method
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        DataManager.deviceToken = token
        print(token)
    }
    //MARK:
    //MARK: Notification Methods
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let VisibleVC = UIApplication.topViewController()
        let apsDict = notification.request.content.userInfo[APIKeys.kAps] as! NSDictionary
        print(apsDict)
        let dataDict = apsDict[APIKeys.kData] as! NSDictionary
        
        print(dataDict)
        if (VisibleVC?.isKind(of: ChatVC.self))! {
            if UserVM.sharedInstance.threadId == dataDict[APIKeys.kThreadId] as! String {
                delegate.didTapOnNotification(data: dataDict)
                completionHandler([.sound])
            }else{
                completionHandler([.alert,.sound])
            }
            
        }else{
            completionHandler([.alert,.sound])
            
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        notificationMethod(userInfo:response.notification.request.content.userInfo as NSDictionary)
        
        completionHandler()
    }
    
    func notificationMethod(userInfo:NSDictionary)  {
        
        let VisibleVC = UIApplication.topViewController()
        let apsDict = userInfo[APIKeys.kAps] as! NSDictionary
        let dataDict = apsDict[APIKeys.kData] as! NSDictionary
        print(dataDict)
        let type = dataDict.value(forKey: "type") as! NSString
        
        if type .isEqual(to: "date") {
            
            _ =  VisibleVC?.navigationController?.popToRootViewController(animated: false)
            
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            // home.otherUserId = Int(dataDict[APIKeys.kSenderId] as! String)!
            UIApplication.topViewController()?.navigationController?.pushViewController(homeVC, animated: false)
        }
        else if type .isEqual(to: "training_session_start") || type .isEqual(to: "training_session_done")  {
            _ =  VisibleVC?.navigationController?.popToRootViewController(animated: false)
           
            
            let videoVC = menuStoryboard.instantiateViewController(withIdentifier: "UserVideoVC") as! UserVideoVC
            VideoMenterID = dataDict[APIKeys.kSenderId] as! String
            sessionTrainingID = dataDict[APIKeys.KTraining_sess_id] as! Int

            UIApplication.topViewController()?.navigationController?.pushViewController(videoVC, animated: false)
        }
        else {
            if (VisibleVC?.isKind(of: ChatVC.self))! {
                _ =  VisibleVC?.navigationController?.popToRootViewController(animated: false)
                
                let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                chatVC.otherUserId = Int(dataDict[APIKeys.kSenderId] as! String)!
                UIApplication.topViewController()?.navigationController?.pushViewController(chatVC, animated: false)
                
            }else{
                //call the left menu delegate
                if let menu = LeftMenu(rawValue: 1) {
                    if self.leftMenuProtocol != nil{
                        
                        self.leftMenuProtocol.changeViewController(menu)
                        
                        let chatVC = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                        chatVC.otherUserId = Int(dataDict[APIKeys.kSenderId] as! String)!
                        UIApplication.topViewController()?.navigationController?.pushViewController(chatVC, animated: false)
                        
                    }
                }
            }
        }
    }
    
    //MARK:
    //MARK: Receive notification before IOS 10
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    //        print(userInfo)
    //
    //    }
    
    
    
    
    
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Unlock_your_10")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

