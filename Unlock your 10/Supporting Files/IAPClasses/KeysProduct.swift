//
//  KeysProduct.swift
//  Unlock your 10
//
//  Created by brst on 11/20/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import Foundation


public struct KeysProduct {
    
    public static let twoKeys        = "com.unlock2Keys"
    public static let fiveKeys       = "com.unlock5Keys"
    public static let tenKeys        = "com.unlock10Keys"
    public static let fifteenKeys    = "com.unlock15Keys"
    public static let twentyKeys     = "com.unlock20Keys"
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [KeysProduct.fiveKeys,KeysProduct.twoKeys,KeysProduct.tenKeys,KeysProduct.fifteenKeys,KeysProduct.twentyKeys]
    
    public static let store = IAPHelper(productIds: KeysProduct.productIdentifiers)
}

public struct PackageProduct {
    
    public static let CEOKeys        = "com.unlockCEOKey"
    public static let MANAGERKeys    = "com.unlockMANAGERKey"
    public static let STAFFKeys      = "com.unlockSTAFFKey"

    fileprivate static let productIdentifiers: Set<ProductIdentifier> = [PackageProduct.CEOKeys,PackageProduct.MANAGERKeys,PackageProduct.STAFFKeys]
    
    public static let store = IAPHelper(productIds: PackageProduct.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
