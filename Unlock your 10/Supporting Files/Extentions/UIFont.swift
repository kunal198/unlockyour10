//
//  UIFont.swift
//  Roasted
//
//  Created by Manish Gumbal on 13/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    enum HealthSplashFont: String {
        case medium = "Lato-Medium"
        case regular = "Lato-Regular"
        case light = "Lato-Light"
        case bold = "Lato-Bold"
//        case lightItalic = "Roboto-LightItalic"
//        case mediumItalic = "Roboto-MediumItalic"
        
        func fontWithSize(size: CGFloat) -> UIFont {
//            let sysFont: UIFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
//            return sysFont;
             return UIFont(name: rawValue, size: size)!
        }
    }
}
