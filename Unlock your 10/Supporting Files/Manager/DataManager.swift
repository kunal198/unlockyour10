//
//  DataManager.swift
//  Roasted
//
//  Created by Manish Gumbal on 21/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation
import StoreKit
class DataManager{
    
    static var userId:Int? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: kUserId)
        }
    }

    static var keysBalance:Int? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: KKeysBalance)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: KKeysBalance)
        }
    }
    static var freeTraining:Int? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: KFreeTraining)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: KFreeTraining)
        }
    }
    static var isPackagePurchase:Int? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: KIsPackagePurchase)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: KIsPackagePurchase)
        }
    }
    
    static var email:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserEmail)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserEmail)
        }
    }
    
    static var name:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserName)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserName)
        }
    }
    
    static var userImage:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserImage)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserImage)
        }
    }
    
    static var resume:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kResume)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kResume)
        }
    }
        
    static var deviceToken: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kDeviceToken)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kDeviceToken)
        }
    }
    
    static var token: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kToken)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kToken)
        }
    }
    
    static var elevatorPitch: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kElevatorPitch)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kElevatorPitch)
        }
    }
    
    static var actionPlan1: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kActionPlan1)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kActionPlan1)
        }
    }
    
    static var actionPlan2: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kActionPlan2)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kActionPlan2)
        }
    }
    static var actionPlan3: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kActionPlan3)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kActionPlan3)
        }
    }
    static var careerField: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kCareerField)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kCareerField)
        }
    }
    static var yearOfExp: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kYearOfExp)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kYearOfExp)
        }
    }
    
    static var description: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kDescription)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kDescription)
        }
    }
    
    static var role: Int?{
        set{
            UserDefaults.standard.setValue(newValue, forKey: kRole)
            UserDefaults.standard.synchronize()

        }
        get {
            return UserDefaults.standard.integer(forKey:kRole)
        }
    }
    
    static var isUserLoggedIn:Bool?{
        set {
            UserDefaults.standard.setValue(newValue, forKey: kIsUserLoggedIn)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kIsUserLoggedIn)
        }
    }
    
    
    static var patientList: [NSDictionary]? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kPatientList)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: kPatientList) as? [NSDictionary]
        }
    }
}

