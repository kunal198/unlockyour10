//
//  AppConstants.swift
//  Roasted
//
//  Created by Manish Gumbal on 16/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation


//****************************   MARK: Data Manager Constants    ***********************
let kUserId = "kUserId"
let kFName = "kFName"
let kLName = "kLName"

let kUserImage  = "kUserImage"
let kResume     = "kResume"

let kUserEmail  = "kUserEmail"
let kUserName   = "kUserName"

let kIsFBLoggedIn   = "kIsFBLoggedIn"
let kDeviceToken    = "kDeviceToken"
let kToken          = "kToken"
let kElevatorPitch  = "kElevatorPitch"
let kActionPlan1    = "kActionPlan1"
let kActionPlan2    = "kActionPlan2"
let kActionPlan3    = "kActionPlan3"

let kCareerField    = "kCareerField"

let kDescription    = "kDescription"

let kYearOfExp      = "kYearOfExp"


let kVerificationCode = "kVerificationCode"

let kRole = "kRole"
let kIsNotificationOn = "kIsNotificationOn"
let kIsUserLoggedIn = "kIsUserLoggedIn"
let kPatientList = "kPatientList"
let kProductList = "KProductList"
let KKeysBalance = "KKeysBalance"
let KIsPackagePurchase = "isPackagePurchase"
let KFreeTraining = "KFreeTraining"






//****************************   MARK: Common Constants    ***********************
let BASE_API_URL = "http://beta.brstdev.com/unlockyour/api/web/v1/user/"


let BASE_IMAGE_URL = "http://14.141.82.35/Server10/roastapp/public/upload"
let BASE_USER_URL = "http://beta.brstdev.com/unlockyour/api/web/v1/user/"

//MARK: Google API key
let GOOGLE_API_KEY = "AIzaSyD5LsOj6dBm4R1fm0rygc797qW62Iw8f-A"

//let GOOGLE_API_KEY = "AIzaSyD-RILx7SB4la4eHG5_J8mTs6yNxXuFgL0"

//MARK: Color
let kTextColor = "555555"
let kLightPurple = "574155"
let kPurple = "3C273A"
let kStatusBarColor = "C4C4C4"
let kLightGray = "7A7A7A"
let KWhiteColor = "D2D2D0"

let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

//MARK:
//MARK: Navigation Title


//MARK:
//MARK: TAbleViewCell

let kCell                   = "Cell"
let kSenderCell               = "SenderCell"
let kReceiverCell              = "ReceiverCell"
let kVideoCell              = "VideoCell"
let kMemorandumsCell        = "MemorandumsCell"
let kComplaintsCell        = "ComplaintsCell"
let kComplaintsInfoCell        = "ComplaintsInfoCell"
let kSubmitTrainingCell        = "SubmitTrainingCell"
let kTrainingSessionCell        = "TrainingSessionCell"
let kNotificationCell        = "NotificationCell"




let kHeaderCell             = "HeaderCell"
let kAppointmentDetailCell  = "AppointmentDetailCell"


// MARK: Logout String

let kLogoutString   = "Are you sure you want to logout?"


let kkeysDeduct   = "Your 10 Keys will deduct from your key's balance when you click on Ok button."
let kJoinTraining   = "Are you sure you want to join this training?"

let kNotEnoughKeys   = "Oops!! You don't have enough keys for send request. Please click on OK button for purchase keys."

let kNotEnoughKeysSendMessage   = "You do not have enough keys to send a message. Please purchase a package or keys first."
let kNotPurchasePackage  = "YPlease purchase package to see complaints."

// MARK:
// MARK:Alert Action 

let kYes       =  "Yes"
let kNo        =  "No"
let kOk        =  "Ok"
let kCancel    =  "Cancel"


// MARK:
// MARK:ViewControllers

let kLeftMenuVC            = "LeftMenuVC"
let kNotificationVC         = "NotificationVC"
let kLoginVC                = "LoginVC"
let kHomeVC                 = "HomeVC"
let kCustomTabbarVC         = "CustomTabBarVC"
let kLoginNavigationVC      = "LoginNavigationVC"
let kCreateMessageVC        = "CreateMessageVC"
let kMyProfile              = "MyProfile"
let kProfileInfoVC          = "ComplaintProfileInfoVC"
let kCareerProfile        = "CareerProfile"
let kIndexPath              = "indexPath"


//MARK: Titles



// MARK: Strings
// Home

//MARK:
//MARK: Segue Constants

//LOGIN
let kLoginToForgot           = "loginToForgotPassword"
let kLoginToSignUP           = "LoginToSignUp"


//SIGNUP
let kInboxToChat              = "InboxToChat"


//INBOX


//PROFILE

let kProfileToEdit              = "ProfileToEdit"
let kMentorProfileToEditProfile = "mentorProfileToEditProfile"

//REVIEWS AND RATING

let kReviewsToEditReviews    = "reviewsToEditReviews"

let kKeysToPackage    = "KeysToPackage"


//PATIENT
let kPatientsToAddPatient    = "patientsToAddPatient"
let kPatientsToEdit          = "patientsToEdit"

//EDIT PATIENT

let kUserPrfileToEditProfile = "userPrfileToEditProfile"

// ADD PATIENT

let kComplaintProfileInfoVC  = "ComplaintProfileInfoVC"

//COMPLAINTS
let kComplaintsToComProInfo  = "ComplaintsToComProInfo"
let kcomplaintInfoArray      = "complaintInfoArray"

//TRAINING SESSION
let kTrainingSessionToLeaveTrainingSession  = "TrainingSessionToLeaveTrainingSession"
let kLeaveTrainingSession    = "LeaveTrainingSession"
//MESSAGES

let kEnterEmail                 = "Please enter your email address."
let kEnterPaypalEmail           = "Please enter your paypal email address."
let kEnterEmailFirst            = "Please enter email address first."


let kEnterPassword              = "Please enter password."
let kEnterValidEmail            = "Please enter valid email address."
let kEnterReferredValidEmail    = "Please enter valid referred email address."
let kSelectAlteastOneAction     = "Please select atleast one action plan."

let kEnterName                  = "Please enter your name."
let kEnterTextFirst             = "Please enter text first."
let kEnterYearOfExp             = "Please enter your years of experience."
let kEnterYourMessage           = "Please enter your message."

let kSelectCareerField          = "Please select career field."
let kSelectUserFirst            = "Please select user first."
let kSelectLimitCross           = "Your maximum number of action plan selected."
let kSelectLessThan5            = "Resume should be less than or equal to 5 MB."
let kSelectAttachLessThan5      = "Attachment should be less than or equal to 5 MB."

let kUploadResume               = "Please upload your resume."
let kBriefStatement             = "Please enter brief statement of minimum 30 characters"

let kEnterConfirmPassword       = "Please enter confirm password."
let kFirstNameLength            = "First name needs to be at least 3 characters."
let kLastNameLength             = "Last name needs to be at least 3 characters."
let kPasswordNeeds8Character    = "Password must contain minimum 3 characters with atleast 1 number and 1 special character(@$!%*?&) with no space."
let kPasswordAndConfirmPassword = "Your password and confirm password does not match."
let kAgreeWithTerms             = "Please agree with Terms & Condition."
let kTokenNotMatched            = "User id or login token not matched"
let kSomeOneLoginToAnotherDevice = "Someone login on another device."
let kSelectCurrentLocation      = "Please select your current location."
let kNoSendMessageFound         = "No sent message found."
let kNoChatFound                = "No Message Found."
let kNoKeysFound                = "Keys not found."
let kNoWihdrawalKeysFound       = "Sorry! You do not have enough keys to make a withdrawal request."
let kNoTrainingSession          = "No training sessions found."
// LunchBreak
let kLunchBreak               = "LunchBreak"
let kCuisineName              = "Please enter cuisine name."
let kLocation                 = "Please enter location."
let kDate                     = "Please select date."
let kRequestUserName          = "Please enter username."
let kSubmit                   = "Submit"
let kSearch                   = "Search"

//Video
let kmentorVideoVC    = "MentorVideoVC"
let kuserVideoVC      = "UserVideoVC"
