//
//  APIKeys.swift
//  Roasted
//
//  Created by Manish Gumbal on 21/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

class APIKeys {
   
    
    
    
    //Common
    static let kReturn     = "return"
    static let kId         = "id"
    static let kMessage    = "message"
    static let kSuccess    = "success"
    static let kData       = "data"
    static let kDate       = "date"
    static let kAps       = "aps"

    
    //User
    static let kUserId            = "user_id"
    static let kEmailId           = "email"
    static let kOtherUserEmail    = "other_user_email"

    
    static let kUserName          = "username"
    static let kRating            = "rating"
    static let kName              = "name"
    static let kQuery             = "query"
    static let kPaypalEmail       = "paypal_email"



    static let kUserRole          = "role"
    static let kElevatorPitch     = "elevator_pitch"
    static let kActionPlans1      = "action_plans1"
    static let kActionPlans2      = "action_plans2"
    static let kActionPlans3      = "action_plans3"


    static let kPassword          = "password"
    static let kDeviceToken       = "device_token"
    static let kToken             = "token"
    static let kPage              = "page"
    static let kPagination        = "pagination"
    static let kUnreadCount       = "unread_count"
    static let kNextpage          = "nextpage"
    static let kCurrentpage       = "currentpage"
    static let kCreated_at        = "created_at"
    static let kOtherUserId       = "other_user_id"
    static let kThreadId          = "thread_id"
    
    //Complaint
    
    static let kComplaint_desc  = "complaint_desc"
    static let kComplaint_id    = "complaint_id"
    static let kCommentComplaint_id    = "comment_complaint_id"
    
    static let kComments         = "comment"
    //chat
    static let kSenderId        = "sender_id"
    
    static let kSenderName      = "sender_name"
    static let kAttachment      = "attachment"


    static let kDeviceType        = "device_type"
    static let kCareer            = "career"
    static let kAddress           = "address"
    static let kYearsOfExp        = "years_of_exp"
    static let kExperience        = "experience"
    static let kReferredEmail     = "referred_email"
    static let kReceiverId        = "receiver_id"
    static let kMentor_id         = "mentor_id"
    static let kTrainingSessions  = "training_sessions"
    static let kAccessTraining  = "access_training"



    
    static let kCountry           = "county"


    static let kDob               = "dob"
    static let kKeyValue          = "key_value"
    static let kKeys              = "keys"
    static let kPackageType       = "package_type"


   
    static let kLatitude          = "lat"
    static let kLongitude         = "longi"
    static let klong              = "lng"



    static let kDescription       = "description"
    static let kDateCreated       = "date_created"
    static let kDateTime          = "date_time"
    
    
    static let kSocialType = "social_type"

    static let kSocialId = "social_id"

  //  static let kDob = "dob"
    static let kLoginType  = "login_type"
    static let kFacebookId = "facebook_id"
    static let kProfilePic = "profile_pic"
    static let kUserInfo   = "user_info"
    static let kResume     = "resume"

    //Settings
    static let kTitle                    = "title"
    static let kComment                  = "comment"
    static let kTrainingSessionTitle     = "training_session_title"
    static let kTrainingSessionDesc      = "training_session_description"

    static let kTrainingSessionId        = "training_session_id"
    static let kTrainingId               = "training_id"
    static let kTrainingSessionDateTime  = "training_session_start_date_time"
    static let kIsRequest                = "is_Request"
    static let kIsPackagePurchased       = "is_package_purchased"
    static let kSessionId                = "session_id"


    // //Lunch Break
    static let KCusineName  = "cuisine_name"
    static let KPlace       = "place"
    
    //Video VC
    
    static let KTwillioID       = "twilio_id"
    static let KTVideoStatus    = "status"
    static let KTrainSessUerID  = "training_session_user_id"
    static let KTraining_sess_id = "training_sess_id"
    
}

