//
//  LeaveTrainingSession.swift
//  Unlock your 10
//
//  Created by Brst on 14/12/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class LeaveTrainingSession: BaseViewController,UITextFieldDelegate,BaseVCDelegate {

    @IBOutlet var titleTxtView: UITextView!
    @IBOutlet var dateTimeTxt: UITextField!
    @IBOutlet var submitButton: UIButton!
    
    var listArray = [NSDictionary]()
    var session_id = Int()
    let datePicker = UIDatePicker()
    var dateStr: String!
    var leftMenuProtocol:LeftMenuProtocol!
    var leftmenuSelf:UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeNavigationBarItem()
        // Do any additional setup after loading the view.
        
    //  titleTxtView.generateShadowUsingBezierPath(radius: 1.5, opacity: 0.25,allCorner: false)
    //dateTimeTxt.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
        
     leftMenuProtocol = leftmenuSelf as! LeftMenuProtocol!
        
        titleTxtView.delegate = self
        dateTimeTxt.delegate = self
        pickerDelegate = self
       dropShadow()
    }
    // OUTPUT 1
    func dropShadow() {
        self.titleTxtView?.layer.cornerRadius = 5
        self.titleTxtView?.layer.borderColor = UIColor .gray .cgColor
        self.titleTxtView?.layer.borderWidth = 1
        self.titleTxtView.layer.masksToBounds = true
    }

    func didSelectDatePicker(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        //dateFormatter.timeZone = TimeZone.current //Current time zone
         dateStr = dateFormatter.string(from: date) //according to date format your date string
        print(dateStr) //Convert String to Date
    }
    func didClickOnDoneButton() {
        self.view.endEditing(true)
        if dateStr != nil{
        let fullNameArr = dateStr.components(separatedBy: "+")
        let name: String!  = fullNameArr[0]
        
        dateTimeTxt.text = name
        }
        else
        {
           dateTimeTxt.text =  Date().toString() // convert date to string with userdefined format.

        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
       // self.viewWillAppear(true)
        submitButton.setTitle("Submit", for: .normal)
        if UserDefaults.standard.object(forKey: "EditTrainigSession") != nil
        {
            let indexPath = UserDefaults.standard.object(forKey: kIndexPath) as! Int
            listArray = UserDefaults.standard.object(forKey: "sessionListArray") as! [NSDictionary]
            let dicData = listArray[indexPath]
            session_id = dicData.object(forKey: APIKeys.kTrainingSessionId) as! Int
            let title = dicData.object(forKey: "training_session_title") as! String
            let dateTime = dicData.object(forKey: "training_session_start_date_time") as! String
            titleTxtView.text = title
            dateTimeTxt.text = dateTime
            
            UserDefaults.standard.removeObject(forKey: "sessionListArray")
            submitButton.setTitle("Update", for: .normal)
            titleTxtView.textColor = UIColor.black
        }
        else{
            textview()
        }
        dateTimeTxt.backgroundColor = .clear
       customizeUI()
    }
    func customizeUI() {
        hideNavigationBar()
    }
    func textview()
    {
        dateTimeTxt.text = ""
        self.dateTimeTxt.placeholder = "Enter Date And Time of Live Recording:"
        titleTxtView.text = "Enter Title And Description:"
        titleTxtView.textColor = UIColor.lightGray
    }
   
    
    func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }

    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        backButtonAction()
    }
    @IBAction func submitTrainingSesson(sender: UIButton) {
        if titleTxtView.text == "Enter Title And Description:" || titleTxtView.text == "" {
            self.showAlert(message: "Please Enter Title", title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                
            })
        }
        else if dateTimeTxt.text == "Enter Date And Time of Live Recording:" || dateTimeTxt.text == "" {
            self.showAlert(message: "Please Select Date And Time", title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                
            })
        }
        else {
        if UserDefaults.standard.object(forKey: "EditTrainigSession") != nil
        {
        callWebserviceForEditTrainigSession()
        }
        else {
            callWebserviceForAddTrainigSession()
        }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LeaveTrainingSession
{
    //Call Api For Edit Training Session
    func callWebserviceForEditTrainigSession() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kTitle]          = titleTxtView.text
        parametersDict[APIKeys.kDescription]    = ""
        parametersDict[APIKeys.kDateTime]       = dateTimeTxt.text
        parametersDict[APIKeys.kSessionId]      = session_id
        
        print(parametersDict)
        
        UserVM.sharedInstance.editTrainingSession(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                
                // self.sessionListArray = UserVM.sharedInstance.sessionListArray
                //print(self.sessionListArray)
                self.showToastWithMessage(message: message!)
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5)
                {
                    self.backButtonAction()
                }
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    
    //Call Api For Add Training Session
    func callWebserviceForAddTrainigSession() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kTitle]          = titleTxtView.text
        parametersDict[APIKeys.kDescription]    = "abc"
        parametersDict[APIKeys.kDateTime]       = dateTimeTxt.text
        //parametersDict[APIKeys.kSessionId]      = session_id
        
        print(parametersDict)
        
        UserVM.sharedInstance.addTrainingSession(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.textview()
                
                 self.showToastWithMessage(message: message!)
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5)
                {
                self.backButtonAction()
                }
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
}
extension LeaveTrainingSession: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter Title And Description:"
            textView.textColor = UIColor.lightGray
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        setDatePickerView(textField: dateTimeTxt, mode: 1)
        return true
    }
   
}
//extension Date {
//    func toString() -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        return dateFormatter.string(from: self)
//    }
//}

