//
//  SubmitTrainingIdea.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 16/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
//Book Request For Training Sessions :
class SubmitTrainingIdea: BaseViewController {
 
    // Outlets
    @IBOutlet var trainingTbl: UITableView!

    var sessionListArray = [NSDictionary]()
    var sessionDetailsDic = NSDictionary()
    @IBOutlet var createSessionBtn: UIButton!
     @IBOutlet var bookLabel:UILabel!
    @IBOutlet var createSessionIco: UIImageView!
     var leaveTrainingSession:LeaveTrainingSession!
     var leftMenuProtocol:LeftMenuProtocol!
     var mentorVideoVC:MentorVideoVC!
     var userVideoVC:UserVideoVC!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    //MARK: CustomizeUI
    func customizeUI()  {

        self.hideNavigationBar()
        self.addLeftMenuGesture()
        //Leave Training Session
        let storyboard = UIStoryboard(storyboard: .Menu)
        leaveTrainingSession = storyboard.instantiateViewController(withIdentifier: kLeaveTrainingSession) as! LeaveTrainingSession
        // Mentor Video VC
        mentorVideoVC = storyboard.instantiateViewController(withIdentifier: kmentorVideoVC) as! MentorVideoVC
        //User Video VC
        userVideoVC = storyboard.instantiateViewController(withIdentifier: kuserVideoVC) as! UserVideoVC
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customizeUI()
        if DataManager.role == 2 {
            createSessionBtn.isHidden = true
            createSessionIco.isHidden = true
            bookLabel.text = "Booked Training Sessions:"
            callWebserviceForGetRequestTrainigSession()
        }
        else {
            bookLabel.text = "Edit And Delete Training Sessions:"
            createSessionBtn.isHidden = false
            createSessionIco.isHidden = false
            callWebserviceForGetTrainigSession()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
       // callWebserviceForBooking()
        
    }
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    @IBAction func openTrainigSession(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "EditTrainigSession")
       // animationPushWithIdentifier(identifier: "openTrainigSession")
        animationPushWithViewController(viewController: leaveTrainingSession)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: UITableView Methods
extension SubmitTrainingIdea: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.sessionListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kSubmitTrainingCell, for: indexPath) as! TrainingSessionCell
        customizeCell(indexPath: indexPath, cell: cell)
        let dicData = self.sessionListArray[indexPath.row]
        print(dicData)
        cell.btnBook.tag = indexPath.row
        cell.btnCancel.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        
          cell.btnBook.addTarget(self, action: #selector(bookNowBtn(sender:)), for: .touchUpInside)
          cell.btnCancel.addTarget(self, action: #selector(cancelBtn(_:)), for: .touchUpInside)
          cell.btnEdit.addTarget(self, action: #selector(editBtn(sender:)), for: .touchUpInside)
          cell.startSessionBtn.addTarget(self, action: #selector(startSessionAction(sender:)), for: .touchUpInside)
          cell.joinBtn.addTarget(self, action: #selector(joinBtnAction(sender:)), for: .touchUpInside)
        
       
        
        if DataManager.role == 3{
            cell.btnBook.isHidden = true
            cell.btnCancel.isHidden = false
            cell.btnEdit.isHidden = false
                        cell.joinBtn.isHidden = true
        //show training data
        cell.lblTitle.text = "Title: "+(dicData.object(forKey: "training_session_title") as? String)!
        
        let fullDate = dicData.object(forKey: "training_session_start_date_time") as? String
        let fullDateArr = fullDate?.components(separatedBy: " ")
        
        let date = fullDateArr![0]
        let time = fullDateArr![1]
        cell.lblDateTime.text = "Date: "+date
        cell.createdTimeLbl.text = "Time: "+time
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
            let sessionDate = dateFormatter.date(from: fullDate!) //according to date format your date string
            let currDate = Date()
            let datee: Date! = sessionDate
            print(datee)
            print(currDate)
            
//            if datee > currDate
//            {
//                cell.startSessionBtn.isHidden = false
//            }
//            else if datee < currDate
//            {
//                cell.startSessionBtn.isHidden = true
//            }
            if datee.compare(currDate) == .orderedDescending {
               cell.startSessionBtn.isHidden = false
            }
            else {
                cell.startSessionBtn.isHidden = true
            }
          
       
        }
        else
        {
            cell.btnBook.isHidden = false
            cell.btnCancel.isHidden = true
            cell.btnEdit.isHidden = true
            
            cell.startSessionBtn.isHidden = true
            
           
            //show training data
            print(self.sessionListArray[indexPath.row])
            print(dicData)
            var arrReqData = [NSDictionary]()
            arrReqData = dicData.object(forKey: "requested_training_session_data") as! [NSDictionary]
             print(arrReqData)
            let arrDic = arrReqData[0]
            
            let title: String = arrDic.value(forKey: "training_session_title") as! String
           cell.lblTitle.text = "Title: "+title
            
            let fullDate = arrDic.object(forKey: "training_session_start_date_time") as? String
            let fullDateArr = fullDate?.components(separatedBy: " ")
            
            let date = fullDateArr![0]
            let time = fullDateArr![1]
            cell.lblDateTime.text = "Date: "+date
            cell.createdTimeLbl.text = "Time: "+time
            cell.btnBook.setTitle("Booked", for: .normal)
            cell.btnBook.isUserInteractionEnabled = false
           //Check to show join button
            let status = arrDic.object(forKey: "training_session_status") as! Int
            if status == 1 {
                cell.joinBtn.isHidden = false
            }
            else {
                cell.joinBtn.isHidden = true
            }
        }
        
         return cell
    }
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
    }
}
extension SubmitTrainingIdea
{   
    
    //Call Api For Get Training Session
    func callWebserviceForGetTrainigSession() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
       
        print(parametersDict)
        
        UserVM.sharedInstance.getTrainingSession(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                
                 self.sessionListArray = UserVM.sharedInstance.sessionListArray
                print(self.sessionListArray)
                self.trainingTbl.reloadData()
                //trainingTbl
               // self.showTrainigData()
                
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    else if message == kNoTrainingSession {
                        DispatchQueue.main.async {
                          self.sessionListArray.removeAll()
                        self.trainingTbl.reloadData()
                        }
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    //Call Api For Delete Training Session
    func callWebserviceForDeleteTrainigSession(session_id:Int) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kSessionId]      = session_id
        
        print(parametersDict)
        
        UserVM.sharedInstance.deleteTrainingSession(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                 self.callWebserviceForGetTrainigSession()
                self.showToastWithMessage(message: message!)
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
       
    //Call Api For Booking
    func callWebserviceForBooking(role: Int,training_id:Int) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = DataManager.role
        parametersDict[APIKeys.kTrainingId]     = training_id
           
        print(parametersDict)
       // ["user_id": 10, "training_id": "2", "token": "1371bbf3fd283fda5ba04389fafdcdc3", "role": "2"]
        UserVM.sharedInstance.bookingRequest(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                
                //self.sessionDetailsDic = UserVM.sharedInstance.sessionDetailsDic
                print(self.sessionDetailsDic)
               self.showToastWithMessage(message: message!)
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }

}
extension SubmitTrainingIdea {
    
    func editBtn(sender:UIButton)
    {
        
        let point = trainingTbl.convert(CGPoint.zero, from: sender)
        if let indexPath = trainingTbl.indexPathForRow(at: point) {
            UserDefaults.standard.set(indexPath.row, forKey: kIndexPath)
            UserDefaults.standard.set(self.sessionListArray, forKey: "sessionListArray")
            UserDefaults.standard.set("EditTrainigSession", forKey: "EditTrainigSession")
        }
        //leaveTrainingSession
        animationPushWithViewController(viewController: leaveTrainingSession)
    }
    @IBAction func cancelBtn(_ sender: UIButton)
    {
        let point = trainingTbl.convert(CGPoint.zero, from: sender)
        if let indexPath = trainingTbl.indexPathForRow(at: point) {
            
            let dicData = self.sessionListArray[indexPath.row]
            let session_id = dicData.object(forKey: APIKeys.kTrainingSessionId) as! Int
            
            callWebserviceForDeleteTrainigSession(session_id: session_id)
        }
    }
    func bookNowBtn(sender:UIButton)
    {
        let point = trainingTbl.convert(CGPoint.zero, from: sender)
        if let indexPath = trainingTbl.indexPathForRow(at: point) {
            
            let dicData = self.sessionListArray[indexPath.row]
            print(dicData)
            let training_id = dicData.object(forKey: APIKeys.kTrainingSessionId) as! Int
            
             callWebserviceForBooking(role:DataManager.role!, training_id: training_id)
        }
    }
    func startSessionAction(sender:UIButton)
    {
        let point = trainingTbl.convert(CGPoint.zero, from: sender)
        if let indexPath = trainingTbl.indexPathForRow(at: point) {
            
            let dicData = self.sessionListArray[indexPath.row]
            print(dicData)
             sessionTrainingID = dicData.object(forKey: APIKeys.kTrainingSessionId) as! Int
            
            //Video Menter ID
            let mentID: String! = String(describing: dicData.object(forKey: APIKeys.KTrainSessUerID) as! Int)
            VideoMenterID = mentID
            
             animationPushWithViewController(viewController: mentorVideoVC)
        }
    }
    
    func joinBtnAction(sender:UIButton)
    {
        let point = trainingTbl.convert(CGPoint.zero, from: sender)
        if let indexPath = trainingTbl.indexPathForRow(at: point) {
            
            let dicData = self.sessionListArray[indexPath.row]
           
            let arrData = dicData.object(forKey: "requested_training_session_data") as! NSArray
            let requestDic = arrData[0] as! NSDictionary
             print(requestDic)
            //Video Menter ID            
            let mentID: String! = String(describing: requestDic.object(forKey: APIKeys.KTrainSessUerID) as! Int)
            VideoMenterID = mentID
            
            //Session ID
            sessionTrainingID = requestDic.object(forKey: APIKeys.kTrainingSessionId) as Any as! Int
            animationPushWithViewController(viewController: userVideoVC)
            
        }
    }
    
}
extension SubmitTrainingIdea
{
    
    //Call Api For Get Request Training Session
    func callWebserviceForGetRequestTrainigSession() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        
        print(parametersDict)
        
        UserVM.sharedInstance.requestSession(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                
                self.sessionListArray = UserVM.sharedInstance.sessionListArray
                print(self.sessionListArray)
                self.trainingTbl.reloadData()
                //trainingTbl
                // self.showTrainigData()
                
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
}

