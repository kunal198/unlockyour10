//
//  Notifications.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 17/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class Notifications: BaseViewController {

    //Outlets
    
    @IBOutlet var notificationTbl: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    override func viewDidLayoutSubviews() {
        notificationTbl.reloadData()
    }
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: UITableView Methods
extension Notifications: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {

    }
}
