//
//  AdvancedNotice.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 14/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class AdvancedNotice: BaseViewController {
   
    // Outlets

    @IBOutlet var jobBackgrounView: UIView!
    @IBOutlet var locationBackgrounView: UIView!
    @IBOutlet var jobFieldTxt: UITextField!
    @IBOutlet var locationTxt: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    override func viewDidLayoutSubviews() {
        jobBackgrounView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        locationBackgrounView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
