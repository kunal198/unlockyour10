//
//  ContactSupport.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 14/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ContactSupport: BaseViewController {

    // Outlets
    
    @IBOutlet var msgBackgrounView: UIView!
    @IBOutlet var msgTxt: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
 
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    override func viewDidLayoutSubviews() {
        msgBackgrounView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: UITextView Delegates

extension ContactSupport: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
        if(textView.text == "Type your Message Here:") {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
        if(textView.text == "") {
            textView.text = "Type your Message Here:"
        }
    }
}
