//
//  MyProfile.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 17/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class MyProfile: BaseViewController {

    //Outlets
    @IBOutlet var imgProfile: UIImageView!
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblCareerField: UILabel!
    @IBOutlet var lblExperience: UILabel!
    @IBOutlet var trainingSessionTbl: UITableView!
    
    @IBOutlet var btnMenuOutlet: UIButton!
    
    @IBOutlet var lblNoTraingFound: UILabel!
    //variables
    var iscommingFrom = String()
    var mentorId = Int()
    
    var mentorDetails = NSDictionary()
    var trainingArray = [NSDictionary]()
    
    var createMessageVC:CreateMessageVC!
    var leftMenuProtocol:LeftMenuProtocol!
    var leftmenuSelf:UIViewController!
   
//MARK: View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard = UIStoryboard(storyboard: .Menu)
         createMessageVC = storyboard.instantiateViewController(withIdentifier: kCreateMessageVC) as! CreateMessageVC
        leftMenuProtocol = leftmenuSelf as! LeftMenuProtocol!
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customizeUI()
        
    }
    
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
        // check iscomming from home or menu
        if iscommingFrom == kHomeVC {
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }else{
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        }
        callWebserviceForGetMentorProfile() // call the webservice for get mentor profile 
    }

// Set the values
    
    func setMentorProfile() {
        lblName.text = "Name: \(String(describing: mentorDetails[APIKeys.kName] as! String))"
        
        lblCareerField.text = "Career Field: \(String(describing: mentorDetails[APIKeys.kCareer] as! String))"
        
        lblExperience.text = "Experience: \(String(describing: mentorDetails[APIKeys.kYearsOfExp] as! String)) Yrs"
        
        imgProfile.sd_setImage(with: URL(string:mentorDetails[APIKeys.kProfilePic] as! String), placeholderImage: #imageLiteral(resourceName: "dummyUser"))
        trainingArray = mentorDetails[APIKeys.kTrainingSessions] as! [NSDictionary]
        trainingSessionTbl.reloadData()
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        // check iscomming from home or menu
        if iscommingFrom == kHomeVC {
            backButtonAction()
        }else{
            slideMenuController()?.toggleLeft()
        }
    }
    
    @IBAction func btnSendMessageAction(_ sender: Any) {
        
        createMessageVC.iscommingFrom = kHomeVC
        createMessageVC.nameText = mentorDetails[APIKeys.kName] as! String
        createMessageVC.receiverId = mentorDetails[APIKeys.kUserId] as? Int
        animationPushWithViewController(viewController: createMessageVC)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: UITableView Methods
extension MyProfile: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trainingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kTrainingSessionCell, for: indexPath) as! TrainingSessionCell
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:TrainingSessionCell)  {
        cell.shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        let dataDict = trainingArray[indexPath.row]
        
        cell.lblTitle.text = dataDict[APIKeys.kTrainingSessionTitle] as? String
        //cell.lblDescription.text = dataDict[APIKeys.kTrainingSessionDesc] as? String
        let dateTime = dataDict[APIKeys.kTrainingSessionDateTime] as? String
        cell.lblDateTime.text = dateTime?.dateFromString(format: .dateTime)?.stringFromDate(format: .dmyDate)
        cell.btnRequest.addTarget(self, action: #selector(self.btnRequestAction), for: UIControlEvents.touchUpInside)
        cell.btnRequest.tag = indexPath.row
        
        if dataDict[APIKeys.kIsRequest] as! Int == 0 {
            cell.btnRequest.isEnabled = true
            cell.btnRequest.setTitle("Request Now", for: .normal)
        }else{
            cell.btnRequest.isEnabled = false
            cell.btnRequest.setTitle("Request Accepted", for: .normal)
        }
    }
    
    func btnRequestAction(sender:UIButton)  {
        
        let dataDict = trainingArray[sender.tag]
        let trainingId = dataDict[APIKeys.kTrainingSessionId] as! Int
        var message = String()
        
        if DataManager.freeTraining! <= 0{
            message = kkeysDeduct
        }else
        {
            message = kJoinTraining
        }
        
        self.showAlert(message: message, title: "", otherButtons: [kOk:{action in
          
            self.callWebserviceForRequestTraining(trainingId: trainingId,index: sender.tag)
            }], cancelTitle: kCancel, cancelAction:{ (action) in
        })
    }
    
}

extension MyProfile {
    
    func callWebserviceForGetMentorProfile() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 3
        parametersDict[APIKeys.kMentor_id]      = mentorId

        
        UserVM.sharedInstance.getMentorProfile(profileDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.lblNoTraingFound.isHidden = true
                self.mentorDetails = UserVM.sharedInstance.mentorDetails
                if ((self.mentorDetails[APIKeys.kTrainingSessions] as! [NSDictionary]).count == 0){
                    self.lblNoTraingFound.isHidden = false
                }
                self.setMentorProfile()
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    func callWebserviceForRequestTraining(trainingId:Int,index:Int) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = DataManager.role
        parametersDict[APIKeys.kTrainingId]     = trainingId
        
        KeysVM.sharedInstance.requestTraining(trainingDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                if DataManager.freeTraining! > 0{
                    DataManager.freeTraining! -= 1
                }
                
                self.trainingArray.remove(at: index)
                self.trainingArray.insert(KeysVM.sharedInstance.trainingData, at: index)
                self.trainingSessionTbl.reloadData()
                self.showAlert(message: message, title: "")
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }else if message == kNotEnoughKeys{
                        
                        self.showAlert(message: kNotEnoughKeys, title: "", otherButtons: [kOk:{action in
                            self.backButtonAction()
                            //call the left menu delegate
                            if let menu = LeftMenu(rawValue: 4) {                            self.leftMenuProtocol.changeViewController(menu)                       }
                            
                            }], cancelTitle: kCancel, cancelAction:{ (action) in
                        })
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
}

