//
//  CareerProfile.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 17/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class CareerProfile: BaseViewController {

 //OUTLETS
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblElevatorPitch: UILabel!
    @IBOutlet var actionPlan1: UILabel!
    @IBOutlet var actionPlan2: UILabel!
    @IBOutlet var actionPlan3: UILabel!
    @IBOutlet var btnEditProfileOutlet: UIButton!

    @IBOutlet var lblNoActionPlan: UILabel!
    @IBOutlet var actionPlanStackView: UIStackView!
    @IBOutlet var btnMenuOutlet: UIButton!
    
    @IBOutlet var lblCareerField: UILabel!
    @IBOutlet var addDateReqBtn: UIButton!
    
    //VARIABLES
    var iscommingFrom = String()
    var role = Int()
    var UserInfoArr = [NSDictionary]()
    var myPortfolioVC = MyPortfolioVC()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let menuStoryboard = UIStoryboard(storyboard: .Menu)
        myPortfolioVC = menuStoryboard.instantiateViewController(withIdentifier: "MyPortfolioVC") as! MyPortfolioVC
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customizeUI() // set the ui
        //print(UserInfoArr)
        if iscommingFrom == kHomeVC {
            if DataManager.role == 3{
                addDateReqBtn.isHidden = true
            }
            else
            {
            addDateReqBtn.isHidden = false
            }
            btnEditProfileOutlet.isHidden = true
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
        else{
            addDateReqBtn.isHidden = true
            btnEditProfileOutlet.isHidden = false
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        }
        
        

    }
    
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
        
        //check is comming from
        if iscommingFrom == kHomeVC {
            
            lblTitle.text = "User's Profile"
            if DataManager.role == 3{
                btnEditProfileOutlet.isHidden = true
                addDateReqBtn.isHidden = true
            }
            else
            {
                btnEditProfileOutlet.isHidden = true
                addDateReqBtn.isHidden = false
            }
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            
                    if UserInfoArr.count != 0
                    {
                        let dicData = UserInfoArr[0]
                        
                        lblName.text = dicData.object(forKey: "name") as? String
                        lblElevatorPitch.text = dicData.object(forKey: "elevator_pitch") as? String
                        
                        
                        //Set User Image
                        let imgUrl = dicData.object(forKey: "profile_pic") as? String
                        if imgUrl != nil
                        {
                        self.imgProfile.sd_setImage(with: NSURL(string: imgUrl!)! as URL, completed: { (image, error, cache, url) in
                        })
                        }
                        else
                        {
                             self.imgProfile.image = #imageLiteral(resourceName: "user")
                        }
                        //let packageinfo = dicData.object(forKey: "packageinfo") as! NSArray
                        // let packInfoDic = packageinfo[0] as! NSDictionary
                        
                        actionPlanStackView.isHidden = false
                        lblNoActionPlan.isHidden = true
                        actionPlan1.text = dicData.object(forKey: "action_plans1") as? String
                        actionPlan2.text = dicData.object(forKey: "action_plans2") as? String
                        actionPlan3.text = dicData.object(forKey: "action_plans3") as? String
                        //check the action Plans is blank
                        if  actionPlan1.text == "" {
                            actionPlan1.text = "N/A"
                        }
                        if actionPlan2.text == "" {
                            actionPlan2.text = "N/A"
                        }
                        if actionPlan3.text == "" {
                            actionPlan3.text = "N/A"
                        }
                        if lblElevatorPitch.text == "" {
                            lblElevatorPitch.text = "Elevator Pitch: N/A"
                        }
                        if dicData.object(forKey: "career_profile") as? String != nil{
                            lblCareerField.text = dicData.object(forKey: "action_plans1") as? String
                        }else{
                            lblCareerField.text = "Career Field: N/A"
                        }
            }

        }else{
            if DataManager.careerField != ""{
                lblCareerField.text = "Career Field: \(DataManager.careerField!)"
            }else{
                lblCareerField.text = "Career Field: N/A"
            }
            //check the elevatorPitch is blank or not
            if DataManager.elevatorPitch != ""  {
                lblElevatorPitch.text = "Elevator Pitch: \(DataManager.elevatorPitch!)"
            }else{
                lblElevatorPitch.text = "Elevator Pitch: N/A"
            }
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "menu"), for: .normal)

            imgProfile.sd_setImage(with:URL(string: DataManager.userImage!), placeholderImage: #imageLiteral(resourceName: "user"))
            lblName.text = DataManager.name
            //check all the action plan of the user will be blank or not
            if DataManager.actionPlan1 == "" && DataManager.actionPlan2 == "" && DataManager.actionPlan3 == "" {
              
                actionPlanStackView.isHidden = true
                lblNoActionPlan.isHidden = false
           
            }else{
      
                actionPlanStackView.isHidden = false
                lblNoActionPlan.isHidden = true
                actionPlan1.text = DataManager.actionPlan1
                actionPlan2.text = DataManager.actionPlan2
                actionPlan3.text = DataManager.actionPlan3
                //check the action Plans is blank
                if DataManager.actionPlan1 == "" {
                    actionPlan1.text = "N/A"
                }
                if DataManager.actionPlan2 == "" {
                    actionPlan2.text = "N/A"
                }
                if DataManager.actionPlan3 == "" {
                    actionPlan3.text = "N/A"
                }
            }
         }
      }
    
    //MARK: Button Action
    @IBAction func addDateReqBtnAction(_ sender: Any) {
        let lunchBreak = UIStoryboard(storyboard: .Menu).instantiateViewController(withIdentifier: "LunchBreak") as! LunchBreak
        lunchBreak.iscommingFrom = kHomeVC
        animationPushWithViewController(viewController: lunchBreak)
        }
//MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        
        if iscommingFrom == kHomeVC {
            backButtonAction()
        }else{
            slideMenuController()?.toggleLeft()
        }
    }

    @IBAction func btnEditProfileAction(_ sender: Any) {
        animationPushWithViewController(viewController: myPortfolioVC)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
