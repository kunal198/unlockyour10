//
//  LeftMenuVC.swift
//  Unlock your 10
//
//  Created by brst on 10/9/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    
    case HomeVC = 0
    case InboxVC
    case CreateMessageVC
    case MyPortfolioVC
    case KeysVC
    case Memorandums
    case Elevator
    case Complaints
    case SubmitTraining
    case LunchBreak
    case AdvancedNotice
    case MentorRegisteration
    case Notifications
    case ContactSupport
    case logout
    
    
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}


class LeftMenuVC: BaseViewController,LeftMenuProtocol {

//OUTLETS
    

    
//VARIABLES

   var selectedIndex = 0
   var mentorMenu = ["Home/Contacts","Inbox","My Portfolio","Keys","Memorandums","Complaints","Training","Contact Support","Logout"]
    
   var userMenu = ["Home/Contacts","Inbox","Create a new message","My Portfolio","Keys","Memorandums","Elevator","Complaints","Training","Lunch Break","Advanced Notice","Register as Mentor","Notifications","Contact Support","Logout"]
    
    var homeVC: UIViewController!
    var inboxVC: UIViewController!
    var createMessageVC : CreateMessageVC!
    var careerProfile :UIViewController!
    var ComplaintsVC :UIViewController!
    var SubmitTrainingIdea : UIViewController!
    var keysVC : UIViewController!
    var contactSupportVC: UIViewController!
    var advancedNoticeVC: UIViewController!
    var mentorRegisteration: UIViewController!
    var lunchBreak: UIViewController!
    var visibleVC: UIViewController!
    var memorandums: UIViewController!
    var complaints: UIViewController!
    var submitTraining: UIViewController!
    var elevator: UIViewController!
    var notifications: UIViewController!
    var mentorProfileVC:UIViewController!
    var mentorKeysVC:UIViewController!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }

    // MARK: CustomizeUI
    func customizeUI()  {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.leftMenuProtocol = self
    
        let storyboard = UIStoryboard(storyboard: .Main)
        let menuStoryboard = UIStoryboard(storyboard: .Menu)

        let inboxVC = storyboard.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        inboxVC.leftMenuSelf = self
        self.inboxVC = UINavigationController(rootViewController: inboxVC)
        
        createMessageVC = menuStoryboard.instantiateViewController(withIdentifier: "CreateMessageVC") as! CreateMessageVC
        createMessageVC.leftMenuProtocol = self  //self the leftMenuProtocol
        
        let complaintsVC = menuStoryboard.instantiateViewController(withIdentifier: "ComplaintsVC") as! ComplaintsVC
        self.ComplaintsVC = UINavigationController(rootViewController: complaintsVC)
        
        
        let submitTrainingIdea = menuStoryboard.instantiateViewController(withIdentifier: "SubmitTrainingIdea") as! SubmitTrainingIdea
        self.SubmitTrainingIdea = UINavigationController(rootViewController: submitTrainingIdea)
        
        let careerProfile = menuStoryboard.instantiateViewController(withIdentifier: "CareerProfile") as! CareerProfile
        self.careerProfile = UINavigationController(rootViewController: careerProfile)

        let keysVC = menuStoryboard.instantiateViewController(withIdentifier: "KeysVC") as! KeysVC
        self.keysVC = UINavigationController(rootViewController: keysVC)

        let mentorKeysVC = menuStoryboard.instantiateViewController(withIdentifier: "MentorKeysVC") as! MentorKeysVC
        self.mentorKeysVC = UINavigationController(rootViewController: mentorKeysVC)

        
        memorandums = menuStoryboard.instantiateViewController(withIdentifier: "Memorandums") as! Memorandums
        
        advancedNoticeVC = menuStoryboard.instantiateViewController(withIdentifier: "AdvancedNotice") as! AdvancedNotice
        
        mentorRegisteration = menuStoryboard.instantiateViewController(withIdentifier: "MentorRegistration") as! MentorRegistration
        
        submitTraining = menuStoryboard.instantiateViewController(withIdentifier: "SubmitTrainingIdea") as! SubmitTrainingIdea
        
        lunchBreak = menuStoryboard.instantiateViewController(withIdentifier: "LunchBreak") as! LunchBreak
        
        contactSupportVC = menuStoryboard.instantiateViewController(withIdentifier: "ContactSupport") as! ContactSupport
        
        complaints = menuStoryboard.instantiateViewController(withIdentifier: "ComplaintsVC") as! ComplaintsVC
        
        elevator = menuStoryboard.instantiateViewController(withIdentifier: "ElevatorVC") as! ElevatorVC
        
        notifications = menuStoryboard.instantiateViewController(withIdentifier: "Notifications") as! Notifications
        
        let mentorProfileVC = menuStoryboard.instantiateViewController(withIdentifier: "MentorProfileVC") as! MentorProfileVC
        
        self.mentorProfileVC = UINavigationController(rootViewController: mentorProfileVC)
        
        visibleVC = UIApplication.topViewController()
        
    }
    
 //MARK: Button Action
    @IBAction func btnCrossAction(_ sender: Any) {
        self.slideMenuController()?.closeLeft()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        
    //check the role of the user(2 for user, 3 for mentor)

    if DataManager.role == 2 {
            
        switch menu {
        case .HomeVC:
            self.slideMenuController()?.changeMainViewController(self.homeVC, close: true)
            //self.slideMenuController()?.closeLeft()
        case .InboxVC:
            self.slideMenuController()?.changeMainViewController(self.inboxVC, close: true)
        case .CreateMessageVC:
            self.slideMenuController()?.changeMainViewController(self.createMessageVC, close: true)
        case .MyPortfolioVC:
            self.slideMenuController()?.changeMainViewController(self.careerProfile, close: true)
        case .KeysVC:
            self.slideMenuController()?.changeMainViewController(self.keysVC, close: true)
            selectedIndex = 4
            
        case .Memorandums:
            self.slideMenuController()?.changeMainViewController(self.memorandums, close: true)
        case .Elevator:
            self.slideMenuController()?.changeMainViewController(self.elevator, close: true)
        case .Complaints:
            self.slideMenuController()?.changeMainViewController(self.ComplaintsVC, close: true)
        case .SubmitTraining:
            self.slideMenuController()?.changeMainViewController(self.SubmitTrainingIdea, close: true)
        case .LunchBreak:
            self.slideMenuController()?.changeMainViewController(self.lunchBreak, close: true)
        case .AdvancedNotice:
            self.slideMenuController()?.changeMainViewController(self.advancedNoticeVC, close: true)
        case .MentorRegisteration:
            self.slideMenuController()?.changeMainViewController(self.mentorRegisteration, close: true)
        case .Notifications:
            self.slideMenuController()?.changeMainViewController(self.notifications, close: true)
        case .ContactSupport:
            self.slideMenuController()?.changeMainViewController(self.contactSupportVC, close: true)
        case .logout:
            
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        }
        
        }else{
        
        switch menu {
        case .HomeVC:  // for Home
            self.slideMenuController()?.changeMainViewController(self.homeVC, close: true)
        //self.slideMenuController()?.closeLeft()
        case .InboxVC: // for Inbox
            self.slideMenuController()?.changeMainViewController(self.inboxVC, close: true)
        case .CreateMessageVC: // for myPortfolioVC
            self.slideMenuController()?.changeMainViewController(self.mentorProfileVC, close: true)
        case .MyPortfolioVC:   // for keysVC
            self.slideMenuController()?.changeMainViewController(self.mentorKeysVC, close: true)
        case .KeysVC: // for memorandums
            self.slideMenuController()?.changeMainViewController(self.memorandums, close: true)
        case .Memorandums:  // for complaints
            self.slideMenuController()?.changeMainViewController(self.ComplaintsVC, close: true)
        case .Elevator: // for submitTraining
            self.slideMenuController()?.changeMainViewController(self.SubmitTrainingIdea, close: true)
        case .Complaints: // for Logout
          self.slideMenuController()?.changeMainViewController(self.contactSupportVC, close: true)
            //UserVM.sharedInstance.isLogout = true
           // self.slideMenuController()?.closeLeft()
        case .SubmitTraining:
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        case .LunchBreak:
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        case .AdvancedNotice:
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        case .MentorRegisteration:
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        case .Notifications:
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        case .ContactSupport: // for
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        case .logout:
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        }
      }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension LeftMenuVC : UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
            selectedIndex = indexPath.row
            tableView.reloadData()
            if let menu = LeftMenu(rawValue: indexPath.row) {
                self.changeViewController(menu)
            }
        
    }
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        if self.tableView == scrollView {
    //
    //        }
    //    }
}

extension LeftMenuVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //check the role of the user(2 for user, 3 for mentor)
        if DataManager.role == 2 {
            return userMenu.count
        }else{
            return mentorMenu.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        if selectedIndex == indexPath.row {
            cell.backgroundColor = UIColor.lightText
        }else{
            cell.backgroundColor = UIColor.clear
        }
        
        
        let lblMenu = cell.viewWithTag(10) as! UILabel
        //check the role of the user(2 for user, 3 for mentor)
        if DataManager.role == 2 {
            lblMenu.text = userMenu[indexPath.row]
        }else{
            lblMenu.text = mentorMenu[indexPath.row]
        }
        
        return cell
    }
}


//MARK: SliderMenu Delegate Methods
extension LeftMenuVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        if UserVM.sharedInstance.isLogout == true {
            self.showAlert(message: kLogoutString, title: "", otherButtons: [kYes:{action in
                //self.Logout()
                UserVM.sharedInstance.isLogout = false
                self.logout()
                
                }], cancelTitle: kNo, cancelAction:{ (action) in
                    UserVM.sharedInstance.isLogout = false
            })
        }
    }
    
}

