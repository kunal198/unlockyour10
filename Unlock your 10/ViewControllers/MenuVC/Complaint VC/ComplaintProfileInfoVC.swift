//
//  ComplaintProfileInfoVC.swift
//  Unlock your 10
//
//  Created by Brst on 13/12/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ComplaintProfileInfoVC: BaseViewController {
    @IBOutlet var compTableView: UITableView!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descLabel: UITextView!
    @IBOutlet var commentLbl: UILabel!
    @IBOutlet var lineLabel: UILabel!
    @IBOutlet var commentTxtView: UITextView!
    @IBOutlet var commentSubmitBtn: UIButton!
    @IBOutlet var commentBckView: UIView!
    @IBOutlet var tableHeaderView: UIView!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var comment: NSLayoutConstraint!
    var complaintInfoArray = [NSDictionary]()
    var commentDataArr =  [NSDictionary]()
    var complaint_id = Int()
    var cellHeight = CGFloat()
    var leftMenuProtocol:LeftMenuProtocol!
    var leftmenuSelf:UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.removeNavigationBarItem()
        commentSubmitBtn.layer.cornerRadius = 5
        commentSubmitBtn.layer.masksToBounds = true
        compTableView.showsVerticalScrollIndicator = false
        //commentTxtView?.generateShadowUsingBezierPath(radius: 2.0, opacity: 0.25,allCorner: false)
       
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(ComplaintProfileInfoVC.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(ComplaintProfileInfoVC.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//
//
        
        
        leftMenuProtocol = leftmenuSelf as! LeftMenuProtocol!
     
        
        //compTableView.estimatedRowHeight = 99
       // compTableView.rowHeight = UITableViewAutomaticDimension
     
        
        commentTxtView.layer.cornerRadius = 5
        commentTxtView.layer.masksToBounds = true
        dropShadow()
        
        compTableView.rowHeight = UITableViewAutomaticDimension
        compTableView.estimatedRowHeight = 99
    }
    // OUTPUT 1
    func dropShadow() {
        self.commentTxtView?.layer.cornerRadius = 5
        self.commentTxtView?.layer.borderColor = UIColor .gray .cgColor
        self.commentTxtView?.layer.borderWidth = 1
        self.commentTxtView.layer.masksToBounds = true
    }

    
    override func viewWillLayoutSubviews() {
        //DispatchQueue.main.async {
            print(self.tableHeaderView.frame)
            self.tableHeaderView.frame.size.height = self.commentLbl.frame.origin.y+self.commentLbl.frame.size.height + 15
            print(self.tableHeaderView.frame)
      //  }

    }
    
    @IBAction func submitComment(sender:UIButton)
    {
        let text = commentTxtView.text! as NSString
        if text .isEqual(to: "Add Comment:")
        {
            self.showAlert(message: "Please Add Comment", title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                
            })
        }
        else
        {
            callWebserviceForPostComments()
        }
    }
//    func keyboardWillShow(sender: NSNotification) {
//        
//        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            let keyboardHeight = keyboardSize.height
//            print(keyboardHeight)
//           // commentTxtView.frame.origin.y = self.view.frame.size.height - (keyboardHeight + 50)
//            
//        }
//        //self.view.frame.origin.y=0
//    }
//    
//    func keyboardWillHide(sender: NSNotification) {
//        //commentTxtView.frame.origin.y = self.view.frame.size.height - 110
//    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        compTableView.reloadData()
    }
    func customizeUI() {
        hideNavigationBar()
    }
    func textColor()
    {
        commentTxtView.text = "Add Comment:"
        commentTxtView.textColor = UIColor.lightGray
        commentTxtView.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        customizeUI()
        textColor()
        
        if UserDefaults.standard.value(forKey: kcomplaintInfoArray) != nil
        {
            let index = UserDefaults.standard.value(forKey: "indexPath") as! Int
            
            
            complaintInfoArray = UserDefaults.standard.value(forKey: "complaintInfoArray") as! [NSDictionary]
            //Get Complaint Data
            let dic: NSDictionary = complaintInfoArray[index]
            
            print(dic)
            if dic.object(forKey: "comment_data") is NSDictionary {
                print("NSDictionary")
            }
            else{
                commentDataArr = dic.object(forKey: "comment_data") as! [NSDictionary]
            }
            
            complaint_id = dic.object(forKey: "complaint_id") as! Int
            
            print(complaint_id)
            //get User Data
            let user_dataDic = dic.object(forKey: "user_data") as! NSDictionary
            nameLabel.text = user_dataDic.object(forKey: "name") as? String
            //Show Complaint description
            descLabel.text = dic.object(forKey: "complaint_desc") as? String
            
            //Set User Image
            let imgUrl = user_dataDic.object(forKey: "profile_pic") as? String
            
            profileImg?.sd_setImage(with: NSURL(string: imgUrl!)! as URL, completed: { (image, error, cache, url) in
            })
            
        }
        
        if DataManager.role == 3 || DataManager.role == 0{
            commentTxtView.isHidden = true
            commentSubmitBtn.isHidden = true
            tableViewHeight.constant = self.view.frame.size.height-65
        }
        else
        {
            tableViewHeight.constant = self.view.frame.size.height-185
            commentTxtView.isHidden = false
            commentSubmitBtn.isHidden = false
        }
    }
    @IBAction func MenuAction(_ sender: Any) {
        backButtonAction()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: UITableView Methods
extension ComplaintProfileInfoVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kComplaintsInfoCell, for: indexPath) as! ComplaintProfileCell
        //  cell.backgroundColor = UIColor .red
        //cell.backgroundColor = UIColor (red: 227/22, green: 227/255, blue: 227/255, alpha: 1.0)
        customizeCell(indexPath: indexPath, cell: cell)
        cell.selectionStyle = .none
        cell.cellImg.layer.borderWidth = 1.0
        cell.cellImg.layer.masksToBounds = false
        cell.cellImg.layer.borderColor = UIColor.white.cgColor
        cell.cellImg.layer.cornerRadius =  cell.cellImg.frame.size.width / 2
        cell.cellImg.clipsToBounds = true
        
        //        //Get Complaint Data
        let commentDic = commentDataArr[indexPath.row]
        
        //Show Complaint Data
        cell.descLabel.text  = commentDic.object(forKey: "comment") as? String
        
        
        //Set User Image
        let imgUrl = commentDic.object(forKey: "comment_user_profile_pic") as? String
       let user_id = commentDic.object(forKey: "user_id") as? Int
        
        cell.cellImg?.setShowActivityIndicator(true)
        cell.cellImg?.setIndicatorStyle(.gray)
        if imgUrl != nil && DataManager.userId != user_id
        {
            cell.cellImg?.sd_setImage(with: NSURL(string: imgUrl!)! as URL, completed: { (image, error, cache, url) in
             cell.cellImg?.setShowActivityIndicator(false)
            })
        }
        else
        {
            cell.cellImg?.image = profileImg.image
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        let shadowVw = cell.viewWithTag(10)
        
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let dic = commentDataArr[indexPath.row]
            let height:CGFloat = self.calculateHeight(inString: (dic.object(forKey: "comment") as? String)!)
            return height + 86
        }
    
        func calculateHeight(inString:String) -> CGFloat
        {
            let messageString = inString
            let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 11)]
    
            let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
    
            let rect : CGRect = attributedString.boundingRect(with: CGSize(width: 222.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
    
            let requredSize:CGRect = rect
            return requredSize.height
        }
    
}
extension ComplaintProfileInfoVC: UITextViewDelegate
{
    
    //Call Api To Post Comments
    func callWebserviceForPostComments() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         =   DataManager.userId
        parametersDict[APIKeys.kToken]          =  DataManager.token
        parametersDict[APIKeys.kComplaint_id]   =   complaint_id
        parametersDict[APIKeys.kComments]       = commentTxtView.text
        
        let comment: String! = commentTxtView.text
        textColor()
        
        UserVM.sharedInstance.postComments(dataDic: parametersDict as NSDictionary!, response: {(success, message, error) in
            
            if success {
                
                let dic = ["comment":comment] as NSDictionary
                self.commentDataArr.append(dic)
                self.compTableView.reloadData()
                self.view.endEditing(true)
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add Comment:"
            textView.textColor = UIColor.lightGray
        }
    }
    
}

