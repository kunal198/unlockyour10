//
//  ComplaintsVC.swift
//  Unlock your 10
//
//  Created by Brst981 on 14/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift



//Mark:Arrays
var namesArray = [String]()
var commentArray = [String]()
var noOfCommntArray = [String]()


//Mark:Variables
let cellIdentifier = "complaintsCell"



class ComplaintsVC: BaseViewController {
    
    // Outlets
    @IBOutlet weak var complaintView: UIView!
    @IBOutlet weak var highlightedView: UIView!
    @IBOutlet weak var complaintTxtView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet var postIco: UIImageView!
    @IBOutlet var complaintsTbl: UITableView!
    @IBOutlet var crossBtn: UIButton!
    
    var complaintProfile:ComplaintProfileInfoVC!
    var leftMenuself:UIViewController!
    var leftMenuProtocol:LeftMenuProtocol!
    //Array to get complaints
    var complaintDetailsDic = NSDictionary()
    var complaintArray = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        namesArray = ["Sara H","Mark Jacobs","Sean Z","Jennifer Anniston"]
        complaintsTbl.rowHeight = UITableViewAutomaticDimension
        complaintsTbl.estimatedRowHeight = 120
        complaintsTbl.showsVerticalScrollIndicator = false
        highlightedView.isHidden = true
       // complaintView?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        //complaintTxtView?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
        submitButton.layer.cornerRadius = 5
        submitButton.layer.masksToBounds = true
        
        complaintView.layer.cornerRadius = 5
        complaintView.layer.masksToBounds = true
        dropShadow()
    }
        // OUTPUT 1
    func dropShadow() {
        self.complaintTxtView?.layer.cornerRadius = 5
        self.complaintTxtView?.layer.borderColor = UIColor .gray .cgColor
        self.complaintTxtView?.layer.borderWidth = 1
        self.complaintTxtView.layer.masksToBounds = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customizeUI()

        
        if DataManager.role == 3{
            postButton.isHidden = true
            postIco.isHidden = true
        }
        else
        {
            postButton.isHidden = false
            postIco.isHidden = false
        }
        
        callWebserviceForGetComplaints()
       
    }
    func textViewData()
    {
        complaintTxtView.text = "Enter Your Complaint Here:"
        complaintTxtView.textColor = UIColor.lightGray
        complaintTxtView.delegate = self
    }
    func customizeUI()  {
        
        self.hideNavigationBar()
        self.addLeftMenuGesture()
        
        let storyboard = UIStoryboard(storyboard: .Menu)
        complaintProfile = storyboard.instantiateViewController(withIdentifier: kComplaintProfileInfoVC) as! ComplaintProfileInfoVC
    }
    override func viewDidLayoutSubviews() {
        complaintsTbl.reloadData()
    }
    
    @IBAction func openPostView() {
        textViewData()
        highlightedView.isHidden = false
    }
    @IBAction func crossBtnAction() {
        self.view.endEditing(true)
        complaintTxtView.text = ""
        highlightedView.isHidden = true
    }
    
    @IBAction func submitPost() {
        let text = complaintTxtView.text! as NSString
        if text.isEqual(to: "Enter Your Complaint Here:") {
            self.showAlert(message: "Please Enter Your Complaint", title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                
            })
        }
        else
        {
            highlightedView.isHidden = true
            callWebserviceForPostComplaints()
        }
        
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK: UITableView Methods
extension ComplaintsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complaintArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kComplaintsCell, for: indexPath) as! ComplaintTableViewCell
        customizeCell(indexPath: indexPath, cell: cell)
        cell.isEditing = true
        //Get Complaint Data
        let dic = complaintArray[indexPath.row]
        
        //Show Complaint Data
        let commentCoun = String(dic.object(forKey: "comment_count") as! Int)
        if commentCoun == "0" {
             cell.commentLabel.text = " Comment"
        }
        else {
            cell.commentLabel.text = commentCoun+" Comment"
        }
        
        
        cell.descLabel.text  = dic.object(forKey: "complaint_desc") as? String
        
        //get User Data
        let user_dataDic = dic.object(forKey: "user_data") as! NSDictionary
        cell.nameLabel.text = user_dataDic.object(forKey: "name") as? String
        
        //Set User Image
        let imgUrl = user_dataDic.object(forKey: "profile_pic") as? String
        
        cell.profileImg?.setShowActivityIndicator(true)
        cell.profileImg?.setIndicatorStyle(.gray)
        
        cell.profileImg?.sd_setImage(with: NSURL(string: imgUrl!)! as URL, completed: { (image, error, cache, url) in
            cell.profileImg?.setShowActivityIndicator(false)
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UserDefaults.standard.set(indexPath.row, forKey: kIndexPath)
        UserDefaults.standard.set(self.complaintArray, forKey: kcomplaintInfoArray)
        
        print(complaintProfile)
        
        animationPushWithViewController(viewController: complaintProfile)

       // self.animationPushWithIdentifier(identifier:kComplaintsToComProInfo)
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        let dic = complaintArray[indexPath.row]
//        let height:CGFloat = self.calculateHeight(inString: (dic.object(forKey: "complaint_desc") as? String)!)
//        return height + 90
//    }
//    
//    func calculateHeight(inString:String) -> CGFloat
//    {
//        let messageString = inString
//        let attributes : [String : Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 15.0)]
//        
//        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)
//        
//        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: 222.0, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
//        
//        let requredSize:CGRect = rect
//        return requredSize.height
//    }
}

extension ComplaintsVC {
    
    //Call Api For Post Compalits
    func callWebserviceForPostComplaints() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kComplaint_desc] = complaintTxtView.text
        
        print(parametersDict)
        
        UserVM.sharedInstance.postComplaints(complaintDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                //self.showToastWithMessage(message: message)
                self.callWebserviceForGetComplaints()
                self.view.endEditing(true)
//                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
//
//                })
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    //Call Api For Get Compalits
    func callWebserviceForGetComplaints() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         =  DataManager.userId
        parametersDict[APIKeys.kToken]          =  DataManager.token
        print(parametersDict)
        UserVM.sharedInstance.getComplaints(complaintDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                //print(response)
                self.textViewData()
                self.complaintArray = UserVM.sharedInstance.complaintListArray
                print(self.complaintArray )
                self.complaintsTbl.reloadData()
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }else if message == kNotEnoughKeysSendMessage{ //kNotPurchasePackage
                        
                        self.showAlert(message: message, title: "", otherButtons: [kOk:{action in
                            self.backButtonAction()
                            //call the left menu delegate
                            if let menu = LeftMenu(rawValue: 4) {
                                
                                self.leftMenuProtocol.changeViewController(menu)
                                
                            }
                            
                            }], cancelTitle: kCancel, cancelAction:{ (action) in
                        })
                        return
                    }
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    
    
    
}
extension ComplaintsVC: UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textViewData()
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
