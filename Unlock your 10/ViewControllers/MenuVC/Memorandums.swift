//
//  Memorandums.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 16/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class Memorandums: BaseViewController {

    //Outlets
    
    @IBOutlet var memorandumsTbl: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
  
    //MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    override func viewDidLayoutSubviews() {
        memorandumsTbl.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: UITableView Methods
extension Memorandums: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kMemorandumsCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        let shadowVw = cell.viewWithTag(10)
        shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
    }
}
