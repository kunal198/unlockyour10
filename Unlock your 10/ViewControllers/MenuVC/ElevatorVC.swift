//
//  ElevatorVC.swift
//  Unlock your 10
//
//  Created by Brst981 on 14/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ElevatorVC: BaseViewController {
    
    
    //Mark: Outlets
    @IBOutlet var searchView: UIView!
    @IBOutlet var elevatorCollec: UICollectionView!
    
    //Mark:Variables
    let cellIdentifier = "elevatorCell"
    var screenSize : CGRect!
    var screenWidth : CGFloat!
    var screenHeight : CGFloat!
    
    
    //Mark:Arrays
    var nameArray = [String]()
    var distanceArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        customizeSearchBar()
        
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        
        nameArray = ["Jennifier Aniston","Peter Smith","Aron Finch","Mark Jacobs","Rachel Johnson","Jack Anderson"]
        
        distanceArray = ["2kms Away","2kms Away","2kms Away","2kms Away","2kms Away","2kms Away"]
        
    }
    
    override func viewDidLayoutSubviews() {
        searchView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        elevatorCollec.reloadData()
    }
    
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //mark:Customizing search bar
    func customizeSearchBar(){
        
        searchView.layer.backgroundColor = UIColor.white.cgColor
        searchView.layer.masksToBounds = false
        searchView.layer.cornerRadius = 2.0
        searchView.layer.shadowOffset = CGSize(width: -1,height: -1)
        searchView.layer.shadowOpacity = 0.2
    }
}

//MARK: UICollectionView Methods

extension ElevatorVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return nameArray.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:cellIdentifier, for: indexPath) as! ElevatorCell
        cell.lblNameDesp?.text = nameArray[indexPath.row]
        cell.lblDistanceDesp?.text = distanceArray[indexPath.row]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Compute the dimension of a cell for an NxN layout with space S between
        // cells.  Take the collection view's width, subtract (N-1)*S points for
        // the spaces between the cells, and then divide by N to find the final
        // dimension for the cell's width and height.
        let cellsAcross: CGFloat = 2
        let spaceBetweenCells: CGFloat = 25
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        return CGSize(width: dim, height: dim)
    }

}

