//
//  MyPortfolioVC.swift
//  Unlock your 10
//
//  Created by brst on 10/14/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MyPortfolioVC: BaseViewController,UIDocumentPickerDelegate,UIGestureRecognizerDelegate {

 //OUTLETS
    @IBOutlet var nameBGView: UIView!
    
    @IBOutlet var elevatorPitchBGView: UIView!
    @IBOutlet var tableViewAddAction: UITableView!
    @IBOutlet var tableBGView: UIView!
    @IBOutlet var btnProfilePicOutlet: UIButton!
    @IBOutlet var btnAddResumeOutlet: UIButton!
    
    @IBOutlet var careerBGView: UIView!
    
    @IBOutlet var yearBGView: UIView!
    
    @IBOutlet var briefDescBGView: UIView!
    
    @IBOutlet var txtName: UITextField!
    
    @IBOutlet var txtElevatorPitch: UITextField!
    
    @IBOutlet var txtCareerField: UITextField!
    
    @IBOutlet var txtYearOfExp: UITextField!
    
    @IBOutlet var txtBriefDesc: UITextView!
    
    @IBOutlet var constraintHeightOfMentorFields: NSLayoutConstraint!
    
    @IBOutlet var constraintHeightOfUserFields: NSLayoutConstraint!
    
    @IBOutlet var constraintHeightOfElevatorPitch: NSLayoutConstraint!
    
    @IBOutlet var constraintBottomOfElevatorPitch: NSLayoutConstraint!
    
  //Variables
    
    let arrayOfCareerFeild = ["Business & Finance","Computers & Technology","Construction Trades","Education, Teaching & Training","Engineering & Engineering Technicians","Fishing, Farming & Forestry","Health & Medical","Hospitality, Travel & Tourism","Legal, Criminal Justice & Law Enforcement","Management","Media Communications & Broadcasting","Military & Armed Forces","Office Administration & Management","Production & Manufacturing","Professional & Service","Psychology & Counseling","Installation, Repair & Maintenance","Sales & Marketing","Social & Life Sciences","Transportation & Moving"]
    
    
    let arrayOfAddAction = ["Obtain a Job","Become a Leader","Improve Leadership Skills","Start a Business","Start a Network","Ask for a Raise","Improve Presentation Skills","Improve Communication Skills","Become an Expert"]
    
    var imageDict = [String:Data]()     // dict for profile image
    var resumeDict = [String:Data]()    // dict for upload resume
    var minetype = String()             // mine type variable
    var careerField = String()          // selected career field
    var yearOfExp = String()            // mentor years of exp.
    var briefState = String()           // mentor brief statement
    var name = String()                 // user/mentor name.
    var elevatorPitch = String()        // user elevator pitch
    var arrayOfActionPlans = [String]() // array of selected action plans.
    var indexOfPicker      = 0
    
//MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeNavigationBarItem()
        pickerDelegate = self  // set the picker delegate
        setPickerView(textField: txtCareerField, array: arrayOfCareerFeild)
        btnProfilePicOutlet.contentMode = .center
        btnProfilePicOutlet.imageView?.contentMode = .scaleAspectFill
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tapBlurButton(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    // UIGestureRecognizerDelegate method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: tableViewAddAction) == true {
            return false
        }
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customizeUI() // set up the UI
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }

    
    //MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()//hide the navigation bar.
        setUserValues() //set the user values
   
        // set the shadow of the views
        nameBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        elevatorPitchBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        careerBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        yearBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        briefDescBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        tableBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)

    }
    
    func setUserValues() {
        //set the user values
        //check the role (2 for user , 3 for mentor)
        if DataManager.role == 3 {
            constraintHeightOfElevatorPitch.constant = 0
            constraintHeightOfUserFields.constant = 0
            constraintBottomOfElevatorPitch.constant = 0
            constraintHeightOfMentorFields.constant = 220
            txtCareerField.text = DataManager.careerField
            txtYearOfExp.text   = DataManager.yearOfExp
            txtBriefDesc.text   = DataManager.description
            txtCareerField.text = DataManager.careerField
            txtBriefDesc.textColor = UIColor.black
            btnAddResumeOutlet.isHidden = false
            
        }else{
            constraintHeightOfElevatorPitch.constant = 48
            constraintBottomOfElevatorPitch.constant = 5
            constraintHeightOfUserFields.constant = 185
            // only show career field
            constraintHeightOfMentorFields.constant = 48
            txtElevatorPitch.text = DataManager.elevatorPitch
            txtCareerField.text = DataManager.careerField
            btnAddResumeOutlet.isHidden = true
            arrayOfActionPlans.removeAll()
            if DataManager.actionPlan1 != "" {
                arrayOfActionPlans.append(DataManager.actionPlan1!)
            }
            if DataManager.actionPlan2 != "" {
                arrayOfActionPlans.append(DataManager.actionPlan2!)
            }
            if DataManager.actionPlan3 != "" {
                arrayOfActionPlans.append(DataManager.actionPlan3!)
            }
            tableViewAddAction.reloadData()
        }
    
        btnProfilePicOutlet.sd_setImage(with: URL(string:DataManager.userImage!), for: .normal, placeholderImage: #imageLiteral(resourceName: "add-image"))
        txtName.text = DataManager.name
        tableViewAddAction.setContentOffset(CGPoint.zero, animated: true)
    }
    
 //MARK: Buttons Action
    
    @IBAction func btnProfileAction(_ sender: Any) {
        view.endEditing(true)
        name          = txtName.text!
        elevatorPitch = txtElevatorPitch.text!
        careerField     = txtCareerField.text!
        yearOfExp       = txtYearOfExp.text!
        briefState      = txtBriefDesc.text
        //  Image picker
        self.view.endEditing(true)
        CustomImagePickerView.sharedInstace.imageSize = btnProfilePicOutlet.frame.size
        self.imagePickerDelegate = self
        self.showImagePicker()
    }
    
    
    @IBAction func btnUpdateProfileAction(_ sender: Any) {
        view.endEditing(true)
        //check the role of the user (2 for user and 3 for mentor)
        if DataManager.role == 3{
            if checkValidations() == true
            {
                callWebServiceForUpdateProfile()
            }
        }else{
            if txtName.text == ""{
                showAlert(message: kEnterName, title: "")
            }else if txtCareerField.isEmpty{
                showAlert(message: kSelectCareerField, title: "")
                
            }else if(arrayOfActionPlans.count == 0){
                showAlert(message: kSelectAlteastOneAction, title: "")
            }
            else{
                callWebServiceForUpdateProfile()
            }
        }
    }
    
    //menu button action
    @IBAction func btnMenuAction(_ sender: Any) {
        backButtonAction()
    }
  
    @IBAction func btnAddResumeAction(_ sender: Any) {
        
        careerField     = txtCareerField.text!
        yearOfExp       = txtYearOfExp.text!
        briefState      = txtBriefDesc.text
        name            = txtName.text!
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: [ "public.composite-content","com.microsoft.word.doc"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
  
    
    //MARK: validations
    func checkValidations() -> Bool {
        
        if (txtName.text?.isEmpty)!||txtCareerField.isEmpty||txtYearOfExp.isEmpty||txtBriefDesc.text=="Share A Brief Statement On Your Experience Or Offer Some Advice" {
            
            if txtName.isEmpty {
                showAlert(message: kEnterName, title: "")
            }else if txtCareerField.isEmpty{
                showAlert(message: kSelectCareerField, title: "")
                
            }else if txtYearOfExp.isEmpty{
                showAlert(message: kEnterYearOfExp, title: "")
            }else if txtBriefDesc.text == "Share A Brief Statement On Your Experience Or Offer Some Advice"{
                showAlert(message: kBriefStatement, title: "")
            }
            return false
        }else if txtBriefDesc.text.length < 30{
            showAlert(message: kBriefStatement, title: "")
            return false
        }
        else{
            return true
        }
    }
    
    
//MARK: Doucument Picker Delegate methods
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        txtName.text = name
        txtCareerField.text = careerField
        txtYearOfExp.text   = yearOfExp
        txtBriefDesc.text   = briefState
        
        if(controller.documentPickerMode == UIDocumentPickerMode.import){
            let data = try! Data(contentsOf: url)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            let mbString = bcf.string(fromByteCount: Int64(data.count))
            let strArr = mbString.components(separatedBy: " ")
            
            if Double(strArr[0])! > 5 {
                showAlert(message: kSelectLessThan5, title: "")
                return
            }
            resumeDict["resume"] = data
            
            if url.path.hasSuffix("pdf"){
                minetype = "pdf"
                btnAddResumeOutlet.setTitle("resume.pdf", for: .normal)
            }else{
                minetype = "msword"
                btnAddResumeOutlet.setTitle("resume.doc", for: .normal)
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController){
        // set the existing values
        txtName.text = name
        txtCareerField.text = careerField
        txtYearOfExp.text   = yearOfExp
        txtBriefDesc.text   = briefState
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
//MARK: UITableView Methods
extension MyPortfolioVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfAddAction.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //check the selected array limit
        if arrayOfActionPlans.count < 3 {
            //check the selected array contains the selected element
            if arrayOfActionPlans.contains(arrayOfAddAction[indexPath.row]) {
                arrayOfActionPlans.remove(at: arrayOfActionPlans.index(of: (arrayOfAddAction[indexPath.row]))!)
            }else{
                //add the selected element in the selected array
                arrayOfActionPlans.append(arrayOfAddAction[indexPath.row])
            }
        }else{
            //check the selected element is contain in the seleced array
            if arrayOfActionPlans.contains(arrayOfAddAction[indexPath.row]) {
                arrayOfActionPlans.remove(at: arrayOfActionPlans.index(of: arrayOfAddAction[indexPath.row])!)
                
            }else{
                showAlert(message: kSelectLimitCross, title: "")
            }
        }
        tableView.reloadData()
    }

// customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
       let lblAction = cell.viewWithTag(10) as! UILabel
       let imgCheck = cell.viewWithTag(20) as! UIImageView
       lblAction.text = arrayOfAddAction[indexPath.row];
        imgCheck.isHidden = true
            for item in  arrayOfActionPlans{
                if indexPath.row == arrayOfAddAction.index(of: item) {
                    imgCheck.isHidden = false
                }
            }
    }
}
//MARK: Custom Image Picker Delegates
extension MyPortfolioVC : CustomImagePickerDelegate {
    
    func didPickImage(_ image: UIImage) {   //Get Image from Picker and use the image
        txtName.text! = name
        txtCareerField.text = careerField
        txtYearOfExp.text   = yearOfExp
        txtBriefDesc.text   = briefState
        
        txtElevatorPitch.text! = elevatorPitch
        btnProfilePicOutlet.setImage(image, for: .normal)
        imageDict["profile_pic"] = image.jpegData(.low)
    }
    
}

//MARK: UITextView Delegates

extension MyPortfolioVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
        if(textView.text == "Share A Brief Statement On Your Experience Or Offer Some Advice") {
            textView.text = ""
            textView.textColor = UIColor.black

        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
        if(textView.text == "") {
            textView.textColor = UIColor.darkGray
            textView.text = "Share A Brief Statement On Your Experience Or Offer Some Advice"
        }
    }
}

//MARK: Picker Delegates
extension MyPortfolioVC: BaseVCDelegate {
    
    func didSelectPickerViewAtIndex(index: Int) {
        indexOfPicker = index
        txtCareerField.text = arrayOfCareerFeild[index]
    }
    
    func didClickOnDoneButton() {
        //if txtCareerField.isEmpty {
            txtCareerField.text = arrayOfCareerFeild[indexOfPicker]
        //}
    }
}


//API Methods
extension MyPortfolioVC{
    
    func callWebServiceForUpdateProfile(){
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = DataManager.role
        parametersDict[APIKeys.kName]           = txtName.text
        parametersDict[APIKeys.kCareer]         = txtCareerField.text

        //check the role of the user (2 for user and 3 for mentor)
        if DataManager.role == 2 {
            parametersDict[APIKeys.kElevatorPitch]  = txtElevatorPitch.text
            //check the action plan selected
            if arrayOfActionPlans.count == 1 {
                parametersDict[APIKeys.kActionPlans1] = arrayOfActionPlans[0]
                parametersDict[APIKeys.kActionPlans2] = ""
                parametersDict[APIKeys.kActionPlans3] = ""
            }else if arrayOfActionPlans.count == 2{
                parametersDict[APIKeys.kActionPlans1] = arrayOfActionPlans[0]
                parametersDict[APIKeys.kActionPlans2] = arrayOfActionPlans[1]
                parametersDict[APIKeys.kActionPlans3] = ""

            }else if arrayOfActionPlans.count == 3{
                parametersDict[APIKeys.kActionPlans1] = arrayOfActionPlans[0]
                parametersDict[APIKeys.kActionPlans2] = arrayOfActionPlans[1]
                parametersDict[APIKeys.kActionPlans3] = arrayOfActionPlans[2]
            }
        }else{
            parametersDict[APIKeys.kExperience]     = txtYearOfExp.text
            parametersDict[APIKeys.kDescription]    = txtBriefDesc.text
        }
   
        
        UserVM.sharedInstance.updateProfile(profileDetails:parametersDict as NSDictionary!,  imageDict: imageDict, applicationDict: resumeDict,mineType:minetype, response: { (success, message, error) in
            if success {
                
                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                    self.backButtonAction()
                })
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
}

//MARK: UITextView and UITextField Delegates

extension MyPortfolioVC: UITextFieldDelegate {
    
    //Textfield
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        
        if textField == txtName {
            let newLength = currentCharacterCount + string.characters.count - range.length
            if validateNameTextFeild(string: string)
            {
                return newLength <= 25
            }else{
                return false
            }
        }
        
        //set limit for elevator pitch textfield.
        if textField == txtElevatorPitch {
            let newLength = currentCharacterCount + string.characters.count - range.length
            return newLength <= 30
        }

        return true
    }
    
}
