//
//  CreateMessageVC.swift
//  Unlock your 10
////
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        IQKeyboardManager.sharedManager().enableAutoToolbar = false
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        IQKeyboardManager.sharedManager().enableAutoToolbar = true
//    }
//  Created by brst on 10/14/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import SDWebImage
import IQKeyboardManagerSwift

class CreateMessageVC: BaseViewController,UIDocumentPickerDelegate {

  //OUTLETS
    
    @IBOutlet var NameBGView: UIView!
    
    @IBOutlet var messageBGView: UIView!
    
    @IBOutlet var AttachFileBGView: UIView!
    
    @IBOutlet var txtEnterName: UITextField!
    
    @IBOutlet var txtVwMessage: UITextView!
    
    @IBOutlet var btnMenuOutlet: UIButton!
    
    @IBOutlet var tableViewMentors: UITableView!
    
    @IBOutlet var constraintHeightOfSearchTable: NSLayoutConstraint!
    
    @IBOutlet var lblAttachFile: UILabel!

    
//Variables
    var iscommingFrom   = String()
    var mentorsArray    = [NSDictionary]()
    var attachDict      = [String:Data]()
    var receiverId:Int!
    var nameText        = String()
    var messageText     = String()
    var receiverIdCopy:Int!
    var homeVC:UIViewController!
    var leftMenuProtocol:LeftMenuProtocol!

 //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        customizeUI()
            self.NameBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
            self.messageBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
            self.AttachFileBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
       
    }

        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
            receiverId = nil
    }
    
//MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    
        //check iscomming from menu or home
        if iscommingFrom == kHomeVC {
            
           btnMenuOutlet.setImage(#imageLiteral(resourceName: "back"), for: .normal)
           txtEnterName.text = nameText
            lblAttachFile.text = "Attach File:(pdf)"
            txtVwMessage.text = "Type your Message Here:"
            txtVwMessage.textColor = UIColor.darkGray

        }else if iscommingFrom == "HomeNoContact"{
            
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            lblAttachFile.text = "Attach File:(pdf)"
            txtVwMessage.text = "Type your Message Here:"
            txtEnterName.text = ""
            receiverId = nil
        }
        else{
            
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
            lblAttachFile.text = "Attach File:(pdf)"
            txtVwMessage.text = "Type your Message Here:"
            txtEnterName.text = ""
            receiverId = nil
        }
        
        mentorsArray.removeAll()
        tableViewMentors.reloadData()
        self.constraintHeightOfSearchTable.constant = 0
        
    }

//MARK: Button Action
    @IBAction func btnMenuAction(_ sender: Any) {
        //check iscomming from menu or home
        if iscommingFrom == kHomeVC||iscommingFrom == "HomeNoContact"{
           backButtonAction()
        }else{
             slideMenuController()?.toggleLeft()
        }
    }
    
    @IBAction func btnAttachFileAction(_ sender: Any) {
        // save the existing values
        if DataManager.role == 2 {
            if DataManager.isPackagePurchase == 0{
                showAlert(message: "If you want to send attachment then purchase any package first.", title: "")
                return
            }
        }
        
            nameText       = txtEnterName.text!
            messageText    = txtVwMessage.text
            receiverIdCopy = receiverId
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.adobe.pdf"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        if receiverId == nil {
            showAlert(message: kSelectUserFirst, title: "")
            
        }else if txtVwMessage.text == "Type your Message Here:"  {
            showAlert(message: kEnterYourMessage, title: "")
            
        }else{
            
            callWebserviceForSendMessage()
        }
    }
    
 //MARK: Document Picker Delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        // set the existing values
        txtEnterName.text! = nameText
        txtVwMessage.text  = messageText
        receiverId         = receiverIdCopy
        
        if(controller.documentPickerMode == UIDocumentPickerMode.import){
            let data = try! Data(contentsOf: url)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            let mbString = bcf.string(fromByteCount: Int64(data.count))
            let strArr = mbString.components(separatedBy: " ")
            
            if Double(strArr[0])! > 5 {
                showAlert(message: kSelectAttachLessThan5, title: "")
                return
            }
            
            lblAttachFile.text = "attach.pdf"
            attachDict["attachment"] = data
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController){
        // set the existing values

        txtEnterName.text! = nameText
        txtVwMessage.text  = messageText
        receiverId         = receiverIdCopy
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: UITextView and UITextField Delegates

extension CreateMessageVC: UITextViewDelegate,UITextFieldDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Type your Message Here:") {
            textView.text = ""
            textView.textColor = UIColor.black

        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == "") {
            textView.text = "Type your Message Here:"
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //check the return key at location 0
        if (range.location == 0 && text == "\n") {
            textView.resignFirstResponder()
            return false;        }
        //check the whitespace
        if (range.location == 0 && text == " ") {
            return false;
            
        }
     return true
        
    }
    
    //Textfield
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if DataManager.role == 3{
            return false
        }else{
            return true
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtEnterName {
            if txtEnterName.isEmpty { //check the textfield
                hideMentorsTableView()
                showAlert(message: kEnterTextFirst, title: "")
            }else{
                callWebserviceForSearchMentors()
            }
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        if textField == txtEnterName {
            receiverId = nil
        }
        
        return true
    }
    
}

//MARK: Webservice Methods
extension CreateMessageVC{
    
    func callWebserviceForSearchMentors() {
        
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kUserId]         = DataManager.userId
            parametersDict[APIKeys.kQuery]          = txtEnterName.text
            parametersDict[APIKeys.kToken]          = DataManager.token
        

            UserVM.sharedInstance.searchMentors(searchDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
                if success {
                    
                  self.mentorsArray = UserVM.sharedInstance.mentorListArray
                  self.ShowMentorsTableView() //show the mentor tableView
                  self.tableViewMentors.reloadData() //reload the table
                    
                }
                else {
                    if message != nil {
                        if message == kTokenNotMatched{   // check other device login
                            DataManager.isUserLoggedIn = false
                            self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                                self.logout()   // logout the user
                            })
                            return
                        }
                        self.hideMentorsTableView()  // hide the mentors tableview
                        self.showAlert(message: message, title: "")
                    }
                    else {
                        self.showErrorMessage(error: error!)
                    }
                }
            })
        }
    
    
    func callWebserviceForSendMessage() {
        
        Indicator.isEnabledIndicator = true
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kReceiverId]     = receiverId
        parametersDict[APIKeys.kMessage]        = txtVwMessage.text
        parametersDict[APIKeys.kToken]          = DataManager.token
        
        MessageVM.sharedInstance.sendMessage(messageDetails: parametersDict as NSDictionary, applicationDict: attachDict, mineType: "pdf", response:{ (success, message, error) in
            if success {
                
                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                    //check is comming from home or menu
                    if self.iscommingFrom == kHomeVC||self.iscommingFrom == "HomeNoContact"{
                        self.backButtonAction()
                    }else{
                        //call the left menu delegate
                        if let menu = LeftMenu(rawValue: 0) {
                            self.leftMenuProtocol.changeViewController(menu)
                        }
                    }
                })
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }else if message == kNotEnoughKeysSendMessage{
                        
                        self.showAlert(message: message, title: "", otherButtons: [kOk:{action in
                            self.backButtonAction()
                            //call the left menu delegate
                            if let menu = LeftMenu(rawValue: 4) {
                                
                                self.leftMenuProtocol.changeViewController(menu)
                                
                            }
                            
                            }], cancelTitle: kCancel, cancelAction:{ (action) in
                        })
                        return
                    }
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }

    
    
    
 //MARK: Show/Hide Table Methods
    func ShowMentorsTableView()  {
        //open Patient table view
            UIView.animate(withDuration: 0.50, delay: 0.0, options: [], animations: {
                self.constraintHeightOfSearchTable.constant = 150
                self.view.layoutIfNeeded()
            })
    }
    
    func hideMentorsTableView()  {
        UIView.animate(withDuration: 0.50, delay: 0.0, options: [], animations: {
            self.constraintHeightOfSearchTable.constant = 0
            self.view.layoutIfNeeded()
        }, completion: { (finished: Bool) in
            self.mentorsArray.removeAll()
            self.tableViewMentors.reloadData()
        })
    }
    
}


//MARK: UITableView Methods
extension CreateMessageVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mentorsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
        let imgProfile = cell.viewWithTag(10) as! UIImageView
        let imgLock = cell.viewWithTag(30) as! UIImageView
        let lblName = cell.viewWithTag(20) as! UILabel
        let mentorDict = mentorsArray[indexPath.row] 
      
        imgProfile.sd_setImage(with: URL(string:mentorDict[APIKeys.kProfilePic] as! String), placeholderImage: #imageLiteral(resourceName: "dummyUser"))
        lblName.text = mentorDict[APIKeys.kName] as? String
        //Differentiate the mentors and users
        if mentorDict[APIKeys.kUserRole] as? Int == 3 {
            imgLock.isHidden = false
        }else{
            imgLock.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mentorDict = mentorsArray[indexPath.row]
        txtEnterName.text = mentorDict[APIKeys.kName] as? String
        receiverId = mentorDict[APIKeys.kUserId] as! Int
        hideMentorsTableView()
    }
    
    
}



