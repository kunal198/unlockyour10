//
//  HomeVC.swift
//  Unlock your 10
//
//  Created by brst on 10/9/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import SDWebImage

class HomeVC: BaseViewController{

    //Outlets
 
  
    @IBOutlet var collectionViewHome: UICollectionView!
    
    @IBOutlet var btnNoRecordFound: UIButton!
    
    @IBOutlet var lblNorecordForMentor: UILabel!
    //VARIABLES
    
    var arrayUsers = [NSDictionary]()
    var createMessageVC:CreateMessageVC!
    var myProfile:MyProfile!
    var careerProfileVC:CareerProfile!
    var leftMenuself:UIViewController!
    var sessionListArray = [NSDictionary]()
    var sessionDetailsDic = NSDictionary()
    var role = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        fetchProducts()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: InApp Purchase
    func fetchProducts() {
       
        KeysProduct.store.requestProducts{success, products in
            if success {
                KeysVM.sharedInstance.productList =  products!

                KeysVM.sharedInstance.productList.sort{(Double($0.price) < Double($1.price))}

            }
        }
        
        PackageProduct.store.requestProducts{success, products in
            if success {
                KeysVM.sharedInstance.packageList =  products!
                
                KeysVM.sharedInstance.packageList.sort{(Double($0.price) < Double($1.price))}
            }
        }
                
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callWebserviceForHome()
    }
    func customizeUI() {
        
        hideNavigationBar()
        let storyboard = UIStoryboard(storyboard: .Menu)
        createMessageVC = storyboard.instantiateViewController(withIdentifier: kCreateMessageVC) as! CreateMessageVC
        myProfile = storyboard.instantiateViewController(withIdentifier: kMyProfile) as! MyProfile
        careerProfileVC = storyboard.instantiateViewController(withIdentifier: kCareerProfile) as! CareerProfile
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }

    @IBAction func btnNorecordFoundAction(_ sender: Any) {
        createMessageVC.iscommingFrom = "HomeNoContact"
        animationPushWithViewController(viewController: createMessageVC)
    }
    
}
extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: Collection view DetaSource Methods
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionViewHome.bounds.size.width/2, height:146)
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath as IndexPath) as! HomeCollectionCell
        customizeCell(indexPath: indexPath,cell:cell)
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataDict = arrayUsers[indexPath.row] as NSDictionary
        //user open mentor only
        let val: Int! = dataDict.object(forKey: APIKeys.kUserRole) as! Int
        let otheruser_id: Int! = dataDict.object(forKey: APIKeys.kUserId) as! Int
        UserDefaults.standard.set(otheruser_id, forKey: "otheruser_id")
        role = val
        
        
        if DataManager.role == 2{
            if dataDict[APIKeys.kUserRole] as! Int == 3 {
                // push to the mentor profile screen
                myProfile.iscommingFrom = kHomeVC
                myProfile.mentorId = dataDict[APIKeys.kUserId] as! Int
                myProfile.leftmenuSelf = leftMenuself
                animationPushWithViewController(viewController: myProfile)
            }
            else
            {
                callWebserviceForGetUserProfile(user_id: dataDict[APIKeys.kUserId] as! Int)
            }
        }
        else{
              if dataDict[APIKeys.kUserRole] as! Int == 3 {
            }
              else{
                callWebserviceForGetUserProfile(user_id: dataDict[APIKeys.kUserId] as! Int)
            }
        }
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:HomeCollectionCell)  {
        cell.shadowVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: true)
        cell.btnSendMessage.tag = indexPath.row
        cell.btnSendMessage.addTarget(self, action: #selector(self.btnSendMessageAction), for: UIControlEvents.touchUpInside)
        let userDict = arrayUsers[indexPath.row]
        
        cell.imgProfile.sd_setImage(with: URL(string:userDict[APIKeys.kProfilePic] as! String), placeholderImage: #imageLiteral(resourceName: "dummyUser"))
        cell.lblName.text = userDict[APIKeys.kName] as? String
        //let userid = userDict[APIKeys.kUserId] as? Int
        if userDict[APIKeys.kUserRole] as! Int == 3 {
            cell.imgLock.isHidden = false
        } else{
            cell.imgLock.isHidden = true
        }
        
    }
    
//MARK: SendMessage Action
    func btnSendMessageAction(sender:UIButton)  {
        let userDict = arrayUsers[sender.tag]
         createMessageVC.iscommingFrom = kHomeVC
         createMessageVC.nameText = userDict[APIKeys.kName] as! String
         createMessageVC.receiverId = userDict[APIKeys.kUserId] as? Int
         animationPushWithViewController(viewController: createMessageVC)
    }
    
}

//API Methods
extension HomeVC{
    
    func callWebserviceForHome() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        
        MessageVM.sharedInstance.getHome(homeDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.btnNoRecordFound.isHidden = true
                  self.lblNorecordForMentor.isHidden = true
                self.arrayUsers = MessageVM.sharedInstance.listArray
                self.collectionViewHome.reloadData() //reload the table
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }else if message == kNoSendMessageFound{
                        
                        if DataManager.role == 2{
                             self.lblNorecordForMentor.isHidden = true
                             self.btnNoRecordFound.isHidden = false
                        }else{
                             self.btnNoRecordFound.isHidden = true
                            self.lblNorecordForMentor.isHidden = false
                        }
                       
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    //Call Api For Edit Training Session
    func callWebserviceForGetUserProfile(user_id: Int) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kOtherUserId]    = user_id
        
        print(parametersDict)
        
        UserVM.sharedInstance.getUserProfile(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                
                self.sessionDetailsDic = UserVM.sharedInstance.sessionDetailsDic
                print(self.sessionDetailsDic)
                
                self.careerProfileVC.iscommingFrom = kHomeVC
                self.careerProfileVC.role = self.role
                self.careerProfileVC.UserInfoArr = [self.sessionDetailsDic]
                //self.careerProfileVC.leftmenuSelf = leftMenuself
                self.animationPushWithViewController(viewController: self.careerProfileVC)
                //                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
//
//                })
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
}




