//
        //"com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive"

//  MentorRegistration.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 14/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class MentorRegistration: BaseViewController,UIDocumentPickerDelegate {
    
    // Outlets

    @IBOutlet var nameBackgroundView: UIView!
    @IBOutlet var emailBackgroundView: UIView!
    @IBOutlet var careerBackgroundView: UIView!
    @IBOutlet var experienceBackgroundView: UIView!
    @IBOutlet var referredByBackgroundView: UIView!
    @IBOutlet var adviceBackgroundView: UIView!
    @IBOutlet var uploadResumeBackgroundView: UIView!
    
 
    @IBOutlet var txtName: UITextField!
  
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var adviceTxtView: UITextView!

    @IBOutlet var txtCareerField: UITextField!
    
    @IBOutlet var txtYearOfExp: UITextField!
    
    @IBOutlet var txtRefByEmail: UITextField!
    
    @IBOutlet var lblResume: UILabel!
    
 //MARK: VARIABLES 

    let arrayOfCareerFeild = ["Business & Finance","Computers & Technology","Construction Trades","Education, Teaching & Training","Engineering & Engineering Technicians","Fishing, Farming & Forestry","Health & Medical","Hospitality, Travel & Tourism","Legal, Criminal Justice & Law Enforcement","Management","Media Communications & Broadcasting","Military & Armed Forces","Office Administration & Management","Production & Manufacturing","Professional & Service","Psychology & Counseling","Installation, Repair & Maintenance","Sales & Marketing","Social & Life Sciences","Transportation & Moving"]
    
    var imageDict = [String:Data]()
    var resumeDict = [String:Data]()
    var minetype = String()
    var careerField = String()
    var yearOfExp = String()
    var referredEmail = String()
    var briefState = String()
    var email = String()

//MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customizeUI()

    }
    
//MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
        pickerDelegate = self  // set the picker delegate
        setPickerView(textField: txtCareerField, array: arrayOfCareerFeild)
        txtName.text = DataManager.name // set the name of user
        txtEmail.text = DataManager.email   // set the email of user
        txtCareerField.text = ""
        txtRefByEmail.text = ""
        txtYearOfExp.text = ""
        adviceTxtView.text = "SHARE A BRIEF STATEMENT ON YOUR EXPERIENCE OR OFFER SOME ADVICE:"
        lblResume.text = "UPLOAD YOUR RESUME:(pdf/doc)"
        
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        nameBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        emailBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        careerBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        experienceBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        referredByBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        adviceBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        uploadResumeBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if checkValidations() == true {
            callWebServiceForMentorRegistor()
        }
    }
    
    @IBAction func btnResumeAction(_ sender: Any) {
        
        careerField     = txtCareerField.text!
        yearOfExp       = txtYearOfExp.text!
        referredEmail   = txtRefByEmail.text!
        briefState      = adviceTxtView.text
        email           = txtEmail.text!
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: [ "public.composite-content","com.microsoft.word.doc"], in: .import)
        
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
       
        txtCareerField.text = careerField
        txtYearOfExp.text   = yearOfExp
        txtRefByEmail.text  = referredEmail
        adviceTxtView.text  = briefState
        txtEmail.text       = email
        
        
        if(controller.documentPickerMode == UIDocumentPickerMode.import){
            let data = try! Data(contentsOf: url)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            let mbString = bcf.string(fromByteCount: Int64(data.count))
            let strArr = mbString.components(separatedBy: " ")
            
            if Double(strArr[0])! > 5 {
                showAlert(message: kSelectLessThan5, title: "")
                return
            }
            resumeDict["resume"] = data
            
            if url.path.hasSuffix("pdf"){
                minetype = "pdf"
                lblResume.text = "resume.pdf"
            }else{
                minetype = "msword"
                lblResume.text = "resume.doc"
            }
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController){
        // set the existing values
     
        txtCareerField.text = careerField
        txtYearOfExp.text   = yearOfExp
        txtRefByEmail.text  = referredEmail
        adviceTxtView.text  = briefState
        txtEmail.text       = email
    }
    
    
  //MARK: validations
    func checkValidations() -> Bool {
        
        if (txtName.text?.isEmpty)!||txtEmail.isEmpty||txtCareerField.isEmpty||txtYearOfExp.isEmpty||adviceTxtView.text=="SHARE A BRIEF STATEMENT ON YOUR EXPERIENCE OR OFFER SOME ADVICE:"||lblResume.text == "UPLOAD YOUR RESUME:(pdf/doc)" {
           
            if txtName.isEmpty {
                showAlert(message: kEnterName, title: "")
            }else if txtEmail.isEmpty{
                showAlert(message: kEnterEmail, title: "")
                
            }else if txtCareerField.isEmpty{
                showAlert(message: kSelectCareerField, title: "")
                
            }else if txtYearOfExp.isEmpty{
                showAlert(message: kEnterYearOfExp, title: "")
            }else if adviceTxtView.text == "SHARE A BRIEF STATEMENT ON YOUR EXPERIENCE OR OFFER SOME ADVICE:"{
                 showAlert(message: kBriefStatement, title: "")
            }
            else{
                showAlert(message: kUploadResume, title: "")
            }
            
           return false
        }else if !isValidEmail(email: txtEmail.text!){
            
            showAlert(message: kEnterValidEmail, title: "")
            return false

        }else if adviceTxtView.text.length < 30{
            
            showAlert(message: kBriefStatement, title: "")
            return false
            
        }
        else if !txtRefByEmail.isEmpty{
            if !isValidEmail(email: txtRefByEmail.text!){
                showAlert(message: kEnterReferredValidEmail, title: "")
                return false
            }
            return true
        }
        else{
            return true
        }
    }

}

//MARK: UITextView Delegates

extension MentorRegistration: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
        if(textView.text == "SHARE A BRIEF STATEMENT ON YOUR EXPERIENCE OR OFFER SOME ADVICE:") {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
        if(textView.text == "") {
            textView.textColor = UIColor.darkGray
            textView.text = "SHARE A BRIEF STATEMENT ON YOUR EXPERIENCE OR OFFER SOME ADVICE:"
        }
    }
}

//MARK: Picker Delegates
extension MentorRegistration: BaseVCDelegate {
    
    func didSelectPickerViewAtIndex(index: Int) {
        txtCareerField.text = arrayOfCareerFeild[index]
       
    }
    func didClickOnDoneButton() {
        if txtCareerField.isEmpty {
             txtCareerField.text = arrayOfCareerFeild[0]
        }
    }
}
//MARK: Webservice Methods
extension MentorRegistration{
    
    func callWebServiceForMentorRegistor(){
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kName]           = txtName.text
        parametersDict[APIKeys.kEmailId]        = txtEmail.text
        parametersDict[APIKeys.kCareer]         = txtCareerField.text
        parametersDict[APIKeys.kYearsOfExp]     = txtYearOfExp.text
        parametersDict[APIKeys.kReferredEmail]  = txtRefByEmail.text
        parametersDict[APIKeys.kDescription]    = adviceTxtView.text
        parametersDict[APIKeys.kToken]          = DataManager.token
        print(parametersDict)
        
        UserVM.sharedInstance.mentorRegister(mentorDetails:parametersDict as NSDictionary!,  imageDict: imageDict, applicationDict: resumeDict,mineType:minetype, response: { (success, message, error) in
            if success {
                DataManager.isUserLoggedIn = false
                
                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                    self.logout()
                })
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }

}


//MARK: UITextView and UITextField Delegates

extension MentorRegistration: UITextFieldDelegate {
    
    //Textfield
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            if txtEmail.isEmpty {
             return true
            }
            return false
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
       
        
        if textField == txtName {
            let newLength = currentCharacterCount + string.characters.count - range.length
            if validateNameTextFeild(string: string)
            {
                return newLength <= 25
            }else{
                return false
            }
        }
        
        return true
    }
    
}
