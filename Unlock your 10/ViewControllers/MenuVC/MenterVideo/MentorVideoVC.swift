//
//  MentorVideoVC.swift
//  Unlock your 10
//
//  Created by Brst on 09/01/18.
//  Copyright © 2018 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import TwilioVideo
class MentorVideoVC: BaseViewController

{
    
    // MARK: View Controller Members
    
    var accessToken = String()
    // user MentorUser
   // var accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzI1MzRkNzYxYjMwZjBhYThiNDg3OTljNmYxNzQxOWVhLTE1MTYwMTUwNTUiLCJpc3MiOiJTSzI1MzRkNzYxYjMwZjBhYThiNDg3OTljNmYxNzQxOWVhIiwic3ViIjoiQUMxMWRkNzVhNGYzMWNmYzEyZTZmOWNmMGJlODRjOTY1MiIsImV4cCI6MTUxNjAxODY1NSwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiVXNlciIsInZpZGVvIjp7fX19.xT6Izx1zeeb9QQkYvAk2uAu8Koa0Z3Q7Q7lFZ5Ktnu0"
    
    // Video SDK components
    var room: TVIRoom?
    var camera: TVICameraCapturer?
    var localVideoTrack: TVILocalVideoTrack?
    var localAudioTrack: TVILocalAudioTrack?
    //    var participant: TVIParticipant?
    //var remoteView: TVIVideoView?
    
    // MARK: UI Element Outlets and handles
    
    // `TVIVideoView` created from a storyboard
    @IBOutlet weak var previewView: TVIVideoView!
    //   @IBOutlet weak var remoteView: TVIVideoView!
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var connectionLabel: UILabel!
    //@IBOutlet weak var participantTbl: UITableView!
    @IBOutlet weak var participantCollView: UICollectionView!
    var participantsArray = [TVIParticipant]()
    var roomName: String!
    var room_ID: String!
    var sessionDetailsDic = NSDictionary()
    var joinDataArr = [NSDictionary]()
    @IBOutlet weak var backButton: UIButton!
    // MARK: UIViewController
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var videoImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        disconnectButton.isHidden = true
        backButton.isHidden = true
        participantCollView.delegate = self
        roomName = "abc"
        liveLabel.isHidden = true
        videoImg.isHidden = true
        //Update Twillio Token Initially
         updateTwillioToken(completion: {(result) in
            self.accessToken = TwilioAccessToken
            if result == true {
                
            }
            self.prepareLocalMedia()
            self.connectCamera()
            DispatchQueue.main.async {
             self.previewView!.contentMode = .scaleAspectFill
            }
            //Timer.scheduledTimer(timeInterval: 3500, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
       
         })
       
    }

    func catchNotification(notification:Notification) -> Void {
        self.callWebserviceForGetDetailsOfTraining()
    }
    override func viewWillAppear(_ animated: Bool) {
        //callWebserviceForGetDetailsOfTraining()
        //callWebserviceForChnageStatusOfTraining(roomID: "1234569", status: "1")
    }
    //MARK: Button Action
    @IBAction func backAction(_ sender: Any) {
        backButtonAction()
    }
    // MARK: IBActions
     func connectCamera() {
        
        // Prepare local media which we will share with Room Participants.
//        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = TVIConnectOptions.init(token: accessToken) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [TVILocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [TVILocalVideoTrack]()
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = self.roomName
            //  builder.roomName = "myroom"
        }
        
        // Connect to the Room using the options we provided.
        room = TwilioVideo.connect(with: connectOptions, delegate: self)
        let name: String! = "Connect to room "+String(describing:"\(room!.name)")
        logMessage(messageText: name)
        
        self.showRoomUI(inRoom: true)
    }
    @IBAction func CloseAction(sender: AnyObject) {
        
//        callWebserviceForChnageStatusOfTraining(roomID: room_ID, status: "2")
//       //self.room?.disconnect()
//        backButtonAction()
//        let name: String! = "Disconnect to room "+String(describing:"\(room!.name)")
//        logMessage(messageText: name)
        showVideoSessionAlertView(message: "Do you really want to finish training session?")
        
    }
    func finishVideoSession() {
        
        if disconnectButton.titleLabel?.text == "Close" {
            backButtonAction()
        }
        else {
            // DispatchQueue.main.async {
            callWebserviceForChnageStatusOfTraining(roomID: room_ID, status: "2")
            self.room!.disconnect()
            let name: String! = "Disconnect to room "+String(describing:"\(room!.name)")
            logMessage(messageText: name)
           
                self.backButtonAction()
           // }
            
        }
    }
//    @IBAction func disconnect(sender: AnyObject) {
//        if disconnectButton.titleLabel?.text == "Close" {
//            backButtonAction()
//        }
//        else {
//        self.room!.disconnect()
//        let name: String! = "Disconnect to room "+String(describing:"\(room!.name)")
//        logMessage(messageText: name)
//            DispatchQueue.main.async {
//                self.backButtonAction()
//            }
//
//        }
//    }
//
    @IBAction func toggleMic(sender: AnyObject) {
        if (self.localAudioTrack != nil) {
            self.localAudioTrack?.isEnabled = !(self.localAudioTrack?.isEnabled)!
            
            // Update the button title
            if (self.localAudioTrack?.isEnabled == true) {
                self.micButton.setTitle("Mute", for: .normal)
            } else {
                self.micButton.setTitle("Unmute", for: .normal)
            }
        }
    }
    
    // MARK: Private
    func startPreview() {
//        if PlatformUtils.isSimulator {
//            return
//        }
        
        // Preview our local camera track in the local video preview view.
        camera = TVICameraCapturer(source: .frontCamera, delegate: self)
        localVideoTrack = TVILocalVideoTrack.init(capturer: camera!)
        if (localVideoTrack == nil) {
            logMessage(messageText: "Failed to create video track")
        } else {
            // Add renderer to video track for local preview
            localVideoTrack!.addRenderer(self.previewView)
            
            logMessage(messageText: "Video track created")
            
            // We will flip camera on tap.
             DispatchQueue.main.async {
            let tap = UITapGestureRecognizer(target: self, action: #selector(MentorVideoVC.flipCamera))
            self.previewView.addGestureRecognizer(tap)
            }

            
        //    connect()
        }
        //Connect Action
        //connect()
    }
    
    func flipCamera() {
        if (self.camera?.source == .frontCamera) {
            self.camera?.selectSource(.backCameraWide)
        } else {
            self.camera?.selectSource(.frontCamera)
        }
    }
    
    func prepareLocalMedia() {
        
        // We will share local audio and video when we connect to the Room.
        
        // Create an audio track.
        if (localAudioTrack == nil) {
            localAudioTrack = TVILocalAudioTrack.init()
            
            if (localAudioTrack == nil) {
                logMessage(messageText: "Failed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (localVideoTrack == nil) {
            self.startPreview()
        }
    }
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
       
       // self.disconnectButton.isHidden = !inRoom
         DispatchQueue.main.async {
        UIApplication.shared.isIdleTimerDisabled = inRoom
        }
    }
    

    
    func logMessage(messageText: String) {
       // messageLabel.text = messageText
    }
    
}
// MARK: UITextFieldDelegate
extension MentorVideoVC : UITextFieldDelegate {
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.connect(sender: textField)
//        return true
//    }
}

// MARK: TVIRoomDelegate
extension MentorVideoVC : TVIRoomDelegate {
    func didConnect(to room: TVIRoom) {
        room_ID = room.name
        disconnectButton.isHidden = false
        liveLabel.isHidden = false
        videoImg.isHidden = false
        let name: String! = "Connected to room "+String(describing:"\(room.name)")
        logMessage(messageText: name)
        //(String(describing: room.localParticipant?.identity))")
        connectionLabel.isHidden = true
        
       // _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(MentorVideoVC.update), userInfo: nil, repeats: true)
        
        callWebserviceForGetDetailsOfTraining()
        callWebserviceForChnageStatusOfTraining(roomID: room_ID, status: "1")
    }
//    // must be internal or public.
//    @objc func update() {
//
//    }
    func room(_ room: TVIRoom, didDisconnectWithError error: Error?) {
        disconnectButton.setTitle("Close", for: .normal)
        disconnectButton.isHidden = false
        connectionLabel.text = "Failed to connect"
        connectionLabel.isHidden = false
        logMessage(messageText: "Disconncted from room \(room.name), error = \(String(describing: error))")
        self.room = nil
        participantsArray.removeAll()
        participantCollView.reloadData()
        self.showRoomUI(inRoom: false)
    }
    
    func room(_ room: TVIRoom, didFailToConnectWithError error: Error) {
        print(error)
        print(error.localizedDescription)
        connectionLabel.text = "Failed to connect"
        logMessage(messageText: "Failed to connect to room with error")
        self.room = nil
        disconnectButton.setTitle("Close", for: .normal)
        connectionLabel.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.backButtonAction()
        }
        participantsArray.removeAll()
        participantCollView.reloadData()
        self.showRoomUI(inRoom: false)
    }
    
    func room(_ room: TVIRoom, participantDidConnect participant: TVIParticipant) {
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) connected")
        
        participantsArray.append(participant)
        print(participantsArray.count)
        participantCollView.reloadData()
        connectionLabel.isHidden = true
        liveLabel.isHidden = false
        videoImg.isHidden = false
        
        callWebserviceForGetDetailsOfTraining()
    }
    
    func room(_ room: TVIRoom, participantDidDisconnect participant: TVIParticipant) {
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) disconnected")
        let array = participantsArray.filter { (participants1) -> Bool in
            if participants1 == participant {
                return false
            }
            return true
            
        }
        callWebserviceForGetDetailsOfTraining()
        participantsArray = array
        //print(participantsArray.count)
        //participantCollView.reloadData()
    }
}

// MARK: TVIParticipantDelegate
extension MentorVideoVC : TVIParticipantDelegate {
    
    func participant(_ participant: TVIParticipant, addedVideoTrack videoTrack: TVIVideoTrack) {
        logMessage(messageText: "Participant \(participant.identity) added video track")
        print(participant.identity)
        
        
        
    }
    
    func participant(_ participant: TVIParticipant, removedVideoTrack vrideoTrack: TVIVideoTrack) {
        logMessage(messageText: "Participant \(participant.identity) removed video track")
    }
    
    func participant(_ participant: TVIParticipant, addedAudioTrack audioTrack: TVIAudioTrack) {
        logMessage(messageText: "Participant \(participant.identity) added audio track")
    }
    
    func participant(_ participant: TVIParticipant, removedAudioTrack audioTrack: TVIAudioTrack) {
        logMessage(messageText: "Participant \(participant.identity) removed audio track")
    }
    
    func participant(_ participant: TVIParticipant, enabledTrack track: TVITrack) {
        var type = ""
        if (track is TVIVideoTrack) {
            type = "video"
        } else {
            type = "audio"
        }
        logMessage(messageText: "Participant \(participant.identity) enabled \(type) track")
    }
    
    func participant(_ participant: TVIParticipant, disabledTrack track: TVITrack) {
        var type = ""
        if (track is TVIVideoTrack) {
            type = "video"
        } else {
            type = "audio"
        }
        logMessage(messageText: "Participant \(participant.identity) disabled \(type) track")
    }
}
// MARK: TVIVideoViewDelegate
extension MentorVideoVC : TVIVideoViewDelegate {
    func videoView(_ view: TVIVideoView, videoDimensionsDidChange dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

// MARK: TVICameraCapturerDelegate
extension MentorVideoVC : TVICameraCapturerDelegate {
    func cameraCapturer(_ capturer: TVICameraCapturer, didStartWith source: TVICameraCaptureSource) {
        self.previewView.shouldMirror = (source == .frontCamera)
    }
}
extension MentorVideoVC: UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return joinDataArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MentorCollViewCell
        
        if joinDataArr.count != 0 {
        let dicData = joinDataArr[indexPath.row]
        
        let joined_user_info = dicData.object(forKey: "joined_user_info") as! NSDictionary
        print(dicData)
        cell.participantName?.text = (joined_user_info.object(forKey: "name") as! String)
       
        let url = (joined_user_info.object(forKey: "profile_pic") as! String)
        let imagUrl = URL(string: url)
        cell.participantImg?.sd_setImage(with: imagUrl, placeholderImage: #imageLiteral(resourceName: "user"))
        cell.participantImg?.layer.cornerRadius =  (cell.participantImg?.frame.size.width)!/2
        cell.participantImg?.layer.masksToBounds = true
        }
        return cell
    }
}
extension MentorVideoVC
{
    
    //Call Api For Change Training Status
    func callWebserviceForChnageStatusOfTraining(roomID: String,status: String) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kTrainingId]     = sessionTrainingID
        parametersDict[APIKeys.KTwillioID]      = roomID
        parametersDict[APIKeys.KTVideoStatus]   = status
        print(parametersDict)
        
        let Url = String(format: BASE_USER_URL+"change_traning_status")
        postVideoStatus(parameters: parametersDict, Url: Url)
    
        
    }
    
    //Call Api For Get Details Of Training Session
    func callWebserviceForGetDetailsOfTraining() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kTrainingId]     = sessionTrainingID

        print(parametersDict)
        
        
        let Url = String(format: BASE_USER_URL+"training_detail")
        guard let serviceUrl = URL(string: Url) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parametersDict, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                    print(json)
                    let message = json.object(forKey: "message") as! String
                    if message == "No training session found." { }
                        else {
                    DispatchQueue.main.async {
                        self.sessionDetailsDic = json[APIKeys.kData] as! NSDictionary
                        self.joinDataArr = self.sessionDetailsDic.object(forKey:"join_data") as! [NSDictionary]
                        print( self.joinDataArr)
                        if self.participantCollView != nil {
                        self.participantCollView.reloadData()
                        }
                    }
                    }
                }catch {
                    print(error)
                }
            }
            }.resume()
    }
    
    
    
    //Post Video Status
    func postVideoStatus(parameters: Any,Url: String) {
        guard let serviceUrl = URL(string: Url) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                }catch {
                    print(error)
                }
            }
            }.resume()
    }
}
extension MentorVideoVC
{
    func showVideoSessionAlertView(message: String) {
        // create the alert
        let alert = UIAlertController(title: "", message: message , preferredStyle: UIAlertControllerStyle.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:  { action in
            self.finishVideoSession()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:  { action in
        }))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    


}

