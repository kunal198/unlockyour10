//
//  MentorCollViewCell.swift
//  Unlock your 10
//
//  Created by Brst on 10/01/18.
//  Copyright © 2018 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class MentorCollViewCell: UICollectionViewCell {
    @IBOutlet weak var participantName = UILabel()
    @IBOutlet weak var participantImg = UIImageView()
}
