//
//  LunchBreakTVCell.swift
//  Unlock your 10
//
//  Created by Brst on 03/01/18.
//  Copyright © 2018 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class LunchBreakTVCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
