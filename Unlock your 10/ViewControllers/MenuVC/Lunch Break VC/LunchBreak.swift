
//
//  LunchBreak.swift
//  Unlock your 10
//
//  Created by Brst-Pc109 on 16/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import GooglePlaces

class LunchBreak: BaseViewController,BaseVCDelegate {
    
    //Outlets
    @IBOutlet var cuisineBackgroundView: UIView!
    @IBOutlet var locationBackgroundView: UIView!
    @IBOutlet var dateBackgroundView: UIView!
    @IBOutlet var usernameBackgroundView: UIView!
    @IBOutlet var btnMenuOutlet: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var searchBtn: UIButton!
    @IBOutlet var cuisineTxt: UITextField!
    @IBOutlet var locationTxt: UITextField!
    @IBOutlet var dateTimeText: UITextField!
    @IBOutlet var userNameTxt: UITextField!
    @IBOutlet var userListTblView: UITableView!
    var userNameListArr = [NSDictionary]()
    var Index = Int()
    var iscommingFrom = String()
    var dateStr: String!
    var fieldtag = Int()
    var submitTag = Int()
    var otherUseId = Int()
    var emptyFieldCheck = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        emptyFieldCheck = false
        //cuisineTxt.tag = 0
        locationTxt.tag = 1
        fieldtag = 0
        pickerDelegate = self
        locationTxt.delegate = self
        cuisineTxt.delegate  = self
        userNameTxt.tag = 50
        userNameTxt.delegate = self
        setDatePickerView(textField: dateTimeText, mode: 100)
    }
    func didClickOnDoneButton() {
        self.view.endEditing(true)
        if dateStr != nil
        {
            let fullNameArr = dateStr.components(separatedBy: "+")
            let name: String! = fullNameArr[0]
            dateTimeText.text = name
        }
        else
        {
            dateTimeText.text =  Date().toString() // convert date to string with userdefined format.
            
        }
        
        
    }
    
    func didSelectPickerViewAtIndex(index: Int)
    {
        Index = index
    }
    func didSelectDatePicker(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        //dateFormatter.timeZone = TimeZone.current //Current time zone
        dateStr = dateFormatter.string(from: date) //according to date format your date string
        print(dateStr) //Convert String to Date
    }
    //MARK: CustomizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        cuisineBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        locationBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        dateBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        usernameBackgroundView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        submitTag = 0
        self.userListTblView.isHidden = true
        userNameListArr.removeAll()
        if iscommingFrom == kHomeVC {
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "back"), for: .normal)
            //searchBtn.setTitle(kSubmit, for: .normal)
        }
        else{
            btnMenuOutlet.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
            //searchBtn.setTitle(kSearch, for: .normal)
        }
        if emptyFieldCheck == true {
            emptyFields()
        }
    }

    //MARK: Button Action
    @IBAction func MenuAction(_ sender: Any) {
        // slideMenuController()?.toggleLeft()
        
        if iscommingFrom == kHomeVC {
            backButtonAction()
        }else{
            emptyFieldCheck = true
            slideMenuController()?.toggleLeft()
            
        }
    }
    //MARK: Button Action
    @IBAction func submitAction(_ sender: Any) {
        
        if (cuisineTxt.text?.isEmpty)!{
            showAlert(message: kCuisineName, title: "")
        }else if locationTxt.isEmpty{
            showAlert(message: kLocation, title: "")
        }
        else if dateTimeText.isEmpty {
            showAlert(message: kDate, title: "")
        }
        else if userNameTxt.isEmpty {
            showAlert(message: kRequestUserName, title: "")
        }
        else{
                self.callWebserviceForAddDateRequest(otheruser_id:otherUseId)
                submitTag = 0
    
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension LunchBreak {
    //Call Api For Add Date Request
    func callWebserviceForCheckName() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kName]           = userNameTxt.text
        
        
        print(parametersDict)
        
        UserVM.sharedInstance.checkName(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.userNameListArr = UserVM.sharedInstance.self.userNameListArr
                print(self.userNameListArr)
                DispatchQueue.main.async {
                    self.userListTblView.isHidden = false
                    self.userListTblView.reloadData()
                }
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    func emptyFields(){
        self.cuisineTxt.text = ""
        self.locationTxt.text = ""
        self.dateTimeText.text = ""
        self.userNameTxt.text = ""
    }
    //Call Api For Add Date Request
    func callWebserviceForAddDateRequest(otheruser_id: Int) {
        
        if otheruser_id == 0
        {
            self.showAlert(message: "No user Found", title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
            })
        }
        else
        {
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kOtherUserId]    = otheruser_id
        parametersDict[APIKeys.KPlace]          = locationTxt.text
        parametersDict[APIKeys.kDateTime]       = dateTimeText.text
        parametersDict[APIKeys.KCusineName]     = cuisineTxt.text
        
        print(parametersDict)
        
        UserVM.sharedInstance.addDateRequest(sessionDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                  self.emptyFields()
                    self.otherUseId = 0
                    DispatchQueue.main.asyncAfter(deadline: .now()+1)
                    {
                        self.backButtonAction()
                    }
                })
                
            }
            else {
                if message != nil {
                    self.otherUseId = 1
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    }
}
extension LunchBreak: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchBtn.isEnabled = false
        if textField.tag == 1 {
            emptyFieldCheck = false
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            present(acController, animated: true, completion: nil)
            
            fieldtag = textField.tag
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if newLength<1 && textField.tag == 50 {
            userListTblView.isHidden = true
            userNameListArr.removeAll()
            submitTag = 0
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchBtn.isEnabled = true
       if textField.tag == 50 {
             callWebserviceForCheckName()
        }
    }
}
extension LunchBreak: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didSelect prediction: GMSAutocompletePrediction) -> Bool {
        
        self.locationTxt.attributedText = prediction.attributedFullText
        //}
        dismiss(animated: true, completion: nil)
        return true;
    }
}
extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
}
extension LunchBreak: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userNameListArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LunchBreakTVCell
        
        let userDic = userNameListArr[indexPath.row]
        
        cell.nameLabel.text = (userDic.object(forKey: "name")  as! String)
        let imgUrl = userDic.object(forKey: "profile_pic") as! String
//        if imgUrl == "" {
//             cell.userImg.image = #imageLiteral(resourceName: "user")
//        }
//        else {
        cell.userImg.sd_setImage(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "dummyUser"))
       // }
        
        cell.userImg.layer.cornerRadius =  cell.userImg.frame.size.width / 2
        cell.userImg.clipsToBounds = true
        
       cell.userImg.setRounded()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.userListTblView.isHidden  = true
        
        let userDic = userNameListArr[indexPath.row]
        otherUseId = userDic.object(forKey: "user_id") as! Int
        userNameTxt.text = userDic.object(forKey: "name") as? String
        submitTag = 1
        
    }
}
extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
}
}
