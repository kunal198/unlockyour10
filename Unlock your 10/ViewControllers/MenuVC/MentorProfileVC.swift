//
//  MentorProfileVC.swift
//  Unlock your 10
//
//  Created by brst on 11/2/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit


class MentorProfileVC: BaseViewController {

    //OUTLETS
    
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblCareerField: UILabel!
    @IBOutlet var lblYearOfExp: UILabel!
    @IBOutlet var txtBriefDesc: UITextView!

    
    
    //VARIABLES
    
 //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customizeUI()
    }
    
    
    func customizeUI()  {
        self.addLeftMenuGesture()
        self.hideNavigationBar()
        imgProfile.sd_setImage(with: URL(string:DataManager.userImage!), placeholderImage: #imageLiteral(resourceName: "user"))
        lblName.text = DataManager.name
        lblCareerField.text = DataManager.careerField!
        lblYearOfExp.text   = "Years Of Experience: \(DataManager.yearOfExp!)"
        txtBriefDesc.text   = DataManager.description!
        
    }
    
    //MARK: Buttons Action
    @IBAction func btnViewResumeAction(_ sender: Any) {
        
        UIApplication.shared.open(URL(string:DataManager.resume!)!, options: [:],
                                  completionHandler: nil)
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        
        animationPushWithIdentifier(identifier: kMentorProfileToEditProfile)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
