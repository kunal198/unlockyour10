//
//  InboxVC.swift
//  Unlock your 10
//
//  Created by brst on 10/13/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class InboxVC: BaseViewController {

//Outlets
    
    
    @IBOutlet var tableViewInbox: UITableView!
    @IBOutlet var lblNoChatFound: UILabel!
    
    @IBOutlet var btnCreateMessageOutlet: UIButton!
    
//Variables
    var arrayOfChats = [NSDictionary]()
    var isCallWebService = false
    var paginationDict = NSDictionary()
    var leftMenuSelf: LeftMenuVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        
    }
 //MARK:
//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //check the role of the user
        if DataManager.role == 2 {
            btnCreateMessageOutlet.isHidden = false
        }else{
            btnCreateMessageOutlet.isHidden = true
        }
        UserVM.sharedInstance.threadId = ""
        callWebserviceForInbox(page: 1,isScrollToTop:true)
        
    }
 //MARK:
 //MARK: Buttons Action
    @IBAction func CreateMessage(_ sender: Any) {
        let createMessageVC = UIStoryboard(storyboard: .Menu).instantiateViewController(withIdentifier: "CreateMessageVC") as! CreateMessageVC
        createMessageVC.iscommingFrom = "HomeNoContact"
        animationPushWithViewController(viewController: createMessageVC)
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
//MARK: Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kInboxToChat ,
            let nextScene = segue.destination as? ChatVC ,
            let indexPath = self.tableViewInbox.indexPathForSelectedRow {
            let selectedThread = arrayOfChats[indexPath.row]
            UserVM.sharedInstance.threadId = selectedThread[APIKeys.kThreadId] as! String
            nextScene.otherUserId = Int(selectedThread[APIKeys.kOtherUserId] as! String)!
            nextScene.leftMenu = leftMenuSelf
        }
    }
    
    func scrollToTop()  {
        tableViewInbox.setContentOffset(CGPoint.zero, animated: true)
    }
    
    
}
//MARK:
//MARK: UITableView Methods
extension InboxVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath) as! InboxCell
        customizeCell(indexPath: indexPath, cell: cell)
        return cell
    }
    
    // Can Edit Row Delegats
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
            let dataDict = self.arrayOfChats[indexPath.row]

            self.showAlert(message: "Are you sure you want to delete this thread?", title: "", otherButtons: [kYes: { action in
                
                self.callWebserviceForDeleteThread(threadId: dataDict[APIKeys.kThreadId] as! String, indexPath: indexPath)
                
            }], cancelTitle: kNo, cancelAction: { (action) in

            })
            
        })
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        animationPushWithIdentifier(identifier: kInboxToChat)
    }
    
    // customize cell
    func customizeCell(indexPath: IndexPath,cell:InboxCell)  {
      
        cell.shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
        let dataDict = arrayOfChats[indexPath.row]
        cell.lblName.text = dataDict[APIKeys.kName] as? String
        cell.imgProfile.sd_setImage(with: URL(string:(dataDict[APIKeys.kProfilePic] as? String)!), placeholderImage: #imageLiteral(resourceName: "dummyUser"))
        cell.lblMessage.text = dataDict[APIKeys.kMessage] as? String
        //check date 
        let createdDateStr = dataDict[APIKeys.kCreated_at] as? String
        let createdDate = createdDateStr?.dateFromString(format: .dateTime)
        
        if createdDate?.isToday() == true {
            cell.lblDate.text = createdDate?.stringFormDateInLocalTimeZone(format: .time)
        }else{
            
            cell.lblDate.text = createdDate?.stringFormDateInLocalTimeZone(format: .mdyDate)
        }
        
        
        //check the unread message count.
        if dataDict[APIKeys.kUnreadCount] as! String == "0" {
            cell.lblBadge.isHidden = true
        }else{
            cell.lblBadge.isHidden = false
            cell.lblBadge.text = dataDict[APIKeys.kUnreadCount] as? String
        }
        
        //check the role of user
        if dataDict[APIKeys.kUserRole] as! Int == 2 {
            cell.imgLock.isHidden = true
        }else{
            cell.imgLock.isHidden = false
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView == tableViewInbox{
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if arrayOfChats.count > 9 {
                    if paginationDict[APIKeys.kNextpage] as! Int != paginationDict[APIKeys.kCurrentpage] as! Int{
                if isCallWebService == true{
                    isCallWebService = false
                    tableViewInbox.tableFooterView!.isHidden = false
                    Indicator.isEnabledIndicator = false
                    //call the webservice
                    callWebserviceForInbox(page: paginationDict[APIKeys.kNextpage] as! Int,isScrollToTop:false)
                }
               }
             }
           }
        }
     }
  }
//MARK:
//MARK: WebService For Inbox

extension InboxVC {
    
    func callWebserviceForInbox(page:Int,isScrollToTop:Bool) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kPage]           = page

        
        MessageVM.sharedInstance.getChatThreads(threadDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            Indicator.isEnabledIndicator = true
            if success {
                //append the next values
                if page != 1{
                    for obj in MessageVM.sharedInstance.listArray{
                        self.arrayOfChats.append(obj)
                    }
                }else{
                    self.arrayOfChats   = MessageVM.sharedInstance.listArray
                }
                
                
                if self.arrayOfChats.count == 0{
                    self.lblNoChatFound.isHidden = false
                }else{
                    self.lblNoChatFound.isHidden = true
                }
                
                self.tableViewInbox.tableFooterView!.isHidden = true
                self.paginationDict = MessageVM.sharedInstance.pagination
                self.tableViewInbox.reloadData() //reload the table
                self.isCallWebService = true
                
                if isScrollToTop{
                    self.scrollToTop()
                }
            }
            else {
                self.tableViewInbox.tableFooterView!.isHidden = true
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }else if message == kNoChatFound{
                        self.lblNoChatFound.isHidden = false
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    func callWebserviceForDeleteThread(threadId:String,indexPath:IndexPath) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kThreadId]       = threadId
        
        MessageVM.sharedInstance.deleteThread(threadDetail: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                //append the next values
                self.arrayOfChats.remove(at: indexPath.row)
                self.tableViewInbox.deleteRows(at: [indexPath], with: .automatic)
                
                if self.arrayOfChats.count == 0{
                    self.lblNoChatFound.isHidden = false
                }else{
                    self.lblNoChatFound.isHidden = true
                }
            }
                
            else {
                self.tableViewInbox.tableFooterView!.isHidden = true
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
}



