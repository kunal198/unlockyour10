//
//  UserVideoVC.swift
//  Unlock your 10
//
//  Created by Brst on 09/01/18.
//  Copyright © 2018 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import TwilioVideo
class UserVideoVC: BaseViewController {
    
    // MARK: View Controller Members
    
    var accessToken = String()
    // Video SDK components
    var room: TVIRoom?
    var camera: TVICameraCapturer?
    var participant: TVIParticipant?
    
    // MARK: UI Element Outlets and handles
    
    // `TVIVideoView` created from a storyboard
    @IBOutlet weak var remoteView: TVIVideoView!
    
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
     @IBOutlet weak var connectionLabel: UILabel!
    @IBOutlet weak var liveLabel: UILabel!
    @IBOutlet weak var videoImg: UIImageView!
    var roomName: String!
    var room_ID: String!
    var timer = Timer()
    // MARK: UIViewController
   let myNotification = Notification.Name(rawValue:"MyNotification")

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        disconnectButton.isHidden = true
        backButton.isHidden = true
        roomName = "abc"
       liveLabel.isHidden = true
        videoImg.isHidden = true
        //Update Twillio Token Initially
            updateTwillioToken(completion: {(result) in
                self.accessToken = TwilioAccessToken
                if result == true { }
                self.connectCamera()
            })
        
       // print("ViewDidLoad\(self.remoteView)")
      
        //NotificAtion center
        let nc = NotificationCenter.default
        nc.addObserver(forName:myNotification, object:nil, queue:nil, using:catchNotification)
    }
      func setTimer() {
        print(timer)
   
        if  UserDefaults.standard.object(forKey: "LeaveSession") != nil {
            callWebserviceForLeaveTrainigSession()
            
            UserDefaults.standard.removeObject(forKey: "LeaveSession")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear\(self.remoteView)")
        self.navigationController?.navigationBar.isHidden = true
        
    }
    func catchNotification(notification:Notification) -> Void {
        MentorVideoVC().self.callWebserviceForGetDetailsOfTraining()
    }
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear\(self.remoteView)")
     
        
    }
    
    func setupRemoteVideoView() {
        
        self.remoteView?.delegate = self
        print("setupRemoteVideoView\(self.remoteView)")
        self.remoteView!.contentMode = .scaleAspectFill;
    }
    //MARK: Button Action
    @IBAction func backAction(_ sender: Any) {
        backButtonAction()
    }
    // MARK: IBActions
    func connectCamera() {
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = TVIConnectOptions.init(token: accessToken) { (builder) in
            
            builder.roomName = self.roomName
        }
        // Connect to the Room using the options we provided.
        room = TwilioVideo.connect(with: connectOptions, delegate: self)
        let name: String! = "Connect to room "+String(describing:"\(room!.name)")
        logMessage(messageText: name)
        
        self.showRoomUI(inRoom: true)
    }
    
    @IBAction func disconnect(sender: AnyObject) {
        showVideoSessionAlertView(message: "Do you really want to leave training session?")
    }
    func leaveUserVideoSession() {
        if disconnectButton.titleLabel?.text == "Close" {
            backButtonAction()
        }
        else {
            
            self.room!.disconnect()
            let name: String! = "Disconnect to room "+String(describing:"\(room!.name)")
            logMessage(messageText: name)
            //Leave Training Session
            callWebserviceForLeaveTrainigSession()
        }
    }
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        UIApplication.shared.isIdleTimerDisabled = inRoom
    }
    
    func cleanupRemoteParticipant() {
        if ((self.participant) != nil) {
            if ((self.participant?.videoTracks.count)! > 0) {
                self.participant?.videoTracks[0].removeRenderer(self.remoteView!)
            }
        }
        self.participant = nil
    }
    
    func logMessage(messageText: String) {
        //messageLabel.text = messageText
    }
    
}

// MARK: TVIRoomDelegate
extension UserVideoVC : TVIRoomDelegate {
    func didConnect(to room: TVIRoom) {
        // At the moment, this example only supports rendering one Participant at a time.
        
        logMessage(messageText: "Connected to room \(room.name)")
        //as \(String(describing: room.localParticipant?.identity))
        print(room.participants)
        for participant in room.participants {
            print(participant.identity)
            print(participant.videoTracks)
         
            print(participant.identity)
            print(String(describing:VideoMenterID))
            if participant.identity == VideoMenterID {
                self.participant = participant
                self.participant?.delegate = self
            }
        }
        //Check Participent is not connected
        if room.participants.count == 0 {
            backButton.isHidden = true
        connectionLabel.text = "Failed to connect"
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.backButtonAction()
            }
        }
        
    }
    //Disconnected
    func room(_ room: TVIRoom, didDisconnectWithError error: Error?) {
        logMessage(messageText: "Disconnct from room \(room.name), error = \(String(describing: error))")
        disconnectButton.setTitle("Close", for: .normal)
        connectionLabel.text = "Failed to connect"
        self.cleanupRemoteParticipant()
        self.room = nil
        self.showRoomUI(inRoom: false)
        disconnectButton.isHidden = true
        liveLabel.isHidden = true
        videoImg.isHidden = true
        backButton.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.backButtonAction()
        }
    }
    func room(_ room: TVIRoom, didFailToConnectWithError error: Error) {
        print(error)
        disconnectButton.setTitle("Close", for: .normal)
        connectionLabel.text = "Failed to connect"
        print(error.localizedDescription)
        logMessage(messageText: "Failed to connect to room with error")
        self.room = nil
        backButton.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.backButtonAction()
        }
        self.showRoomUI(inRoom: false)
        disconnectButton.isHidden = true
    }
    
    func room(_ room: TVIRoom, participantDidConnect participant: TVIParticipant) {
        if (self.participant == nil) {
            if participant.identity == VideoMenterID {
                self.participant = participant
                self.participant?.delegate = self
            }
        }
        liveLabel.isHidden = false
        videoImg.isHidden = false
        backButton.isHidden = true
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) connected")
         connectionLabel.isHidden = true
    }
    func room(_ room: TVIRoom, participantDidDisconnect participant: TVIParticipant) {
        if (self.participant == participant) {
            if participant.identity == VideoMenterID {
                backButton.isHidden = false
                cleanupRemoteParticipant()
                self.leaveUserVideoSession()
               
            }
        }
        
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) disconnected")
        connectionLabel.isHidden = true
    }
}
// MARK: TVIParticipantDelegate
extension UserVideoVC : TVIParticipantDelegate {
    func participant(_ participant: TVIParticipant, addedVideoTrack videoTrack: TVIVideoTrack) {
        DispatchQueue.main.async {
        self.logMessage(messageText: "Participant \(participant.identity) added video track")
        print(participant.identity)
        if (self.participant == participant) {
            self.setupRemoteVideoView()
            print(self.remoteView)
            videoTrack.addRenderer(self.remoteView!)
            self.hideLabel()
        }
        
      
        }
    }
    func hideLabel()
    {
        DispatchQueue.main.asyncAfter(deadline: .now()+4) {
            
            self.disconnectButton.isHidden = false
            self.connectionLabel.isHidden = true
            self.liveLabel.isHidden = false
            self.videoImg.isHidden = false
            self.callWebserviceForJoiningTraining()
        }
    }
    func participant(_ participant: TVIParticipant, removedVideoTrack vrideoTrack: TVIVideoTrack) {
        logMessage(messageText: "Participant \(participant.identity) removed video track")
        backButton.isHidden = false
        if (self.participant == participant) {
            vrideoTrack.removeRenderer(self.remoteView!)
        }
    }
    
    func participant(_ participant: TVIParticipant, addedAudioTrack audioTrack: TVIAudioTrack) {
        logMessage(messageText: "Participant \(participant.identity) added audio track")
        
        
    }
    
    func participant(_ participant: TVIParticipant, removedAudioTrack audioTrack: TVIAudioTrack) {
        logMessage(messageText: "Participant \(participant.identity) removed audio track")
    }
    
    func participant(_ participant: TVIParticipant, enabledTrack track: TVITrack) {
        var type = ""
        if (track is TVIVideoTrack) {
            type = "video"
        } else {
            type = "audio"
        }
        logMessage(messageText: "Participant \(participant.identity) enabled \(type) track")
    }
    
    func participant(_ participant: TVIParticipant, disabledTrack track: TVITrack) {
        var type = ""
        if (track is TVIVideoTrack) {
            type = "video"
        } else {
            type = "audio"
        }
        logMessage(messageText: "Participant \(participant.identity) disabled \(type) track")
    }
}

// MARK: TVIVideoViewDelegate
extension UserVideoVC : TVIVideoViewDelegate {
    func videoView(_ view: TVIVideoView, videoDimensionsDidChange dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}
extension UserVideoVC
{
    //Call Api For Join Training
    func callWebserviceForJoiningTraining() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kTrainingId]     = sessionTrainingID
        print(parametersDict)
        
        let Url = String(format: BASE_USER_URL+"join_training")
        postUserVideoStatus(parameters: parametersDict, Url: Url,chck: 0)

    }
    
    //Call Api For Leave Training
    func callWebserviceForLeaveTrainigSession() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kTrainingId]     = sessionTrainingID
        print(parametersDict)
        
        let Url = String(format: BASE_USER_URL+"leave_training")
        postUserVideoStatus(parameters: parametersDict, Url: Url,chck: 1)

    }
    
    
    func postUserVideoStatus(parameters: Any,Url: String,chck: Int) {
        guard let serviceUrl = URL(string: Url) else { return }
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                    if chck == 1 {
                        DispatchQueue.main.async {
                             self.backButtonAction()
                        }
                    }
                    else {
                        let nc = NotificationCenter.default
                        nc.post(name:self.myNotification,
                                object: nil,
                                userInfo:["message":"Hello there!", "date":Date()])
                    }
                }catch {
                    print(error)
                }
            }
            }.resume()
    }
}
extension UserVideoVC
{
    func showVideoSessionAlertView(message: String) {
        
        // create the alert
        let alert = UIAlertController(title: "", message: message , preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
        
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:  { action in
            self.leaveUserVideoSession()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:  { action in
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
