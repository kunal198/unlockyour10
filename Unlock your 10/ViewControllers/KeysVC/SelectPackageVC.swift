//
//  SelectPackageVC.swift
//  Unlock your 10
//
//  Created by brst on 11/20/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import StoreKit

class SelectPackageVC: BaseViewController {
    
//OUTLETS
  
  //VARIABLES
    var products    = [SKProduct]()
    var packageType = Int()
    var savePackageCount = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }

//MARK: customizeUI
    
    func customizeUI()  {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePurchaseNotifications(_:)),                                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),                                                             object: nil)
 
        if KeysVM.sharedInstance.packageList.count != 0 {
            self.products = KeysVM.sharedInstance.packageList
        }else{
            fetchProducts()
        }
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
    }
    
//MARK: InApp Purchase

    func fetchProducts() {
        products = []
        PackageProduct.store.requestProducts{success, products in
            if success {
                self.products = products!
                self.products.sort{(Double($0.price) < Double($1.price))}
            }
        }
    }
    
    func handlePurchaseNotifications(_ notification: Notification)  {
        
        Indicator.sharedInstance.hideIndicator()    //
        guard let productID = notification.object as? String else { return }
        
        if productID == "com.unlock2Keys" || productID == "com.unlock5Keys" || productID == "com.unlock10Keys" || productID == "com.unlock15Keys" || productID == "com.unlock20Keys"{
            return
        }
        savePackageCount = 0
        callWebserviceForSavePackage(packageType: packageType)
        debugPrint(productID)
        
        //    for (index, product) in products.enumerated() {////        guard product.productIdentifier == productID else { continue }//        self.collectionViewPackage.reloadItems(at: [IndexPath(row: index, section: 0)])////       }
        
    }
    
    
//MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    
    @IBAction func btnCEOKeysAction(_ sender: Any) {
        Indicator.sharedInstance.showIndicator()
        packageType = 1
        let product = products[2]
        KeysProduct.store.buyProduct(product)
        
    }
    
    @IBAction func btnManagerKeysAction(_ sender: Any) {
        Indicator.sharedInstance.showIndicator()
        packageType = 2
        let product = products[1]
        KeysProduct.store.buyProduct(product)
        
    }
    
    @IBAction func btnStaffKeysAction(_ sender: Any) {
        Indicator.sharedInstance.showIndicator()
        packageType = 3
        let product = products[0]
        KeysProduct.store.buyProduct(product)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK: Webservice Method
extension SelectPackageVC{
    
    func callWebserviceForSavePackage(packageType:Int) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 2
        parametersDict[APIKeys.kPackageType]    = packageType
        
        Indicator.isEnabledIndicator = false
        KeysVM.sharedInstance.purchasePackage(packageDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.savePackageCount = 0
                //append the next values
                Indicator.isEnabledIndicator = true
            }
            else {
                
                self.savePackageCount += 1
                if self.savePackageCount < 3{
                    self.callWebserviceForSavePackage(packageType: packageType)
                }
                
                if message != nil {
                   
//                    if message == kTokenNotMatched{   // check other device login
//
//                        DataManager.isUserLoggedIn = false
//                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
//                            self.logout()   // logout the user
//                        })
//                        return
//                    }
                    // self.showAlert(message: message, title: "")
                }
                else {
                    
                    // self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    
    
    
}
