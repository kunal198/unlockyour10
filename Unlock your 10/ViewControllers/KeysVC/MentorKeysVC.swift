
//
//  MentorKeysVC.swift
//  Unlock your 10
//
//  Created by brst on 11/18/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class MentorKeysVC: BaseViewController {
//OUTLETS
    @IBOutlet var balanceBGView: UIView!
    
    @IBOutlet var withdrawlBGVw: UIView!
    
    @IBOutlet var referBGVw: UIView!
    
    @IBOutlet var lblKeysBalance: UILabel!
    
    @IBOutlet var referPopUpVw: UIView!
    
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var withdrawlPopUpVw: UIView!
    
    @IBOutlet var paypalEmailBGVw: UIView!
  
    @IBOutlet var emailBGVw: UIView!

    @IBOutlet var txtPaypalEmail: UITextField!
    
    //VARIABLES
    
    var imgBlur = UIImageView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeVC()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callWebserviceForGetKeys()
    }
   //CustomizeVC
    func customizeVC()  {
        hideNavigationBar()
        initImgBlur()
        // add the shadow of the Views
        balanceBGView.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        withdrawlBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        referBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
      
    }
    
    //MARK: Blur image method
    func initImgBlur()  {
        imgBlur.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        imgBlur.backgroundColor = UIColor.black
        imgBlur.alpha = 0
        imgBlur.isUserInteractionEnabled = true
        referPopUpVw.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
         withdrawlPopUpVw.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    //MARK: Insert pop up method
    func insertPopUp()  {
        
        DispatchQueue.main.async( execute: {
            
            self.view.addSubview(self.imgBlur)
            self.view.addSubview(self.referPopUpVw)
            
            self.imgBlur.fadeInForIndicator()
        })
         emailBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    //Remove pop up method
    func RemovePopUp()  {
        DispatchQueue.main.async( execute: {
            self.imgBlur.fadeOutForIndicator()
            self.referPopUpVw.removeFromSuperview()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 00.50, execute: {
                self.imgBlur.removeFromSuperview()
            })
        })
    }
    
    
    //MARK: Insert pop up method
    func insertWithdrawlPopUp()  {
        
        DispatchQueue.main.async( execute: {
            
            self.view.addSubview(self.imgBlur)
            self.view.addSubview(self.withdrawlPopUpVw)
            
            self.imgBlur.fadeInForIndicator()
        })
          paypalEmailBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    //Remove pop up method
    func RemoveWithdrawlPopUp()  {
        DispatchQueue.main.async( execute: {
            self.imgBlur.fadeOutForIndicator()
            self.withdrawlPopUpVw.removeFromSuperview()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 00.50, execute: {
                self.imgBlur.removeFromSuperview()
            })
        })
    }
  //MARK: BUTTONS ACTION
    
    @IBAction func btnMenuAction(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnWithdrawlRequestAction(_ sender: Any) {
        insertWithdrawlPopUp()
    }
    
    @IBAction func btnWithdrawlCloseAction(_ sender: Any) {
        txtPaypalEmail.text = ""
        RemoveWithdrawlPopUp()
        
    }
    @IBAction func btnReferUserAction(_ sender: Any) {
        insertPopUp()
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
       
        if txtEmail.isEmpty  {
            self.showAlert(message: kEnterEmailFirst, title: "")
        }else  if !isValidEmail(email: txtEmail.text!) {
            self.showAlert(message: kEnterValidEmail, title:"" )
        }else{
            callWebserviceForReferUser()
        }
        
    }
    
    @IBAction func btnWithdrawlSubmitAction(_ sender: Any) {
        if txtPaypalEmail.isEmpty {
            self.showAlert(message:kEnterPaypalEmail, title: "")
        }else  if !isValidEmail(email: txtPaypalEmail.text!) {
            self.showAlert(message: kEnterValidEmail, title:"" )
        }else
        {
            callWebserviceForWithdrawalRequest()
        }
        
    }
    
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.txtEmail.text = ""
        RemovePopUp()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
//MARK: API methods
extension MentorKeysVC{
   
    func callWebserviceForGetKeys() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 3
        
        
        KeysVM.sharedInstance.getKeys(keysDict: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                //append the next values
                DataManager.keysBalance  = KeysVM.sharedInstance.keyValue
                self.lblKeysBalance.text = "You are earned \(KeysVM.sharedInstance.keyValue) keys in your account"
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    else if message == kNoKeysFound{
                        self.lblKeysBalance.text = "No keys found."
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    func callWebserviceForWithdrawalRequest() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 3
        parametersDict[APIKeys.kPaypalEmail]    = txtPaypalEmail.text
        
        KeysVM.sharedInstance.withdrawalRequest(requestDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                // remove popup
                self.RemoveWithdrawlPopUp()
                self.txtPaypalEmail.text = ""
                 self.lblKeysBalance.text = "No keys found."
                self.showAlert(message: message, title: "")

            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    else if message == kNoWihdrawalKeysFound{
                        self.RemoveWithdrawlPopUp()
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    func callWebserviceForReferUser() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 2
        parametersDict[APIKeys.kOtherUserEmail] = txtEmail.text
        
        KeysVM.sharedInstance.referUser(userDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.txtEmail.text = ""
                self.RemovePopUp()
                self.showAlert(message: message, title: "")
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
}


