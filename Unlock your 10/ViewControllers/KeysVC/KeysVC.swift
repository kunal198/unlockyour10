 //
//  KeysVC.swift
//  Unlock your 10
//
//  Created by brst on 10/14/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import StoreKit

class KeysVC: BaseViewController {
//Outlets
    
    @IBOutlet var lblKeysBalance: UILabel!
   
    @IBOutlet var collectionViewPackage: UICollectionView!
    
    @IBOutlet var balanceBGVw: UIView!
    
    @IBOutlet var packageBGVw: UIView!
    
    @IBOutlet var selectPackageBGVw: UIView!
    
    @IBOutlet var referBGVw: UIView!
    
    @IBOutlet var referPopUpVw: UIView!
    
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var emialBGVw: UIView!
    
    
 //VARIABLES
    var imgBlur     = UIImageView()
    var products    = [SKProduct]()
    let arrayKeys   = ["2","5","10","15","20"]
    let arrayPrices = ["$1.99","$4.99","$9.99","$14.99","$19.99"]
    var keysBalance = Int()
    var selectedIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        if KeysVM.sharedInstance.productList.count != 0 {
            self.products = KeysVM.sharedInstance.productList
            self.collectionViewPackage.reloadData()
        }else{
            fetchProducts()
        }
    }
    
//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
        // add the shadow of the Views
        balanceBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        packageBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        selectPackageBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        referBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        emialBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.handlePurchaseNotification(_:)),                                                               name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),                                                             object: nil)
        
        initImgBlur()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        callWebserviceForGetKeys()
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

    }
    
    //MARK: InApp Purchase
    func fetchProducts() {
        products = []
        KeysProduct.store.requestProducts{success, products in
            if success {
                
                self.products = products!
                self.products.sort{(Double($0.price) < Double($1.price))}
                self.collectionViewPackage.reloadData()
            }
        }
    }
   
  func handlePurchaseNotification(_ notification: Notification)
  {
    Indicator.sharedInstance.hideIndicator()
    guard let productID = notification.object as? String else { return }
    if productID == "com.unlockCEOKey" || productID == "com.unlockMANAGERKey" || productID == "com.unlockSTAFFKey"{
        return
}
    
    DataManager.keysBalance = DataManager.keysBalance! + Int(arrayKeys[selectedIndex])!
    lblKeysBalance.text = "You are having \(DataManager.keysBalance!) keys left in your account"
    callWebserviceForSaveKeys(keys: Int(arrayKeys[selectedIndex])!)
//    for (index, product) in products.enumerated() {
//
//        guard product.productIdentifier == productID else { continue }
//        self.collectionViewPackage.reloadItems(at: [IndexPath(row: index, section: 0)])
//
//       }
    
    }
    //MARK: Blur image method
    func initImgBlur()  {
        imgBlur.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        imgBlur.backgroundColor = UIColor.black
        imgBlur.alpha = 0
        imgBlur.isUserInteractionEnabled = true
        referPopUpVw.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    //MARK: Insert pop up method
    func insertPopUp()  {
        DispatchQueue.main.async( execute: {
            
            self.view.addSubview(self.imgBlur)
            self.view.addSubview(self.referPopUpVw)
            
            self.imgBlur.fadeInForIndicator()
        })
        emialBGVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
    }
    
    //Remove pop up method
    func RemovePopUp()  {
        DispatchQueue.main.async( execute: {
            self.imgBlur.fadeOutForIndicator()
            self.referPopUpVw.removeFromSuperview()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 00.50, execute: {
                self.imgBlur.removeFromSuperview()
            })
        })
    }
    
    
    //MARK: Buttons Action
 
    
    @IBAction func btnMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    
    @IBAction func btnPackagesAction(_ sender: Any) {
        animationPushWithIdentifier(identifier: kKeysToPackage)
    }
    
    @IBAction func btnReferUserAction(_ sender: Any) {
        insertPopUp()
        
    }
   
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if txtEmail.isEmpty  {
            self.showAlert(message: kEnterEmailFirst, title: "")
        }else  if !isValidEmail(email: txtEmail.text!) {
            self.showAlert(message: kEnterValidEmail, title:"" )
        }else{
           callWebserviceForReferUser()
        }
        
    }

    @IBAction func btnCloseAction(_ sender: Any) {
        self.txtEmail.text = ""
        RemovePopUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: UIcollectionView Methods

extension KeysVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK: - UICollectionViewDataSource protocol
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 120, height: 112)
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCell, for: indexPath as IndexPath)
        let lblKeys   = cell.viewWithTag(10) as! UILabel
        let lblPrice  = cell.viewWithTag(20) as! UILabel
        lblKeys.text  = "\(arrayKeys[indexPath.row]) Keys @"
        lblPrice.text = arrayPrices[indexPath.row]
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Indicator.sharedInstance.showIndicator()
        let product = products[(indexPath as NSIndexPath).row]
        selectedIndex = indexPath.row
        KeysProduct.store.buyProduct(product)
    }
    
}

 //MARK:
 //MARK: WebService For KeysVC
 
 extension KeysVC {
    
    func callWebserviceForGetKeys() {
        Indicator.isEnabledIndicator = true
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 2
        
        
        KeysVM.sharedInstance.getKeys(keysDict: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                //append the next values
                DataManager.keysBalance  = KeysVM.sharedInstance.keyValue
                self.lblKeysBalance.text = "You are having \(KeysVM.sharedInstance.keyValue) keys left in your account"
                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    else if message == kNoKeysFound{
                        self.lblKeysBalance.text = "No keys found."
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    func callWebserviceForSaveKeys(keys:Int) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 2
        parametersDict[APIKeys.kKeys]           = keys

        Indicator.isEnabledIndicator = false
        KeysVM.sharedInstance.purchaseKeys(keysDict: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                //append the next values
                Indicator.isEnabledIndicator = true

                
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                   // self.showAlert(message: message, title: "")
                }
                else {
                   // self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    func callWebserviceForReferUser() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kUserRole]       = 2
        parametersDict[APIKeys.kOtherUserEmail] = txtEmail.text
        
        KeysVM.sharedInstance.referUser(userDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            if success {
                self.txtEmail.text = ""
                self.RemovePopUp()
                self.showAlert(message: message, title: "")
            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    
    
    
 }

