//
//  ChatVC.swift
//  Unlock your 10
//
//  Created by brst on 11/7/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class ChatVC: BaseViewController,ChatVCDelegate ,UIGestureRecognizerDelegate{

//Outlets
    
    @IBOutlet var tableViewChat: UITableView!
    
    @IBOutlet var chatShadowVw: UIView!
    
    @IBOutlet var txtMessage: UITextView!
    
    @IBOutlet var constraintWidthOfAttachment: NSLayoutConstraint!
    
    @IBOutlet var constraintBottomOfMessageView: NSLayoutConstraint!
    
  //Variables
   
    var messageArary = [NSDictionary]()
    var attachDict = [String:Data]()
    var messageText = String()
    var otherUserId = Int()
    var isCallWebService = false
    var paginationDict = NSDictionary()
    var firstData      = NSDictionary()
    var leftMenuProtocol:LeftMenuProtocol!
    var leftMenu:LeftMenuVC!
    
 //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.delegate = self
        self.tableViewChat.tableHeaderView!.isHidden = true
        callWebserviceForGetChatHistory(page:1,isScrollBottom:true)
        
        hideNavigationBar()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        leftMenuProtocol = leftMenu
    }

    func tapBlurButton(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // UIGestureRecognizerDelegate method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: tableViewChat) == true {
            return false
        }
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            customizeUI()
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
            IQKeyboardManager.sharedManager().enable = false
    }

    override func viewDidDisappear(_ animated: Bool) {
             super.viewDidDisappear(animated)
            NotificationCenter.default.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
             NotificationCenter.default.removeObserver(self)
             IQKeyboardManager.sharedManager().enableAutoToolbar = true
             IQKeyboardManager.sharedManager().enable = true
    }
    
  //Chat Delegate method
    func didTapOnNotification(data:NSDictionary){
        otherUserId = Int(data[APIKeys.kSenderId] as! String)!
        callWebserviceForGetChatHistory(page:1,isScrollBottom:true)

    }
   
//MARK: CustomizeUI
    func customizeUI()  {

        chatShadowVw.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        tableViewChat.estimatedRowHeight = 130
        tableViewChat.rowHeight = UITableViewAutomaticDimension
        
        //keyboard notifications
        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(ChatVC.keyboardWillShow(notification:))
,
                           name: .UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(ChatVC.keyboardWillHide(notification:)),
                           name: .UIKeyboardWillHide,
                           object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callViewWilldisaappear(notification:)), name: .UIApplicationDidEnterBackground, object: nil)
        
    }
 // Keyboard Notification Actions
    
    func callViewWilldisaappear(notification:NSNotification)  {
        
        self.view.endEditing(true)
        
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            constraintBottomOfMessageView.constant = keyboardSize.height
            self.scrollToBottom()
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            constraintBottomOfMessageView.constant = 0
        }
    }
    
//Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }

    @IBAction func btnAttachmentAction(_ sender: Any) {
        //set the temp value to the variable
        messageText = txtMessage.text
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.adobe.pdf"], in: .import)
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
            self.view.endEditing(true)
            if txtMessage.text == "" || txtMessage.text == "Type your message here:"{
                showAlert(message:kEnterTextFirst , title: "")
            }else{
                callWebserviceForSendMessage()
            }
    }
 //scroll to bottom
    
    func scrollToBottom()  {
        DispatchQueue.main.async {
            if self.messageArary.count != 0{
             self.tableViewChat.scrollToRow(at: IndexPath(item:self.messageArary.count-1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: UITableView Methods
extension ChatVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataDict = messageArary[indexPath.row]
        
        //check the message is send by sender or receiver
        if dataDict[APIKeys.kSenderId] as! String != "\(DataManager.userId!)" {
            let cell = tableView.dequeueReusableCell(withIdentifier: kReceiverCell, for: indexPath) as! ReceiverCell
            customizeReceiverCell(indexPath: indexPath, cell: cell,dataDict:dataDict)
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: kSenderCell, for: indexPath) as! SenderCell
            customizeSenderCell(indexPath: indexPath, cell: cell,dataDict: dataDict)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    // customize cells
    func customizeSenderCell(indexPath: IndexPath,cell:SenderCell,dataDict:NSDictionary)  {
        cell.shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        cell.lblName.text = dataDict[APIKeys.kSenderName] as? String
        cell.lblMessage.text = dataDict[APIKeys.kMessage] as? String
        //check the attachtmnt
        if dataDict[APIKeys.kAttachment] as? String == "" {
            cell.btnViewAttachment.isHidden = true
            cell.constraintHeightOfViewAttachment.constant = 0
            cell.lblLine.isHidden = true
        }else{
            cell.btnViewAttachment.isHidden = false
            cell.constraintHeightOfViewAttachment.constant = 25
            cell.lblLine.isHidden = false
        }
        cell.btnViewAttachment.addTarget(self, action: #selector(ChatVC.btnViewAttachmentAction), for: UIControlEvents.touchUpInside)
        cell.btnViewAttachment.tag = indexPath.row
        
        //check date
        let createdDateStr = dataDict[APIKeys.kCreated_at] as? String
        let createdDate = createdDateStr?.dateFromString(format: .dateTime)
        
        if createdDate?.isToday() == true {
            cell.lblTime.text = createdDate?.stringFormDateInLocalTimeZone(format: .time)
        }else{
            cell.lblTime.text = createdDate?.stringFormDateInLocalTimeZone(format: .mdyDate)
        }
        
        
        
    }
    
    func customizeReceiverCell(indexPath: IndexPath,cell:ReceiverCell,dataDict:NSDictionary)  {
        cell.shadowVw?.generateShadowUsingBezierPath(radius: 1.0, opacity: 0.25,allCorner: false)
        cell.lblName.text = dataDict[APIKeys.kSenderName] as? String
        cell.lblMessage.text = dataDict[APIKeys.kMessage] as? String
        //check the attachtmnt
        if dataDict[APIKeys.kAttachment] as? String == "" {
            cell.btnViewAttachment.isHidden = true
            cell.constraintHeightOfViewAttachtment.constant = 0
            cell.lblLine.isHidden = true
        }else{
            cell.btnViewAttachment.isHidden = false
            cell.constraintHeightOfViewAttachtment.constant = 25
            cell.lblLine.isHidden = false
        }
        cell.btnViewAttachment.addTarget(self, action: #selector(ChatVC.btnViewAttachmentAction), for: UIControlEvents.touchUpInside)
        cell.btnViewAttachment.tag = indexPath.row
    
        //check date
        let createdDateStr = dataDict[APIKeys.kCreated_at] as? String
        let createdDate = createdDateStr?.dateFromString(format: .dateTime)
        
        if createdDate?.isToday() == true {
            cell.lblTime.text = createdDate?.stringFormDateInLocalTimeZone(format: .time)
        }else{
            cell.lblTime.text = createdDate?.stringFormDateInLocalTimeZone(format: .mdyDate)
        }
        
        
    }
    
    
    //View Attachment
    func btnViewAttachmentAction(sender:UIButton) {
        let dataDict = messageArary[sender.tag]
        
        UIApplication.shared.open(URL(string:dataDict[APIKeys.kAttachment] as! String)!, options: [:],
                                  completionHandler: nil)

    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView == tableViewChat{
            
            if (scrollView.contentOffset.y <= 0)
            {
                if messageArary.count > 9 {
                    if paginationDict[APIKeys.kNextpage] as! Int != paginationDict[APIKeys.kCurrentpage] as! Int{
                        if isCallWebService == true{
                            isCallWebService = false
                            tableViewChat.tableHeaderView!.isHidden = false
                            Indicator.isEnabledIndicator = false
                            //call the webservice
                            callWebserviceForGetChatHistory(page: paginationDict[APIKeys.kNextpage] as! Int,isScrollBottom:false)
                        }
                    }
                }
            }
        }
    }

    
    
}
//MARK: UITextView Delegates

extension ChatVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
        if(textView.text == "Type your message here:") {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
        if(textView.text == "") {
            textView.textColor = UIColor.darkGray
            textView.text = "Type your message here:"
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //check the return key at location 0
        if (range.location == 0 && text == "\n") {
            textView.resignFirstResponder()
            return false;
        }
        
        //check the whitespace
        if (range.location == 0 && text == " ") {
            return false;
        }
        
        //check text in the textview
        if DataManager.role == 3{
        let newString = NSString(string: textView.text!).replacingCharacters(in: range, with: text)
         if newString == ""{
             hideAttachment()
         }else{
            ShowAttachment()
         }
        }else{
            if DataManager.isPackagePurchase == 1{
                let newString = NSString(string: textView.text!).replacingCharacters(in: range, with: text)
                if newString == ""{
                    hideAttachment()
                }else{
                    ShowAttachment()
                }
            }
            
        }
        return true
    }

    
    //MARK: Show/Hide Table Methods
    func ShowAttachment()  {
        //open Patient table view
        UIView.animate(withDuration: 0.40, delay: 0.0, options: [], animations: {
            self.constraintWidthOfAttachment.constant = 55
            self.view.layoutIfNeeded()
        })
    }
    
    func hideAttachment()  {
        UIView.animate(withDuration: 0.40, delay: 0.0, options: [], animations: {
            self.constraintWidthOfAttachment.constant = 0
            self.view.layoutIfNeeded()
        }, completion: { (finished: Bool) in
      })
    }
}

//MARK: Document Picker Delegates

extension ChatVC:UIDocumentPickerDelegate  {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        // set the existing values
        
        txtMessage.text = messageText
        
        if(controller.documentPickerMode == UIDocumentPickerMode.import){
            let data = try! Data(contentsOf: url)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            let mbString = bcf.string(fromByteCount: Int64(data.count))
            let strArr = mbString.components(separatedBy: " ")
            
            if Double(strArr[0])! > 5 {
                showAlert(message: kSelectAttachLessThan5, title: "")
                return
            }
            
//            lblAttachFile.text = "attach.pdf"
              attachDict["attachment"] = data
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController){
        // set the existing values
        txtMessage.text = messageText
        
    }
}

//MARK: Send message Webservice
extension ChatVC{
    func callWebserviceForSendMessage() {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kReceiverId]      = otherUserId
        parametersDict[APIKeys.kMessage]        = txtMessage.text
        parametersDict[APIKeys.kToken]          = DataManager.token
        
        MessageVM.sharedInstance.sendMessage(messageDetails: parametersDict as NSDictionary, applicationDict: attachDict, mineType: "pdf", response:{ (success, message, error) in
            if success {
                
                // add the message
                self.txtMessage.textColor = UIColor.darkGray
                self.txtMessage.text = "Type your message here:"
                self.attachDict = [:]
                self.hideAttachment()
                self.messageArary.append(MessageVM.sharedInstance.sendMessage)
                self.tableViewChat.reloadData()
                self.scrollToBottom()

            }
            else {
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
    }
    
    
    func callWebserviceForGetChatHistory(page:Int,isScrollBottom:Bool) {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kUserId]         = DataManager.userId
        parametersDict[APIKeys.kToken]          = DataManager.token
        parametersDict[APIKeys.kOtherUserId]    = otherUserId
        parametersDict[APIKeys.kPage]           = page
        
        MessageVM.sharedInstance.getChatHistory(chatDetails: parametersDict as NSDictionary!, response:{ (success, message, error) in
            Indicator.isEnabledIndicator = true
            if success {
                //append the next values
                if isScrollBottom{
                    self.messageArary.removeAll()
                }else{
                    self.firstData = self.messageArary[0]
                }
                    for obj in MessageVM.sharedInstance.listArray{
                        self.messageArary.insert(obj, at: 0)
                    }
                
                self.tableViewChat.tableHeaderView!.isHidden = true
//                self.lblNoChatFound.isHidden = true
                self.paginationDict = MessageVM.sharedInstance.pagination
                self.isCallWebService = true
                self.tableViewChat.reloadData() //reload the table

                if isScrollBottom{
                    self.scrollToBottom()
                    
                }else{
                    
                    let index = self.messageArary.index(of: self.firstData)
                    self.tableViewChat.scrollToRow(at: IndexPath(item:index!, section: 0), at: .top, animated: false)
                    
                    
//                    let range = NSMakeRange(0, self.tableViewChat.numberOfSections)
//                    let sections = NSIndexSet(indexesIn: range)
//                    self.tableViewChat.reloadSections(sections as IndexSet, with: .middle)
                }
    
            }
            else {
                self.tableViewChat.tableHeaderView!.isHidden = true
                if message != nil {
                    if message == kTokenNotMatched{   // check other device login
                        
                        DataManager.isUserLoggedIn = false
                        self.showAlert(message: kSomeOneLoginToAnotherDevice, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                            self.logout()   // logout the user
                        })
                        return
                    }else if message == kNoChatFound{
//                        self.lblNoChatFound.isHidden = false
                        return
                    }
                    
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        })
     }
}

