//
//  ForgotPassVC.swift
//  Unlock your 10
//
//  Created by Brst981 on 11/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

class ForgotPassVC: BaseViewController {

 //Outlets
    @IBOutlet var txtEmail: UITextField!
  
    @IBOutlet var btnRecoverPasswordOutlet: UIButton!
    
    
//Variables
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }
    
//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
        txtEmail.setPlaceholder(color: UIColor.white, size: 16, style: .medium)
        btnRecoverPasswordOutlet.setButtonBorderLayer()
        
    }
 //MARK: Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
    
    @IBAction func btnRecoverAction(_ sender: Any) {
        view.endEditing(true)
        if txtEmail.isEmpty {
            self.showAlert(message: kEnterEmail, title: "")
        }
        else if !txtEmail.isEmailValid{
            self.showAlert(message: kEnterValidEmail, title: "")
            
        }else{
            callWebserviceForForgotPassword() // call webservice for login
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

}
extension ForgotPassVC{
    
    
    func callWebserviceForForgotPassword()  {
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kEmailId] = txtEmail.text
     
        UserVM.sharedInstance.forgotPassword(forgotPasswordDetails:parametersDict as NSDictionary!) { (success, message, error) in
            if success {
                self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk , cancelAction: { (action) in
                    self.backButtonAction()
                })
            }
            else {
                if message != nil {
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        }
        
    }
    
}





