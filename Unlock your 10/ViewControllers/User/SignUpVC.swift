//
//  SignUpVC.swift
//  Unlock your 10
//
//  Created by Brst981 on 11/10/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import QuartzCore


class SignUpVC: BaseViewController {
    
   
    //Outlets
    
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtCreatepassword: UITextField!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var btnCreateAccountOutlet: UIButton!
   
    //Variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }

//MARK: customizeUI
    func customizeUI() {
         hideNavigationBar()
        //set the placehoder color
        txtUserName.setPlaceholder(color: UIColor.white, size: 15, style: .medium)
        txtEmail.setPlaceholder(color: UIColor.white, size: 15, style: .medium)
        txtCreatepassword.setPlaceholder(color: UIColor.white, size: 15, style: .medium)
        txtConfirmPassword.setPlaceholder(color: UIColor.white, size: 15, style: .medium)
        //set the create account button border.
        btnCreateAccountOutlet.setButtonBorderLayer()
       
    }
    
 //MARK: Buttons action
    @IBAction func btnBackAction(_ sender: Any) {
        backButtonAction()
    }
   
    @IBAction func SignUp(_ sender: Any) {
        if txtUserName.text == "" || txtEmail.text == "" || txtCreatepassword.text == "" || txtConfirmPassword.text == "" {
            self.showAlert(message: "Please enter all fields for creating an account!", title: "")
        } else  if !isValidEmail(email: txtEmail.text!) {
            self.showAlert(message: "Please enter the valid email.", title:"" )
        }else if !txtCreatepassword.isPasswordValid{
            showAlert(message: kPasswordNeeds8Character, title: "")
        }
        else if txtCreatepassword.text != txtConfirmPassword.text {
            self.showAlert(message: kPasswordAndConfirmPassword, title: "")
        } else {
            
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kName]        = txtUserName.text
            parametersDict[APIKeys.kEmailId]     = txtEmail.text
            parametersDict[APIKeys.kPassword]    = txtCreatepassword.text
            parametersDict[APIKeys.kDeviceToken] = "123"

            UserVM.sharedInstance.signupByEmail(signupDict: parametersDict as NSDictionary, response: { (success, message, error) in
                if success {
                    self.showAlert(message: message, title: "", otherButtons: nil, cancelTitle: kOk, cancelAction: { (action) in
                        self.backButtonAction()
                    })
                }
                else {
                    if message != nil {
                        self.showAlert(message: message, title: "")
                    }
                    else {
                        self.showErrorMessage(error: error!)
                    }
                    self.txtEmail.text = ""
                }
            })

        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 

}

//MARK: UITextView and UITextField Delegates

extension SignUpVC: UITextFieldDelegate {
    
 
    //Textfield
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        if textField == txtUserName {
            let newLength = currentCharacterCount + string.characters.count - range.length
            if validateNameTextFeild(string: string)
            {
                return newLength <= 25
            }else{
                return false
            }
        }
  
        return true
    }
    
}


