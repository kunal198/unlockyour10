//
//  LoginVC.swift
//  Unlock your 10
//
//  Created by brst on 10/9/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import FBSDKLoginKit



class LoginVC: BaseViewController {

    //OUTLETS
    
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnLoginOutlet: UIButton!
    @IBOutlet var btnSignUpOutlet: UIButton!
    
  //Variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
    
//MARK: View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        txtEmail.text    = DataManager.email
        txtPassword.text = ""
    }
//MARK: customizeUI
    func customizeUI()  {
        hideNavigationBar()
        txtEmail.setPlaceholder(color: UIColor.white, size: 16, style: .medium)
        txtPassword.setPlaceholder(color: UIColor.white, size: 16, style: .medium)
        btnLoginOutlet.setButtonBorderLayer()
        btnSignUpOutlet.setUnderLine()
        
    }
    
//MARK: Buttons Action
    @IBAction func btnLoginAction(_ sender: Any) {
        
        self.view.endEditing(true) // remove the keyboard
        if txtEmail.isEmpty {
            self.showAlert(message: kEnterEmail, title: "")
            
        }else if txtPassword.isEmpty {
            self.showAlert(message: kEnterPassword, title: "")
        }
        else if !txtEmail.isEmailValid{
            self.showAlert(message: kEnterValidEmail, title: "")
            
        }else{
            loginByEmail() // call webservice for login
        }
    }
    
    @IBAction func btnForgotPassAction(_ sender: Any) {
        animationPushWithIdentifier(identifier: kLoginToForgot)
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        animationPushWithIdentifier(identifier: kLoginToSignUP)
    }
    
    @IBAction func btnFacebookAction(_ sender: Any) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            
            if error == nil {
                if result?.grantedPermissions != nil {
                    self.getFBUserData()
                    //      Indicator.sharedInstance.showIndicator()
                }
            }
            else {
                print(error?.localizedDescription ?? "")
            }
        }
    }
    
 //MARK: Facebook method
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil){
                    let resultDict = result as? NSDictionary
                    debugPrint(resultDict!)
                    self.loginByfacebook(name: (resultDict?["name"] as? String)!, fb_id: (resultDict?["id"] as? String)! , email: (resultDict?["email"] as? String) ?? "")
                    
                } else {
                    
                    print(error?.localizedDescription ?? "")
                }
            })
        }
    }
    
//Validations

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: Login Webservice


extension LoginVC{
    
    func loginByEmail(){
        
        let email: String! = txtEmail.text
        
            var parametersDict = JSONDictionary()
            parametersDict[APIKeys.kEmailId] = email
            parametersDict[APIKeys.kPassword] = txtPassword.text
            parametersDict[APIKeys.kDeviceToken] = DataManager.deviceToken
        
            UserVM.sharedInstance.loginWithEmail(loginDetails:parametersDict as NSDictionary!) { (success, message, error) in
                
                print(parametersDict)
                if success {
                  DataManager.isUserLoggedIn = true
                    self.appDelegate.createMenu()
                
                }
                else {
                    if message != nil {
                        self.showAlert(message: message, title: "")
                    }
                    else {
                        self.showErrorMessage(error: error!)
                    }
                }
            }
        }
    
    
    func loginByfacebook(name:String,fb_id:String,email:String){
        
        var parametersDict = JSONDictionary()
        parametersDict[APIKeys.kName] = name
       // parametersDict[APIKeys.kEmailId] = email
        parametersDict[APIKeys.kSocialId] = fb_id
        parametersDict[APIKeys.kDeviceToken] = DataManager.deviceToken
        print(parametersDict)
        UserVM.sharedInstance.loginWithEmail(loginDetails:parametersDict as NSDictionary!) { (success, message, error) in
            if success {
                DataManager.isUserLoggedIn = true
                self.appDelegate.createMenu()
            }
            else {
                if message != nil {
                    self.showAlert(message: message, title: "")
                }
                else {
                    self.showErrorMessage(error: error!)
                }
            }
        }
    }
    
}





