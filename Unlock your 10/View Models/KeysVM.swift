//
//  KeysVM.swift
//  Unlock your 10
//
//  Created by brst on 11/22/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import Foundation
import StoreKit

class KeysVM {
    
    public static let sharedInstance = KeysVM()
    private init() {}

   //variables
    var keyValue = Int()
    var productList = [SKProduct]()
    var packageList = [SKProduct]()
    var trainingData = NSDictionary()
    
    
    func getKeys(keysDict: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.getKeys(keysDetails: keysDict, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                //save the key value
                self.keyValue = (responseDict![APIKeys.kData] as! NSDictionary).value(forKey: APIKeys.kKeyValue) as! Int
                //check the user role
                if DataManager.role == 2{
                    DataManager.isPackagePurchase = (responseDict![APIKeys.kData] as! NSDictionary).value(forKey: APIKeys.kIsPackagePurchased) as? Int
                    DataManager.freeTraining = (responseDict![APIKeys.kData] as! NSDictionary).value(forKey: APIKeys.kAccessTraining) as? Int
                }
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func purchaseKeys(keysDict: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.purchaseKeys(keysDetails: keysDict, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                //                self.parseUserData(responseDict: responseDict!, type: "signup")
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func purchasePackage(packageDetails: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.purchasePackage(packageDetails: packageDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                //                self.parseUserData(responseDict: responseDict!, type: "signup")
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
    func withdrawalRequest(requestDetails: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.withdrawalRequest(requestDetails: requestDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
         
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func referUser(userDetails: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.referUser(userDetails: userDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func requestTraining(trainingDetails: NSDictionary!, response: @escaping responseCallBack) {
        trainingData = NSDictionary()
        APIManager.requestTraining(trainingDetails: trainingDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                self.trainingData = responseDict![APIKeys.kData] as! NSDictionary
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
}
