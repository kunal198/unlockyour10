//
//  MessageVM.swift
//  Unlock your 10
//
//  Created by brst on 10/25/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import Foundation


class MessageVM {

    var listArray  = [NSDictionary]()
    var pagination = NSDictionary()
    var sendMessage = NSDictionary()
    
    public static let sharedInstance = MessageVM()
    private init() {}

    func sendMessage(messageDetails: NSDictionary!,applicationDict:[String:Data]?,mineType:String?, response: @escaping responseCallBack) {
        sendMessage = NSDictionary()
        APIManager.createMessage(messageDetails:messageDetails,applicationDict:applicationDict,mineType:mineType , successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                  self.sendMessage = responseDict!["message_data"] as! NSDictionary
                
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func getHome(homeDetails: NSDictionary!, response: @escaping responseCallBack) {
        listArray = [NSDictionary]()
        APIManager.getHome(homeDetails:homeDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                self.listArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func getChatThreads(threadDetails: NSDictionary!, response: @escaping responseCallBack) {
        listArray = [NSDictionary]()
        pagination = NSDictionary()
        
        APIManager.getChatThreads(threadDetails:threadDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                self.listArray = responseDict![APIKeys.kData] as! [NSDictionary]
                self.pagination = responseDict![APIKeys.kPagination] as! NSDictionary
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
    func getChatHistory(chatDetails: NSDictionary!, response: @escaping responseCallBack) {
        listArray = [NSDictionary]()
        pagination = NSDictionary()
        
        APIManager.getChatHistory(chatDetails:chatDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                self.listArray = responseDict![APIKeys.kData] as! [NSDictionary]
                self.pagination = responseDict![APIKeys.kPagination] as! NSDictionary
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }

    func deleteThread(threadDetail: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.deleteThread(threadDetails:threadDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
}
