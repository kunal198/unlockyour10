//
//  UserVM.swift
//  HealthSplashPatient
//
//  Created by 123 on 13/04/17.
//  Copyright © 2017 Deftsoft. All rights reserved.
//

import Foundation


class UserVM {
    
    public static let sharedInstance = UserVM()
    private init() {}

    //array for data List
    var mentorListArray = [NSDictionary]()
    var mentorDetails = NSDictionary()
    //Array for get complaints
    var complaintListArray = [NSDictionary]()
    var complaintDetailsDic = NSDictionary()
    
    //Array for get training session
    var sessionListArray = [NSDictionary]()
    var userNameListArr = [NSDictionary]()
    var sessionDetailsDic = NSDictionary()
    var checkUser_id = Int()
    var isLogout = Bool()
    var threadId = String()
    
    
    func signupByEmail(signupDict: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.signupByEmail(signupDetails: signupDict, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.parseUserData(responseDict: responseDict!, type: "signup")
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func loginWithEmail(loginDetails: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.loginWithEmail(loginDetails:loginDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.parseUserData(responseDict: responseDict!, type: "login")
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    func forgotPassword(forgotPasswordDetails: NSDictionary!, response: @escaping responseCallBack) {
        APIManager.forgotPassword(forgotPasswordDetails: forgotPasswordDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func mentorRegister(mentorDetails: NSDictionary!,imageDict:[String:Data]?,applicationDict:[String:Data]?,mineType:String?, response: @escaping responseCallBack) {
        APIManager.mentorRegister(mentorDetails: mentorDetails,imageDict: imageDict,applicationDict:applicationDict,mineType:mineType , successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func searchMentors(searchDetails: NSDictionary!, response: @escaping responseCallBack) {
         mentorListArray = [NSDictionary]()
        APIManager.searchMentors(searchDetails: searchDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                self.mentorListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
            
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    func updateProfile(profileDetails: NSDictionary!,imageDict:[String:Data]?,applicationDict:[String:Data]?,mineType:String?, response: @escaping responseCallBack) {
        APIManager.updateProfile(profileDetails: profileDetails,imageDict: imageDict,applicationDict:applicationDict,mineType:mineType , successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.parseUserData(responseDict: responseDict!, type: "updateProfile")

                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
 
    func getMentorProfile(profileDetails: NSDictionary!, response: @escaping responseCallBack) {
        mentorDetails = NSDictionary()
        APIManager.getMentorProfile(profileDetails: profileDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.mentorDetails = responseDict![APIKeys.kData] as! NSDictionary

                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }

    //Post Comlaints
    func postComplaints(complaintDetails: NSDictionary!, response: @escaping responseCallBack) {
        //mentorListArray = [NSDictionary]()
        APIManager.PostComplaints(complaintDetails: complaintDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
               // self.mentorListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    //Get Complaints
    func getComplaints(complaintDetails: NSDictionary!, response: @escaping responseCallBack) {
        complaintListArray = [NSDictionary]()
        APIManager.getComplaints(getComplaints: complaintDetails, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
                self.complaintListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                //self.complaintDetailsDic = responseDict![APIKeys.kData] as! NSDictionary
                
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
    
    func postComments(dataDic: NSDictionary!, response: @escaping responseCallBack) {
        complaintListArray = [NSDictionary]()
        APIManager.postComments(dicData: dataDic, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                
               // self.complaintListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                //self.complaintDetailsDic = responseDict![APIKeys.kData] as! NSDictionary
                
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    //Add Training Session
    func addTrainingSession(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        complaintListArray = [NSDictionary]()
        APIManager.addTrainingSession(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {

                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    //Get Training Session
    func getTrainingSession(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        sessionListArray = [NSDictionary]()
        APIManager.getTrainingSession(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.sessionListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    //delete Training Session
    func deleteTrainingSession(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        sessionListArray = [NSDictionary]()
        APIManager.deleteTrainingSession(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                //self.sessionListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    //Edit Training Session
    func editTrainingSession(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        sessionListArray = [NSDictionary]()
        APIManager.editTrainingSession(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                //self.sessionListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    //Add Date Request
    func addDateRequest(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        sessionListArray = [NSDictionary]()
        APIManager.addDateRequest(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                //self.sessionListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    //Check Name
    func checkName(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        sessionListArray = [NSDictionary]()
        APIManager.checkName(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.userNameListArr = responseDict![APIKeys.kData] as! [NSDictionary]
                    //self.checkUser_id = responseDict![APIKeys.kData] as! Int
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    //Get User Profile
    func getUserProfile(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        sessionListArray = [NSDictionary]()
        APIManager.getUserProfile(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.sessionDetailsDic = responseDict![APIKeys.kData] as! NSDictionary
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    
    //Booking
    func bookingRequest(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
       // sessionListArray = [NSDictionary]()
        APIManager.bookingRequest(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
            //    self.sessionDetailsDic = responseDict![APIKeys.kData] as! NSDictionary
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    //Booking
    func requestSession(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        // sessionListArray = [NSDictionary]()
        APIManager.getRequestTrainingSession(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.sessionListArray = responseDict![APIKeys.kData] as! [NSDictionary]
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    //ChnageStatusOfTraining
    func ChnageTrainingStatus(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        // sessionListArray = [NSDictionary]()
        APIManager.ChnageTrainingStatus(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    //Get Details Of Training
    func getDetailsOfTrainig(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        // sessionListArray = [NSDictionary]()
        APIManager.getDetailsOfTrainig(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")            
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                self.sessionDetailsDic = responseDict![APIKeys.kData] as! NSDictionary
                response(true, message, nil)
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
}

extension UserVM
{
    //Join Training Session
    func joinTraining(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        // sessionListArray = [NSDictionary]()
        APIManager.joinTraining(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
    
    //Leave Training Session
    func leaveTrainingSession(sessionDetail: NSDictionary!, response: @escaping responseCallBack) {
        // sessionListArray = [NSDictionary]()
        APIManager.leaveTrainingSession(dicData: sessionDetail, successCallback: { (responseDict) in
            debugPrint(responseDict ?? "No Value")
            let message = responseDict![APIKeys.kMessage] as? String
            if responseDict![APIKeys.kSuccess] as! String == "1" {
                response(true, message, nil)
                
            }
            else {
                response(false, message, nil)
            }
        }) { (errorReason, error) in
            response(false, nil, APIManager.errorForNetworkErrorReason(errorReason: errorReason!))
        }
    }
}
